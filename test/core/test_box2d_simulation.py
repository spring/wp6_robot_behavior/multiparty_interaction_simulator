import mpi_sim
from Box2D import b2PolygonShape


class DummyObjectA(mpi_sim.Object):
    def _create_in_box2d_world(self, box2d_world):
        return box2d_world.CreateStaticBody(
            position=(0,0),
            angle=0,
            shapes=b2PolygonShape(box=(1, 1)),
        )


class DummyObjectB(mpi_sim.Object):
    def _create_in_box2d_world(self, box2d_world):
        return box2d_world.CreateStaticBody(
            position=(0,0),
            angle=0,
            shapes=b2PolygonShape(box=(1, 1)),
        )


def test_get_bodies_and_operator():

    sim = mpi_sim.Simulation()

    obj1 = DummyObjectA(id=1, properties=['X'])
    obj2 = DummyObjectA(id=2, properties=['Y'])
    obj3 = DummyObjectB(id=3, properties=['Y'])
    obj4 = DummyObjectB(id=4, properties=['Z'])
    sim.add_object([obj1, obj2, obj3, obj4])

    # object types

    bodies = sim.box2d_simulation.get_bodies(object_types=DummyObjectA)
    assert len(bodies) == 2
    assert bodies[0].userData['object'].id == 1
    assert bodies[1].userData['object'].id == 2

    bodies = sim.box2d_simulation.get_bodies(object_types=[DummyObjectA,DummyObjectB])
    assert len(bodies) == 4
    assert bodies[0].userData['object'].id == 1
    assert bodies[1].userData['object'].id == 2
    assert bodies[2].userData['object'].id == 3
    assert bodies[3].userData['object'].id == 4

    # object ids

    bodies = sim.box2d_simulation.get_bodies(object_ids=2)
    assert len(bodies) == 1
    assert bodies[0].userData['object'].id == 2

    bodies = sim.box2d_simulation.get_bodies(object_ids=[2, 4])
    assert len(bodies) == 2
    assert bodies[0].userData['object'].id == 2
    assert bodies[1].userData['object'].id == 4

    # object properties

    bodies = sim.box2d_simulation.get_bodies(object_properties='X')
    assert len(bodies) == 1
    assert bodies[0].userData['object'].id == 1

    bodies = sim.box2d_simulation.get_bodies(object_properties=['X', 'Z'])
    assert len(bodies) == 2
    assert bodies[0].userData['object'].id == 1
    assert bodies[1].userData['object'].id == 4

    # combinations

    bodies = sim.box2d_simulation.get_bodies(object_types=DummyObjectA, object_properties='Y')
    assert len(bodies) == 1
    assert bodies[0].userData['object'].id == 2

    bodies = sim.box2d_simulation.get_bodies(object_ids=[2, 3, 4], object_properties='Y')
    assert len(bodies) == 2
    assert bodies[0].userData['object'].id == 2
    assert bodies[1].userData['object'].id == 3


def test_get_bodies_or_operator():

    sim = mpi_sim.Simulation()

    obj1 = DummyObjectA(id=1, properties=['X'])
    obj2 = DummyObjectA(id=2, properties=['Y'])
    obj3 = DummyObjectB(id=3, properties=['Y'])
    obj4 = DummyObjectB(id=4, properties=['Z'])
    sim.add_object([obj1, obj2, obj3, obj4])

    # object types

    bodies = sim.box2d_simulation.get_bodies(object_types=DummyObjectA, search_operator='or')
    assert len(bodies) == 2
    assert bodies[0].userData['object'].id == 1
    assert bodies[1].userData['object'].id == 2

    bodies = sim.box2d_simulation.get_bodies(object_types=[DummyObjectA,DummyObjectB], search_operator='or')
    assert len(bodies) == 4
    assert bodies[0].userData['object'].id == 1
    assert bodies[1].userData['object'].id == 2
    assert bodies[2].userData['object'].id == 3
    assert bodies[3].userData['object'].id == 4

    # object ids

    bodies = sim.box2d_simulation.get_bodies(object_ids=2, search_operator='or')
    assert len(bodies) == 1
    assert bodies[0].userData['object'].id == 2

    bodies = sim.box2d_simulation.get_bodies(object_ids=[2, 4], search_operator='or')
    assert len(bodies) == 2
    assert bodies[0].userData['object'].id == 2
    assert bodies[1].userData['object'].id == 4

    # object properties

    bodies = sim.box2d_simulation.get_bodies(object_properties='X', search_operator='or')
    assert len(bodies) == 1
    assert bodies[0].userData['object'].id == 1

    bodies = sim.box2d_simulation.get_bodies(object_properties=['X', 'Z'], search_operator='or')
    assert len(bodies) == 2
    assert bodies[0].userData['object'].id == 1
    assert bodies[1].userData['object'].id == 4

    # combinations

    bodies = sim.box2d_simulation.get_bodies(object_types=DummyObjectA, object_properties='Y', search_operator='or')
    assert len(bodies) == 3
    assert bodies[0].userData['object'].id == 1
    assert bodies[1].userData['object'].id == 2
    assert bodies[2].userData['object'].id == 3

    bodies = sim.box2d_simulation.get_bodies(object_ids=[3, 4], object_properties='X', search_operator='or')
    assert len(bodies) == 3
    assert bodies[0].userData['object'].id == 1
    assert bodies[1].userData['object'].id == 3
    assert bodies[2].userData['object'].id == 4