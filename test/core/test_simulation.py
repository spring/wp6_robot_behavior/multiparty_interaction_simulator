import mpi_sim as sim
import numpy as np


class DummyObject(sim.Object):
    pass


class DummyScript(sim.Script):
    pass


def test_entity_adding():

    simulation = sim.Simulation()

    ###############################
    # OBJECTS
    obj1 = DummyObject()
    id1 = simulation.add_object(obj1, id=1)
    assert id1 == 1

    obj2 = DummyObject()
    id2 = simulation.add_object(obj2)
    assert id2 >= 0 and id2 not in [id1]

    obj3 = DummyObject()
    obj4 = DummyObject()
    [id3, id4] = simulation.add_object([obj3, obj4])
    assert id3 >= 0 and id3 not in [id1, id2]
    assert id4 >= 0 and id4 not in [id1, id2, id3]

    obj5 = DummyObject()
    obj6 = DummyObject()
    [id5, id6] = simulation.add_object([obj5, obj6], id=[None, 1000])
    assert id5 >= 0 and id5 not in [id1, id2, id3, id4]
    assert id6 == 1000

    ###############################
    # SCRIPTS
    script1 = DummyScript()
    id1 = simulation.add_script(script1, id=10000)
    assert id1 == 10000

    script2 = DummyScript()
    id2 = simulation.add_script(script2)
    assert id2 >= 0 and id2 not in [id1]

    script3 = DummyScript()
    script4 = DummyScript()
    [id3, id4] = simulation.add_script([script3, script4])
    assert id3 >= 0 and id3 not in [id1, id2]
    assert id4 >= 0 and id4 not in [id1, id2, id3]

    script5 = DummyScript()
    script6 = DummyScript()
    [id5, id6] = simulation.add_script([script5, script6], id=[None, 20000])
    assert id5 >= 0 and id5 not in [id1, id2, id3, id4]
    assert id6 == 20000

    simulation.close()


def test_entity_retrieval():

    simulation = sim.Simulation()

    # object
    obj = DummyObject()
    simulation.add_object(obj, id=1)

    assert simulation.entities[1] == obj
    assert simulation.objects[1] == obj

    obj_list = simulation.get_objects_by_type(DummyObject)
    assert len(obj_list) == 1
    assert obj_list[0] == obj

    # script
    script = DummyScript()
    simulation.add_script(script, id=2)

    assert simulation.entities[2] == script
    assert simulation.scripts[2] == script

    script_list = simulation.get_scripts_by_type(DummyScript)
    assert len(script_list) == 1
    assert script_list[0] == script

    simulation.close()


def test_reset_after_step():
    """Tests if the simulation resets correctly to its initial state after the step function was called."""

    init_position = [1.0, 1.0]
    init_orientation = 0.0

    simulation = sim.Simulation()

    human_obj = sim.objects.Human(position=init_position, orientation=init_orientation)
    simulation.add_object(human_obj, id=1)

    # check if initial position is correctly set
    np.testing.assert_array_equal(init_position, human_obj.position)
    np.testing.assert_array_equal(init_orientation, human_obj.orientation)

    # set a goal position for the human, so that it moves
    human_obj.goal_position = [2.0, 2.0]
    human_obj.goal_orientation = 1.0

    # do 2 steps to allow the human to move
    simulation.step()
    simulation.step()

    # check if position of human has changed
    # assert not np.array_equal(init_position, human_obj.position)
    # assert not np.array_equal(init_orientation, human_obj.orientation)

    simulation.reset()

    # check if initial position is correctly reset
    np.testing.assert_array_equal(init_position, human_obj.position)
    # np.testing.assert_array_equal(init_orientation, human_obj.orientation)

    simulation.close()


def test_reset_after_run():
    """Tests if the simulation resets correctly to its initial state after the run method was called."""

    init_position = [1.0, 1.0]
    init_orientation = 0.0

    simulation = sim.Simulation()

    human_obj = sim.objects.Human(position=init_position, orientation=init_orientation)
    simulation.add_object(human_obj, id=1)

    # check if initial position is correctly set
    np.testing.assert_array_equal(init_position, human_obj.position)
    np.testing.assert_array_equal(init_orientation, human_obj.orientation)

    # set a goal position for the human, so that it moves
    human_obj.goal_position = [2.0, 2.0]
    human_obj.goal_orientation = 1.0

    # do 2 steps to allow the human to move
    simulation.run(n_steps=2)

    # check if position of human has changed
    # assert not np.array_equal(init_position, human_obj.position)
    # assert not np.array_equal(init_orientation, human_obj.orientation)

    simulation.reset()

    # check if initial position is correctly reset
    np.testing.assert_array_equal(init_position, human_obj.position)
    # np.testing.assert_array_equal(init_orientation, human_obj.orientation)

    simulation.close()


def test_reset_after_set_reset_point():
    """Tests if the simulation resets correctly to its initial state after the set_reset_point method was called."""

    init_position = [1.0, 1.0]
    init_orientation = 0.0

    simulation = sim.Simulation()
    assert simulation.step_counter is None
    assert simulation.time is None

    assert simulation.has_reset_point == False

    human_obj = sim.objects.Human(position=init_position, orientation=init_orientation)
    simulation.add_object(human_obj, id=1)

    # check if initial position is correctly set
    np.testing.assert_array_equal(init_position, human_obj.position)
    # np.testing.assert_array_equal(init_orientation, human_obj.orientation)

    # set a goal position for the human, so that it moves
    human_obj.goal_position = [2.0, 2.0]
    human_obj.goal_orientation = 1.0

    # do 2 steps to allow the human to move
    simulation.run(n_steps=2)

    assert simulation.has_reset_point == True
    assert simulation.step_counter == 1
    assert simulation.time == simulation.step_length

    # save new position of human as the position it needs to be reset to
    reset_position = human_obj.position.copy()
    reset_orientation = human_obj.orientation

    # set a new reset point
    simulation.set_reset_point()
    assert simulation.has_reset_point == True

    # do 2 steps to allow the human to move further
    simulation.run(n_steps=2)
    assert simulation.has_reset_point == True

    assert simulation.step_counter == 3
    np.testing.assert_array_almost_equal(simulation.time, 3 * simulation.step_length)

    # check if position of human has changed
    # assert not np.array_equal(reset_position, human_obj.position)
    # assert not np.array_equal(reset_orientation, human_obj.orientation)

    simulation.reset()
    assert simulation.has_reset_point == True

    assert simulation.step_counter == 1
    np.testing.assert_array_almost_equal(simulation.time, simulation.step_length)

    # check if the reset position is correctly reset
    np.testing.assert_array_equal(reset_position, human_obj.position)
    # np.testing.assert_array_equal(reset_orientation, human_obj.orientation)

    simulation.close()


def test_automatic_id_generation():

    simulation = sim.Simulation()

    ################
    # objects

    obj_1 = DummyObject()
    id_1 = simulation.add_object(obj_1)

    assert obj_1 == simulation.objects[id_1]
    assert obj_1.id == id_1

    obj_2 = DummyObject()
    id_2 = simulation.add_object(obj_2)

    assert obj_2 == simulation.objects[id_2]
    assert obj_2.id == id_2

    ################
    # scripts

    script_1 = DummyScript()
    script_id_1 = simulation.add_script(script_1)

    assert script_1 == simulation.scripts[script_id_1]
    assert script_1.id == script_id_1

    script_2 = DummyScript()
    script_id_2 = simulation.add_script(script_2)

    assert script_2 == simulation.scripts[script_id_2]
    assert script_2.id == script_id_2


def test_get_nearby_objects():

    ################
    # Placement:
    # 3|
    # 2| 1  2
    # 1| W
    #   -----
    #   1 2 3
    ################

    # create a simulation with different objects
    wall = sim.objects.Wall(
        position = [1.0, 1.0],
        orientation = 0.0,
        width = 0.5,
        height = 0.5
    )
    human_1 = sim.objects.Human(position=[1.0, 2.0])
    human_2 = sim.objects.Human(position=[3.0, 2.0])

    simulation = sim.Simulation()

    simulation.add_object(wall)
    simulation.add_object(human_1)
    simulation.add_object(human_2)

    # test the nearby objects function

    objects = simulation.get_nearby_objects(
        point=[2.0, 2.0],
        radius=5.0
    )

    # are all objects in it?
    assert len(objects) == 3
    assert wall in objects
    assert human_1 in objects
    assert human_2 in objects

    # test with different radius
    objects = simulation.get_nearby_objects(
        point=[1.0, 1.5],
        radius=1.0
    )
    # are all objects in it?
    assert len(objects) == 2
    assert wall in objects
    assert human_1 in objects

    # test with sub object types
    objects = simulation.get_nearby_objects(
        point=[1.0, 1.5],
        radius=1.0,
        object_types=(sim.objects.Human, )

    )
    # are all objects in it?
    assert len(objects) == 1
    assert human_1 in objects

    # test with single sub object types
    objects = simulation.get_nearby_objects(
        point=[1.0, 1.5],
        radius=1.0,
        object_types=sim.objects.Human

    )
    # are all objects in it?
    assert len(objects) == 1
    assert human_1 in objects

    # test with giving an object
    objects = simulation.get_nearby_objects(
        point=human_1,
        radius=2.0,
        object_types=sim.objects.Human

    )
    # are all objects in it?
    assert len(objects) == 1
    assert human_2 in objects


def test_run_stop_conditions():

    sim_step_length = 0.05

    simulation = sim.Simulation(step_length=sim_step_length)
    obj1 = DummyObject()  # 5 times per second
    simulation.add_object(obj1)

    #######################################
    # stop after certain duration

    assert simulation.time == 0.0 or simulation.time is None

    # 1 second
    simulation.run(duration=1.0)

    # the simulation has nothing to do, so the RTF should be larger than 1
    assert simulation.time >= 1.0 and simulation.time <= 1.0 + simulation.step_length

    #######################################
    # stop after certain number of steps

    simulation.reset()

    assert simulation.time == 0.0 or simulation.time is None

    # 1 second
    simulation.run(n_steps=20)

    # counter starts from 0
    assert simulation.step_counter + 1 == 20

    #######################################
    # stop condition

    def my_stop_condition(simulation, step):
        if step == 2:
            return True
        return False

    simulation.reset()

    assert simulation.time == 0.0 or simulation.time is None

    # 1 second
    simulation.run(stop_condition=my_stop_condition)

    # counter starts from 0
    assert simulation.step_counter == 1
