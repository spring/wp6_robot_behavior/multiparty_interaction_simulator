import mpi_sim as sim


class DummyScript(sim.Script):

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.n_adds = 0
        self.n_steps = 0
        self.n_resets = 0
        self.n_removes = 0
        self.n_closes = 0

    def _add(self):
        self.n_adds += 1

    def _reset(self):
        self.n_resets += 1

    def _step(self):
        self.n_steps += 1

    def _remove(self):
        self.n_removes += 1

    def _close(self):
        self.n_closes += 1


def test_script_in_simulation():

    simulation = sim.Simulation()

    script = DummyScript()

    assert script.n_adds == 0
    assert script.n_steps == 0
    assert script.n_resets == 0
    assert script.n_removes == 0
    assert script.n_closes == 0

    id = simulation.add_script(script, id=1)
    assert id == 1

    assert script == simulation.scripts[1]

    assert script.n_adds == 1
    assert script.n_steps == 0
    assert script.n_resets == 0
    assert script.n_removes == 0
    assert script.n_closes == 0

    simulation.step()

    assert script.n_adds == 1
    assert script.n_steps == 1
    assert script.n_resets == 0
    assert script.n_removes == 0
    assert script.n_closes == 0

    simulation.reset()

    assert script.n_adds == 1
    assert script.n_steps == 1
    assert script.n_resets == 1
    assert script.n_removes == 0
    assert script.n_closes == 0

    simulation.remove_script(script)

    assert script.is_removed

    assert script.n_adds == 1
    assert script.n_steps == 1
    assert script.n_resets == 1
    assert script.n_removes == 1
    assert script.n_closes == 0

    simulation.step()

    assert script.n_adds == 1
    assert script.n_steps == 1
    assert script.n_resets == 1
    assert script.n_removes == 1
    assert script.n_closes == 0

    simulation.reset()

    assert not script.is_removed

    assert script.n_adds == 1
    assert script.n_steps == 1
    assert script.n_resets == 2
    assert script.n_removes == 1
    assert script.n_closes == 0

    simulation.step()

    assert script.n_adds == 1
    assert script.n_steps == 2
    assert script.n_resets == 2
    assert script.n_removes == 1
    assert script.n_closes == 0

    simulation.close()

    assert script.n_adds == 1
    assert script.n_steps == 2
    assert script.n_resets == 2
    assert script.n_removes == 2
    assert script.n_closes == 1
