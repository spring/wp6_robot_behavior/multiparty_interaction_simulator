import mpi_sim as sim


class DummyProcess(sim.Process):

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.n_steps = 0
        self.n_resets = 0

    def _reset(self):
        self.n_resets += 1

    def _step(self):
        self.n_steps += 1


def test_processes():

    simulation = sim.Simulation()

    # add a single process

    process1 = DummyProcess()
    assert process1.name is None
    assert process1.simulation is None

    simulation.add_process(process1)

    assert process1.name == 'DummyProcess'
    assert process1.simulation == simulation

    assert len(simulation.get_processes_by_type(DummyProcess)) > 0

    assert process1.n_resets == 0
    assert process1.n_steps == 0

    simulation.run(n_steps=2)

    assert process1.n_resets == 0
    assert process1.n_steps == 2

    simulation.reset()

    assert process1.n_resets == 1
    assert process1.n_steps == 2

    simulation.run(n_steps=2)

    assert process1.n_resets == 1
    assert process1.n_steps == 4

    # add a further process
    process2 = DummyProcess()
    simulation.add_process(process2, name='DummyProcess2')
    assert process2.name == 'DummyProcess2'

    assert 'DummyProcess' in simulation.processes
    assert 'DummyProcess2' in simulation.processes

    assert process1.n_resets == 1
    assert process1.n_steps == 4
    assert process2.n_resets == 0
    assert process2.n_steps == 0

    simulation.run(n_steps=2)

    assert process1.n_resets == 1
    assert process1.n_steps == 6
    assert process2.n_resets == 0
    assert process2.n_steps == 2

    # remove process
    simulation.remove_process(process1)

    assert 'DummyProcess' not in simulation.processes
    assert 'DummyProcess2' in simulation.processes

    simulation.reset()

    assert process1.n_resets == 1
    assert process1.n_steps == 6
    assert process2.n_resets == 1
    assert process2.n_steps == 2

    assert process2 == simulation.processes['DummyProcess2']