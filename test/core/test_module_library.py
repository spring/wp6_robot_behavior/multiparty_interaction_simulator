import mpi_sim
import mpi_sim.core.module_library as modlib
import os

CURRENT_DIRECTORY = os.path.dirname(os.path.realpath(__file__))


def test_load_internal_modules():

    # objects
    assert modlib.get_object('Wall') == mpi_sim.objects.Wall
    assert modlib.get_object('Human') == mpi_sim.objects.Human
    assert modlib.get_object('ARIRobot') == mpi_sim.objects.ARIRobot

    # components
    assert modlib.get_component('SpeechGenerator') == mpi_sim.components.SpeechGenerator


    # scripts


    # processes
    assert modlib.get_process('GUI') == mpi_sim.processes.GUI


def test_load_external_modules():

    config = mpi_sim.AttrDict(
        object_directories=[os.path.join(CURRENT_DIRECTORY, 'files/objects')],
        component_directories=[os.path.join(CURRENT_DIRECTORY, 'files/components')],
        script_directories=[os.path.join(CURRENT_DIRECTORY, 'files/scripts')],
        process_directories=[os.path.join(CURRENT_DIRECTORY, 'files/processes')]
    )

    modlib._load_from_directories(config)

    assert modlib.get_object('DummyObject').test_identifier == 1
    assert modlib.get_component('DummyComponent').test_identifier == 2
    assert modlib.get_script('DummyScript').test_identifier == 3
    assert modlib.get_process('DummyProcess').test_identifier == 4
