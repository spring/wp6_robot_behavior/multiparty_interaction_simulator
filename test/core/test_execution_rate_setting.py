import mpi_sim as sim
import numpy.testing


class DummyProcess(sim.Process):

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.n_steps = 0

    def _step(self):
        self.n_steps += 1


class DummyObject(sim.Object):

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.n_steps = 0

    def _step(self):
        self.n_steps += 1


def test_execution_rate_setting_1():

    simulation = sim.Simulation(step_length=1/20)

    process1 = DummyProcess(execution_rate=20)
    obj1 = DummyObject(execution_rate=20)

    simulation.add_process(process1)
    simulation.add_object(obj1)

    simulation.run(n_steps=2)

    assert process1.n_steps == 2
    assert obj1.n_steps == 2

    simulation.reset()

    assert process1.n_steps == 2
    assert obj1.n_steps == 2

    simulation.run(n_steps=4)

    assert process1.n_steps == 2 + 4
    assert obj1.n_steps == 2 + 4


def test_execution_rate_setting_2():

    simulation = sim.Simulation(step_length=1/20)

    process1 = DummyProcess(execution_rate=10)  # 10 times per second
    obj1 = DummyObject(execution_rate=5)  # 5 times per second

    simulation.add_process(process1)
    simulation.add_object(obj1)

    simulation.run(n_steps=20)  # 1 second

    assert process1.n_steps == 10
    assert obj1.n_steps == 5

    simulation.reset()

    assert process1.n_steps == 10
    assert obj1.n_steps == 5

    simulation.run(n_steps=40)  # 2 seconds

    assert process1.n_steps == 10 + 20
    assert obj1.n_steps == 5 + 10


def test_real_time_factor():

    #######################################
    # run as fast as possible

    simulation = sim.Simulation(step_length=1/20)

    process1 = DummyProcess(execution_rate=20)  # 10 times per second
    obj1 = DummyObject(execution_rate=20)  # 5 times per second

    simulation.add_process(process1)
    simulation.add_object(obj1)

    simulation.run(n_steps=20)  # 1 second

    # the simulation has nothing to do, so the RTF should be larger than 1
    assert simulation.real_time_factor > 1.0

    #######################################
    # run with rtf of 1

    simulation = sim.Simulation(step_length=1/20, max_real_time_factor=1.0)

    process1 = DummyProcess(execution_rate=20)  # 10 times per second
    obj1 = DummyObject(execution_rate=20)  # 5 times per second

    simulation.add_process(process1)
    simulation.add_object(obj1)

    simulation.run(n_steps=20)  # 1 second

    numpy.testing.assert_almost_equal(simulation.real_time_factor, 1.0, decimal=2)

    #######################################
    # run with rtf of 2

    simulation = sim.Simulation(step_length=1/20, max_real_time_factor=2.0)

    process1 = DummyProcess(execution_rate=20)  # 10 times per second
    obj1 = DummyObject(execution_rate=20)  # 5 times per second

    simulation.add_process(process1)
    simulation.add_object(obj1)

    simulation.run(n_steps=20)  # 1 second

    numpy.testing.assert_almost_equal(simulation.real_time_factor, 2.0, decimal=1)
