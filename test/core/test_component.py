import mpi_sim as sim
import time

class DummyComponent(sim.Component):

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.n_adds_to_simulation = 0
        self.n_adds_to_object = 0
        self.n_steps = 0
        self.n_resets = 0
        self.n_removes = 0
        self.n_closes = 0
        self.last_step_time = None

    def _add_to_simulation(self):
        self.n_adds_to_simulation += 1

    def _add_to_object(self):
        self.n_adds_to_object += 1

    def _reset(self):
        self.n_resets += 1

    def _step(self):
        self.n_steps += 1
        self.last_step_time = time.time()

    def _remove(self):
        self.n_removes += 1

    def _close(self):
        self.n_closes += 1


class DummyObject(sim.Object):
    pass


def test_standard_component_in_simulation():

    simulation = sim.Simulation()

    obj = DummyObject()

    component = DummyComponent()
    obj.add_component(component)

    assert obj.exist_component('DummyComponent')
    assert 'DummyComponent' in obj.components
    assert component == obj.get_component('DummyComponent')
    assert component == obj.DummyComponent
    assert component.order >= 0

    assert component.n_adds_to_simulation == 0
    assert component.n_adds_to_object == 1
    assert component.n_steps == 0
    assert component.n_resets == 0
    assert component.n_removes == 0
    assert component.n_closes == 0

    id = simulation.add_object(obj, id=1)
    assert id == 1

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 0
    assert component.n_resets == 0
    assert component.n_removes == 0
    assert component.n_closes == 0

    simulation.step()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 0
    assert component.n_removes == 0
    assert component.n_closes == 0

    simulation.reset()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 1
    assert component.n_removes == 0
    assert component.n_closes == 0

    simulation.remove_object(obj)

    assert component.is_removed

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 1
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.step()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 1
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.reset()

    assert not component.is_removed

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 2
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.step()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 2
    assert component.n_resets == 2
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.close()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 2
    assert component.n_resets == 2
    assert component.n_removes == 2
    assert component.n_closes == 1


def test_sensor_component_in_simulation():

    simulation = sim.Simulation()

    obj = DummyObject()

    component = DummyComponent(order=sim.SensorComponent.default_config().order)
    obj.add_component(component)

    assert obj.exist_component('DummyComponent')
    assert component == obj.get_component('DummyComponent')
    assert component == obj.DummyComponent
    assert component.order >= 0

    assert component.n_adds_to_simulation == 0
    assert component.n_adds_to_object == 1
    assert component.n_steps == 0
    assert component.n_resets == 0
    assert component.n_removes == 0
    assert component.n_closes == 0

    id = simulation.add_object(obj, id=1)
    assert id == 1

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 0
    assert component.n_resets == 0
    assert component.n_removes == 0
    assert component.n_closes == 0

    simulation.step()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 0
    assert component.n_removes == 0
    assert component.n_closes == 0

    simulation.reset()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 1
    assert component.n_removes == 0
    assert component.n_closes == 0

    simulation.remove_object(obj)

    assert component.is_removed

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 1
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.step()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 1
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.reset()

    assert not component.is_removed

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 2
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.step()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 2
    assert component.n_resets == 2
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.close()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 2
    assert component.n_resets == 2
    assert component.n_removes == 2
    assert component.n_closes == 1


def test_generator_component_in_simulation():

    simulation = sim.Simulation()

    obj = DummyObject()

    component = DummyComponent(order=sim.GeneratorComponent.default_config().order)
    obj.add_component(component)

    assert obj.exist_component('DummyComponent')
    assert component == obj.get_component('DummyComponent')
    assert component == obj.DummyComponent
    assert component.order >= 0

    assert component.n_adds_to_simulation == 0
    assert component.n_adds_to_object == 1
    assert component.n_steps == 0
    assert component.n_resets == 0
    assert component.n_removes == 0
    assert component.n_closes == 0

    id = simulation.add_object(obj, id=1)
    assert id == 1

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 0
    assert component.n_resets == 0
    assert component.n_removes == 0
    assert component.n_closes == 0

    simulation.step()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 0
    assert component.n_removes == 0
    assert component.n_closes == 0

    simulation.reset()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 1
    assert component.n_removes == 0
    assert component.n_closes == 0

    simulation.remove_object(obj)

    assert component.is_removed

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 1
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.step()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 1
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.reset()

    assert not component.is_removed

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 1
    assert component.n_resets == 2
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.step()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 2
    assert component.n_resets == 2
    assert component.n_removes == 1
    assert component.n_closes == 0

    simulation.close()

    assert component.n_adds_to_simulation == 1
    assert component.n_adds_to_object == 1
    assert component.n_steps == 2
    assert component.n_resets == 2
    assert component.n_removes == 2
    assert component.n_closes == 1


def test_component_step_order():
    simulation = sim.Simulation()

    obj = DummyObject()
    simulation.add_object(obj, id=1)

    generator_component = DummyComponent()
    obj.add_component(generator_component, name='gen', order=50)

    standard_component = DummyComponent()
    obj.add_component(standard_component, name='std', order=100)

    sensor_component = DummyComponent()
    obj.add_component(sensor_component, name='sen', order=150)

    assert obj.exist_component('gen')
    assert obj.exist_component('std')
    assert obj.exist_component('sen')

    assert generator_component == obj.get_component('gen')
    assert standard_component == obj.get_component('std')
    assert sensor_component == obj.get_component('sen')

    simulation.step()

    assert generator_component.last_step_time < standard_component.last_step_time
    assert standard_component.last_step_time < sensor_component.last_step_time