import mpi_sim as sim

class DummyObject(sim.Object):

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.n_adds = 0
        self.n_steps = 0
        self.n_resets = 0
        self.n_removes = 0
        self.n_closes = 0

    def _add(self):
        self.n_adds += 1

    def _reset(self):
        self.n_resets += 1

    def _step(self):
        self.n_steps += 1

    def _remove(self):
        self.n_removes += 1

    def _close(self):
        self.n_closes += 1


def test_object_in_simulation():

    simulation = sim.Simulation()

    obj = DummyObject()

    assert obj.n_adds == 0
    assert obj.n_steps == 0
    assert obj.n_resets == 0
    assert obj.n_removes == 0
    assert obj.n_closes == 0

    id = simulation.add_object(obj, id=1)
    assert id == 1

    assert obj == simulation.objects[1]

    assert obj.n_adds == 1
    assert obj.n_steps == 0
    assert obj.n_resets == 0
    assert obj.n_removes == 0
    assert obj.n_closes == 0

    simulation.step()

    assert obj.n_adds == 1
    assert obj.n_steps == 1
    assert obj.n_resets == 0
    assert obj.n_removes == 0
    assert obj.n_closes == 0

    simulation.reset()

    assert obj.n_adds == 1
    assert obj.n_steps == 1
    assert obj.n_resets == 1
    assert obj.n_removes == 0
    assert obj.n_closes == 0

    simulation.remove_object(obj)

    assert obj.is_removed

    assert obj.n_adds == 1
    assert obj.n_steps == 1
    assert obj.n_resets == 1
    assert obj.n_removes == 1
    assert obj.n_closes == 0

    simulation.step()

    assert obj.n_adds == 1
    assert obj.n_steps == 1
    assert obj.n_resets == 1
    assert obj.n_removes == 1
    assert obj.n_closes == 0

    simulation.reset()

    assert not obj.is_removed

    assert obj.n_adds == 1
    assert obj.n_steps == 1
    assert obj.n_resets == 2
    assert obj.n_removes == 1
    assert obj.n_closes == 0

    simulation.step()

    assert obj.n_adds == 1
    assert obj.n_steps == 2
    assert obj.n_resets == 2
    assert obj.n_removes == 1
    assert obj.n_closes == 0

    simulation.close()

    assert obj.n_adds == 1
    assert obj.n_steps == 2
    assert obj.n_resets == 2
    assert obj.n_removes == 2
    assert obj.n_closes == 1