import mpi_sim


def test_create_simulation_from_config():

    world_config = {
        'objects': [
            {'type': 'Wall', 'id': 1},
            {'type': 'Wall', 'id': 2},
            {'type': 'Human', 'id': 3},
            {'type': 'Human', 'id': 4, 'components': [{'type': 'SpeechGenerator', 'name': 'speech'}]},
        ],
        'processes': [
            {'type': 'GUI'},
        ]
    }

    sim = mpi_sim.Simulation(world_config=world_config)

    ###############
    # check objects
    assert isinstance(sim.objects[1], mpi_sim.objects.Wall)
    assert isinstance(sim.objects[2], mpi_sim.objects.Wall)
    assert isinstance(sim.objects[3], mpi_sim.objects.Human)
    assert isinstance(sim.objects[4], mpi_sim.objects.Human)

    obj = sim.objects[4]
    assert isinstance(obj.components['speech'], mpi_sim.components.SpeechGenerator)

    ###############
    # check processes
    assert isinstance(sim.processes['gui'], mpi_sim.processes.GUI)
