import mpi_sim
from mpi_sim.core.object import Object
from mpi_sim.utils import object_filter


class DummyObject(Object):
    pass


def test_object_filter():

    obj1 = DummyObject(
        id=10,
        properties=['p1', 'p2']
    )

    obj2 = DummyObject(
        id=11,
        properties=['p1', 'p2']
    )

    sim = mpi_sim.Simulation()
    sim.add_object(obj1)
    sim.add_object(obj2)

    assert object_filter(obj1, ids=10)
    assert object_filter(obj1, ids=[10, 11])
    assert not object_filter(obj1, ids=11)

    assert object_filter(obj1, properties='p2')
    assert object_filter(obj1, properties='p1')
    assert object_filter(obj1, properties=['p2', 'p3'])
    assert not object_filter(obj1, properties=['p0', 'p3'])

    assert object_filter(obj1, objects=obj1)
    assert not object_filter(obj1, objects=obj2)
    assert object_filter(obj1, objects=[obj1, obj2])

    assert object_filter(obj1, types=DummyObject)
    assert not object_filter(obj1, types=mpi_sim.objects.Chair)
    assert object_filter(obj1, types=[mpi_sim.objects.Chair, DummyObject])
