import mpi_sim


def test_readonly_dict():

    my_dict = mpi_sim.utils.ReadOnlyDict({1: '1', 2: '2', 3: '3'})

    assert len(my_dict) == 3

    # iterate over list
    for key, data in my_dict.items():
        if key == 1:
            assert data == '1'
        if key == 2:
            assert data == '2'
        if key == 3:
            assert data == '3'
        if key < 1 or key > 3:
            raise Exception('Invalid number of elements')

    # access elements elementwise
    assert my_dict[1] == '1'
    assert my_dict[2] == '2'
    assert my_dict[3] == '3'

    ######
    # check for exceptions

    # changing elements
    is_exception = False
    try:
        my_dict[1] = 2
    except:
        is_exception = True
    if not is_exception:
        raise Exception('Changes to the list are not allowed!')

    # adding elements
    is_exception = False
    try:
        my_dict[0] = 2
    except:
        is_exception = True
    if not is_exception:
        raise Exception('Changes to the list are not allowed!')

