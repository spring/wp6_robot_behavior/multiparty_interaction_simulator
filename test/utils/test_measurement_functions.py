import mpi_sim
import numpy as np
from numpy.testing import assert_array_almost_equal
import Box2D


def test_constrain_angle():
    assert mpi_sim.utils.constraint_angle(0.0) == 0.0
    assert mpi_sim.utils.constraint_angle(np.pi) == np.pi
    assert mpi_sim.utils.constraint_angle(np.pi * 3) == np.pi or mpi_sim.utils.constraint_angle(np.pi * 3) == -np.pi
    assert mpi_sim.utils.constraint_angle(np.pi * -3) == np.pi or mpi_sim.utils.constraint_angle(np.pi * -3) == -np.pi
    assert mpi_sim.utils.constraint_angle(-np.pi) == -np.pi
    assert mpi_sim.utils.constraint_angle(-np.pi - 0.1) == np.pi - 0.1
    assert mpi_sim.utils.constraint_angle(np.pi + 0.1) == -np.pi + 0.1


def test_angle_difference():
    assert mpi_sim.utils.angle_difference(0.0, -0.1) == -0.1
    assert mpi_sim.utils.angle_difference(0.0, 0.1) == 0.1
    assert mpi_sim.utils.angle_difference(np.pi / 2, -np.pi / 2) == -np.pi
    assert mpi_sim.utils.angle_difference(np.pi, -np.pi / 2) == np.pi / 2
    assert mpi_sim.utils.angle_difference(-np.pi / 2, np.pi) == -np.pi / 2


def test_angle_to_point():

    human1 = mpi_sim.objects.Human(position=[0., 0.], orientation=0.0)
    # human2 = mpi_sim.objects.Human(position=[0., 2.], orientation=0.0)
    sim = mpi_sim.Simulation(world_size=[[-5., 5.], [-5., 5.]])
    sim.add_object(human1)
    # sim.add_object(human2)

    sim.step()

    # test positions that are clockwise around human 1
    assert mpi_sim.utils.measure_relative_angle(human1, [0., 0.]) == 0.0
    assert mpi_sim.utils.measure_relative_angle(human1, [0., 1.]) == 0.0
    assert mpi_sim.utils.measure_relative_angle(human1, [1., 1.]) == -np.pi / 4
    assert mpi_sim.utils.measure_relative_angle(human1, [1., 0.]) == -np.pi / 2
    assert mpi_sim.utils.measure_relative_angle(human1, [1., -1.]) == -np.pi * 3 / 4
    assert mpi_sim.utils.measure_relative_angle(human1, [0., -1.]) in [-np.pi, np.pi]
    assert mpi_sim.utils.measure_relative_angle(human1, [-1., -1.]) == np.pi * 3 / 4
    assert mpi_sim.utils.measure_relative_angle(human1, [-1., 0.]) == np.pi / 2
    assert mpi_sim.utils.measure_relative_angle(human1, [-1., 1.]) == np.pi / 4

    # give a different orientation to human 1
    human1.orientation = -np.pi / 2
    sim.step()

    # test positions that are clockwise around human 1
    assert mpi_sim.utils.measure_relative_angle(human1, [0., 0.]) == 0.0
    assert_array_almost_equal(mpi_sim.utils.measure_relative_angle(human1, [0., 1.]), np.pi / 2)
    assert_array_almost_equal(mpi_sim.utils.measure_relative_angle(human1, [1., 1.]), np.pi / 4)
    assert_array_almost_equal(mpi_sim.utils.measure_relative_angle(human1, [1., 0.]), 0.0)
    assert_array_almost_equal(mpi_sim.utils.measure_relative_angle(human1, [1., -1.]), -np.pi / 4)
    assert_array_almost_equal(mpi_sim.utils.measure_relative_angle(human1, [0., -1.]), -np.pi / 2)
    assert_array_almost_equal(mpi_sim.utils.measure_relative_angle(human1, [-1., -1.]), -np.pi * 3 / 4)
    assert_array_almost_equal(mpi_sim.utils.measure_relative_angle(human1, [-1., 0.]), -np.pi)
    assert_array_almost_equal(mpi_sim.utils.measure_relative_angle(human1, [-1., 1.]), np.pi * 3 / 4)


def test_measure_center_distance():

    world_config = {
        'objects': [
            {'type': 'Human', 'id': 1, 'position': [1.0, 1.0]},
            {'type': 'Human', 'id': 2, 'position': [1.0, 2.0]}
        ]
    }

    simulation = mpi_sim.Simulation(world_config=world_config)

    human_1 = simulation.objects[1]
    human_2 = simulation.objects[2]

    # between objects
    assert mpi_sim.utils.measure_center_distance(human_1, human_2) == 1.0
    assert mpi_sim.utils.measure_center_distance(human_2, human_1) == 1.0

    # between points
    assert mpi_sim.utils.measure_center_distance([0., 0.], [0., 10.]) == 10.0
    assert mpi_sim.utils.measure_center_distance([0., 10.], [0., 0.]) == 10.0

    # between points and objects
    assert mpi_sim.utils.measure_center_distance([0., 1.], human_1) == 1.0
    assert mpi_sim.utils.measure_center_distance(human_1, [0., 1.]) == 1.0


class ObjRectangle(mpi_sim.Object):
    '''Object that has a single rectangular body'''

    def _create_in_box2d_world(self, box2d_world):
        self.box2d_body = box2d_world.CreateDynamicBody(
            position=(0.0, 0.0),
            angle=0.0,
            fixtures=[
                Box2D.b2FixtureDef(
                    shape=Box2D.b2PolygonShape(box=(0.5, 0.5)),
                    density=1.0
                ),
            ],
        )
        self.box2d_body.mass = 1.0

        return self.box2d_body


class ObjCirclesAndRectangle(mpi_sim.Object):
    """
        Object that has two bodies:
            1) rectangle with a half circle on the right,
            2) circle on top of the rectangle
    """

    def _create_in_box2d_world(self, box2d_world):
        self.box2d_rect = box2d_world.CreateDynamicBody(
            position=(0.0, 0.0),
            angle=0.0,
            fixtures=[
                Box2D.b2FixtureDef(
                    shape=Box2D.b2PolygonShape(box=(0.5, 0.5)),
                    density=1.0
                ),
                Box2D.b2FixtureDef(
                    shape=Box2D.b2CircleShape(radius=0.5, pos=(-0.5, .0)),
                    density=1.0
                ),
            ],
        )
        self.box2d_circ = box2d_world.CreateDynamicBody(
            position=(0.0, 1.0),
            angle=0.0,
            fixtures=[
                Box2D.b2FixtureDef(
                    shape=Box2D.b2CircleShape(radius=0.5),
                    density=1.0
                ),
            ],
        )
        self.box2d_rect.mass = 1.0
        self.box2d_circ.mass = 1.0

        return self.box2d_rect, self.box2d_circ


def test_measure_distance():
    """Measurs the distance between two objects for which class exist above this method."""

    # create the simulation with the two test objects
    sim = mpi_sim.Simulation()

    rect_obj = ObjRectangle()
    sim.add_object(rect_obj)

    circ_rect_obj = ObjCirclesAndRectangle()
    sim.add_object(circ_rect_obj)

    ##########################
    # initial distance should be 0 as they overlap

    dist_1_2 = mpi_sim.utils.measure_distance(rect_obj, circ_rect_obj)
    assert dist_1_2.distance == 0.0

    ##########################
    # set rect to the right of the complex object

    rect_obj.box2d_body.position = (2.0, 0.0)
    dist_1_2 = mpi_sim.utils.measure_distance(rect_obj, circ_rect_obj)

    np.testing.assert_approx_equal(dist_1_2.distance, 1.0, significant=1)

    ##########################
    # set rect to the right / bottom of the complex object

    rect_obj.box2d_body.position = (2.0, -2.0)
    dist_1_2 = mpi_sim.utils.measure_distance(rect_obj, circ_rect_obj)

    np.testing.assert_approx_equal(dist_1_2.distance, np.sqrt(2), significant=1)
    np.testing.assert_array_almost_equal(dist_1_2.point_a, [1.5, -1.5], decimal=1)
    np.testing.assert_array_almost_equal(dist_1_2.point_b, [0.5, -0.5], decimal=1)

    ##########################
    # set rect to the left of the complex object

    rect_obj.box2d_body.position = (-2.5, 0.0)
    dist_1_2 = mpi_sim.utils.measure_distance(rect_obj, circ_rect_obj)

    np.testing.assert_approx_equal(dist_1_2.distance, 1.0, significant=1)
    np.testing.assert_array_almost_equal(dist_1_2.point_a, [-2.0, 0.0], decimal=1)
    np.testing.assert_array_almost_equal(dist_1_2.point_b, [-1.0, 0.0], decimal=1)

    ##########################
    # set rect to the top of the complex object

    rect_obj.box2d_body.position = (0.0, 3.0)
    dist_1_2 = mpi_sim.utils.measure_distance(rect_obj, circ_rect_obj)

    np.testing.assert_approx_equal(dist_1_2.distance, 1.0, significant=1)
    np.testing.assert_array_almost_equal(dist_1_2.point_a, [0.0, 2.5], decimal=1)
    np.testing.assert_array_almost_equal(dist_1_2.point_b, [0.0, 1.5], decimal=1)



# def test_measure_angle():
#     # TODO (Alex):
#     pass
#
#     world_config = {
#         'objects': [
#             {'type': 'Human', 'id': 1, 'position': [1.0, 1.0], 'orientation': 0.0},
#             {'type': 'Human', 'id': 1, 'position': [1.0, 2.0], 'orientation': 0.0}
#         ]
#     }
#
#     simulation = sim.Simulation(world_config=world_config)
#
#     human_1 = simulation.get_object(1)
#     human_2 = simulation.get_object(2)
#
#     # TODO: test different angles
#     assert sim.utils.measure_angle(human_1, human_2) == None
#     assert sim.utils.measure_angle(human_2, human_1) == None

