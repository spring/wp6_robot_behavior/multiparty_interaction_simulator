import mpi_sim
import numpy as np
import numpy.testing as np_test
from Box2D import b2PolygonShape, b2CircleShape
from mpi_sim.utils import create_occupancy_grid_map
from mpi_sim.utils import create_local_perspective_occupancy_grid_map
from mpi_sim.utils import get_occupancy_grid_map_shape
from mpi_sim.utils import OccupancyGridMap
from mpi_sim.utils import transform_position_to_map_coordinate
from mpi_sim.utils import transform_map_coordinate_to_position

class PolygonObject(mpi_sim.Object):

    @staticmethod
    def default_config():
        dc = mpi_sim.Object.default_config()
        dc.position = (0, 0)
        dc.width = 1
        dc.height = 1
        return dc


    def _create_in_box2d_world(self, box2d_world):
        return box2d_world.CreateStaticBody(
            position=self.config.position,
            shapes=b2PolygonShape(box=(self.config.width, self.config.height)),
        )


class CircleObject(mpi_sim.Object):

    @staticmethod
    def default_config():
        dc = mpi_sim.Object.default_config()
        dc.position = (0, 0)
        dc.radius = 1
        return dc


    def _create_in_box2d_world(self, box2d_world):
        return box2d_world.CreateStaticBody(
            position=self.config.position,
            shapes=b2CircleShape(radius=self.config.radius),
        )


def test_polygon_one():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 10), (0, 5)))
    sim.add_object(PolygonObject(position=(2,2), width=0.5, height=0.5))

    # create the map
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=1.0)

    expected_map = [
        [0., 0., 0., 0., 0.],
        [0., 1., 1., 0., 0.],
        [0., 1., 1., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
    ]
    np_test.assert_array_almost_equal(expected_map, occupancy_grid_map.map)
    assert occupancy_grid_map.resolution == 1.0
    assert occupancy_grid_map.area == ((0, 10), (0, 5))


def test_polygon_two():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 5), (0, 5)))
    sim.add_object(PolygonObject(position=(2,2), width=1.5, height=0.5))

    # create the map
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=1.0)

    expected_map = [
        [0., 1., 1., 0., 0.],
        [0., 1., 1., 0., 0.],
        [0., 1., 1., 0., 0.],
        [0., 1., 1., 0., 0.],
        [0., 0., 0., 0., 0.],
    ]
    np_test.assert_array_almost_equal(expected_map, occupancy_grid_map.map)
    assert occupancy_grid_map.resolution == 1.0
    assert occupancy_grid_map.area == ((0, 5), (0, 5))


def test_polygon_three():
    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 5), (0, 5)))
    sim.add_object(PolygonObject(position=(2, 2), width=0.5, height=1.5))

    # create the map
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=1.0)

    expected_map = [
        [0., 0., 0., 0., 0.],
        [1., 1., 1., 1., 0.],
        [1., 1., 1., 1., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
    ]
    np_test.assert_array_almost_equal(expected_map, occupancy_grid_map.map)
    assert occupancy_grid_map.resolution == 1.0
    assert occupancy_grid_map.area == ((0, 5), (0, 5))


def test_polygon_four():
    # outside of visible area

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 5), (0, 5)))
    sim.add_object(PolygonObject(position=(0, 0), width=0.5, height=1.5))

    # create the map
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=1.0)

    expected_map = [
        [1., 1., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
    ]
    np_test.assert_array_almost_equal(expected_map, occupancy_grid_map.map)
    assert occupancy_grid_map.resolution == 1.0
    assert occupancy_grid_map.area == ((0, 5), (0, 5))


def test_circle_one():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 5), (0, 5)))
    sim.add_object(CircleObject(position=(3, 3), radius=0.5))

    # create the map
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=1.0)

    expected_map = [
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0.],
        [0., 0., 1., 1., 0.],
        [0., 0., 1., 1., 0.],
        [0., 0., 0., 0., 0.],
    ]
    np_test.assert_array_almost_equal(expected_map, occupancy_grid_map.map)
    assert occupancy_grid_map.resolution == 1.0
    assert occupancy_grid_map.area == ((0, 5), (0, 5))


def test_circle_two():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 5), (0, 5)))
    sim.add_object(CircleObject(position=(3, 5), radius=1.5))

    # create the map
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=1.0)

    # print('')
    # print(occupancy_grid_map)

    expected_map = [
        [0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 1.],
        [0., 0., 0., 1., 1.],
        [0., 0., 0., 1., 1.],
        [0., 0., 0., 0., 1.],
    ]
    np_test.assert_array_almost_equal(expected_map, occupancy_grid_map.map)
    assert occupancy_grid_map.resolution == 1.0
    assert occupancy_grid_map.area == ((0, 5), (0, 5))


def test_circle_and_polygon_one():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 5), (0, 5)))
    sim.add_object(CircleObject(position=(3, 5), radius=1.5))
    sim.add_object(PolygonObject(position=(2, 2), width=0.5, height=0.5))

    # create the map
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=1.0)

    # print('')
    # print(occupancy_grid_map)

    expected_map = [
        [0., 0., 0., 0., 0.],
        [0., 1., 1., 0., 1.],
        [0., 1., 1., 1., 1.],
        [0., 0., 0., 1., 1.],
        [0., 0., 0., 0., 1.],
    ]
    np_test.assert_array_almost_equal(expected_map, occupancy_grid_map.map)
    assert occupancy_grid_map.resolution == 1.0
    assert occupancy_grid_map.area == ((0, 5), (0, 5))


def test_circle_and_polygon_two():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 5), (0, 5)))
    sim.add_object(CircleObject(position=(3, 5), radius=1.5))
    sim.add_object(PolygonObject(position=(2, 2), width=1.5, height=1.5))

    # create the map
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=1.0)

    # print('')
    # print(occupancy_grid_map)

    expected_map = [
        [1., 1., 1., 1., 0.],
        [1., 1., 1., 1., 1.],
        [1., 1., 1., 1., 1.],
        [1., 1., 1., 1., 1.],
        [0., 0., 0., 0., 1.],
    ]
    np_test.assert_array_almost_equal(expected_map, occupancy_grid_map.map)
    assert occupancy_grid_map.resolution == 1.0
    assert occupancy_grid_map.area == ((0, 5), (0, 5))


def test_map_shape():

    #####################

    visible_area = ((0, 5), (0, 10))
    resolution = 1.0

    sim = mpi_sim.Simulation(visible_area=visible_area)
    map_shape = get_occupancy_grid_map_shape(area=visible_area, resolution=resolution)
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=resolution)
    assert map_shape[0] == occupancy_grid_map.map.shape[0]
    assert map_shape[1] == occupancy_grid_map.map.shape[1]

    #####################

    visible_area = ((0, 15), (0, 10))
    resolution = 0.5

    sim = mpi_sim.Simulation(visible_area=visible_area)
    map_shape = get_occupancy_grid_map_shape(area=visible_area, resolution=resolution)
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=resolution)
    assert map_shape[0] == occupancy_grid_map.map.shape[0]
    assert map_shape[1] == occupancy_grid_map.map.shape[1]

    #####################

    visible_area = ((0, 15.1), (0, 10.2))
    resolution = 0.5

    sim = mpi_sim.Simulation(visible_area=visible_area)
    map_shape = get_occupancy_grid_map_shape(area=visible_area, resolution=resolution)
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=resolution)
    assert map_shape[0] == occupancy_grid_map.map.shape[0]
    assert map_shape[1] == occupancy_grid_map.map.shape[1]

    #####################

    visible_area = ((0, 15.6), (0, 6.8))
    resolution = 0.5

    sim = mpi_sim.Simulation(visible_area=visible_area)
    map_shape = get_occupancy_grid_map_shape(area=visible_area, resolution=resolution)
    occupancy_grid_map = create_occupancy_grid_map(simulation=sim, resolution=resolution)
    assert map_shape[0] == occupancy_grid_map.map.shape[0]
    assert map_shape[1] == occupancy_grid_map.map.shape[1]


def test_local_perspective_no_rotation():

    # map in human view form with axes on bottom and y axes on the left
    global_map = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
    ])
    global_map = np.flip(global_map, axis=0).transpose()  # in correct (x,y) array form
    global_map = OccupancyGridMap(map=global_map, area=((0, 10), (0, 5)), resolution=1.0)

    local_map = create_local_perspective_occupancy_grid_map(global_map=global_map, position=(0.0, 0.0), orientation=0.0, depth=3)

    expected_local_map = np.array([
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local_map = np.flip(expected_local_map, axis=0).transpose()

    np_test.assert_array_almost_equal(expected_local_map, local_map.map)


def test_local_perspective_east_rotation():

    # map in human view form with axes on bottom and y axes on the left
    global_map = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
    ])
    global_map = np.flip(global_map, axis=0).transpose()  # in correct (x,y) array form
    global_map = OccupancyGridMap(map=global_map, area=((0, 10), (0, 5)), resolution=1.0)

    local_map = create_local_perspective_occupancy_grid_map(global_map=global_map, position=(0.0, 0.0), orientation=-np.pi / 2, depth=3)

    expected_local_map = np.array([
        [0., 1., 1., 1., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local_map = np.flip(expected_local_map, axis=0).transpose()

    np_test.assert_array_almost_equal(expected_local_map, local_map.map)


def test_local_perspective_west_rotation():

    # map in human view form with axes on bottom and y axes on the left
    global_map = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
    ])
    global_map = np.flip(global_map, axis=0).transpose()  # in correct (x,y) array form
    global_map = OccupancyGridMap(map=global_map, area=((0, 10), (0, 5)), resolution=1.0)

    local_map = create_local_perspective_occupancy_grid_map(global_map=global_map, position=(0.0, 0.0), orientation=np.pi / 2, depth=3)

    expected_local_map = np.array([
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local_map = np.flip(expected_local_map, axis=0).transpose()

    np_test.assert_array_almost_equal(expected_local_map, local_map.map)


def test_local_perspective_south_rotation():

    # map in human view form with axes on bottom and y axes on the left
    global_map = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
    ])
    global_map = np.flip(global_map, axis=0).transpose()  # in correct (x,y) array form
    global_map = OccupancyGridMap(map=global_map, area=((0, 10), (0, 5)), resolution=1.0)

    local_map = create_local_perspective_occupancy_grid_map(global_map=global_map, position=(0.0, 0.0), orientation=np.pi, depth=3)

    expected_local_map = np.array([
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local_map = np.flip(expected_local_map, axis=0).transpose()

    np_test.assert_array_almost_equal(expected_local_map, local_map.map)



def test_local_perspective_no_rotation_area2():

    # map in human view form with axes on bottom and y axes on the left
    global_map = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 1., 1., 1., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 1., 0., 0., 0., 0., 0., 0.],
        [0., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
    ])
    global_map = np.flip(global_map, axis=0).transpose()  # in correct (x,y) array form
    global_map = OccupancyGridMap(map=global_map, area=((-5, 5), (-2, 3)), resolution=1.0)

    local_map = create_local_perspective_occupancy_grid_map(global_map=global_map, position=(-4.0, -1.0), orientation=0.0, depth=3)

    expected_local_map = np.array([
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 1., 1., 1., 1.],
        [0., 0., 0., 1., 1., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local_map = np.flip(expected_local_map, axis=0).transpose()

    lm = np.flip(local_map.map.transpose(), axis=0)

    np_test.assert_array_almost_equal(expected_local_map, local_map.map)


def test_local_perspective_no_rotation_area3():

    # map in human view form with axes on bottom and y axes on the left
    global_map = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 1., 1., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
    ])
    global_map = np.flip(global_map, axis=0).transpose()  # in correct (x,y) array form
    global_map = OccupancyGridMap(map=global_map, area=((-5, 5), (-2, 3)), resolution=1.0)

    local_map = create_local_perspective_occupancy_grid_map(global_map=global_map, position=(-3.0, -1.0), orientation=0.0, depth=3)

    expected_local_map = np.array([
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 0., 1., 1.],
        [0., 0., 0., 1., 1., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local_map = np.flip(expected_local_map, axis=0).transpose()

    lm = np.flip(local_map.map.transpose(), axis=0)

    np_test.assert_array_almost_equal(expected_local_map, local_map.map)


def test_local_perspective_rotation_east_area3():

    # map in human view form with axes on bottom and y axes on the left
    global_map = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 1., 1., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
    ])
    global_map = np.flip(global_map, axis=0).transpose()  # in correct (x,y) array form
    global_map = OccupancyGridMap(map=global_map, area=((-5, 5), (-2, 3)), resolution=1.0)

    local_map = create_local_perspective_occupancy_grid_map(global_map=global_map, position=(-3.0, -1.0), orientation=-np.pi/2, depth=3)

    expected_local_map = np.array([
        [0., 1., 1., 0., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 1., 0., 1., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local_map = np.flip(expected_local_map, axis=0).transpose()

    lm = np.flip(local_map.map.transpose(), axis=0)

    np_test.assert_array_almost_equal(expected_local_map, local_map.map)


def test_local_perspective_rotation_west_area3():

    # map in human view form with axes on bottom and y axes on the left
    global_map = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 1., 1., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
    ])
    global_map = np.flip(global_map, axis=0).transpose()  # in correct (x,y) array form
    global_map = OccupancyGridMap(map=global_map, area=((-5, 5), (-2, 3)), resolution=1.0)

    local_map = create_local_perspective_occupancy_grid_map(global_map=global_map, position=(-3.0, -1.0), orientation=np.pi/2, depth=3)

    expected_local_map = np.array([
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 1.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 0., 1., 1.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local_map = np.flip(expected_local_map, axis=0).transpose()

    # lm = np.flip(local_map.map.transpose(), axis=0)

    np_test.assert_array_almost_equal(expected_local_map, local_map.map)


def test_transform_position_to_map_coordinate():

    pos = transform_position_to_map_coordinate(
        position=(0.0, 0.0),
        map_area=((0, 10), (0, 5)),
        map_resolution=1.0
    )
    assert pos == (0., 0.)

    pos = transform_position_to_map_coordinate(
        position=(1.0, 2.0),
        map_area=((0, 10), (0, 5)),
        map_resolution=1.0
    )
    assert pos == (1., 2.)

    pos = transform_position_to_map_coordinate(
        position=(-1.0, -2.0),
        map_area=((0, 10), (0, 5)),
        map_resolution=1.0
    )
    assert pos == (-1., -2.)

    pos = transform_position_to_map_coordinate(
        position=(-1.0, -2.0),
        map_area=((0, 10), (0, 5)),
        map_resolution=0.5
    )
    assert pos == (-2., -4.)

    ################
    # more complex area

    pos = transform_position_to_map_coordinate(
        position=(-5.0, -1.0),
        map_area=((-5, 5), (-1, 1)),
        map_resolution=1.0
    )
    assert pos == (0., 0.)


def test_transform_map_coordinate_to_position():

    pos = transform_map_coordinate_to_position(
        map_coordinate=(0, 0),
        map_area=((0, 10), (0, 5)),
        map_resolution=1.0
    )
    assert pos == (0., 0.)

    pos = transform_map_coordinate_to_position(
        map_coordinate=(1, 2),
        map_area=((0, 10), (0, 5)),
        map_resolution=1.0
    )
    assert pos == (1., 2.)

    pos = transform_map_coordinate_to_position(
        map_coordinate=(-1, -2),
        map_area=((0, 10), (0, 5)),
        map_resolution=1.0
    )
    assert pos == (-1., -2.)

    pos = transform_map_coordinate_to_position(
        map_coordinate=(-2, -4 ),
        map_area=((0, 10), (0, 5)),
        map_resolution=0.5
    )
    assert pos == (-1., -2.)

    ################
    # more complex area

    pos = transform_map_coordinate_to_position(
        map_coordinate=(0., 0.),
        map_area=((-5, 5), (-1, 1)),
        map_resolution=1.0
    )
    assert pos == (-5.0, -1.0)
