import mpi_sim


def test_as_list():

    # scalar input
    x = 1
    x_as_list = mpi_sim.utils.as_list(x)

    assert isinstance(x_as_list, list)
    assert len(x_as_list) == 1
    assert x_as_list[0] == x

    # list input
    x = [1]
    x_as_list = mpi_sim.utils.as_list(x)
    assert x == x_as_list

    # list input
    x = [1,2]
    x_as_list = mpi_sim.utils.as_list(x)
    assert x == x_as_list
