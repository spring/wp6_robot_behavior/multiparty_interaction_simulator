import mpi_sim


def test_readonly_list():

    my_list = mpi_sim.utils.ReadOnlyList([1,'hello',3])

    assert len(my_list) == 3

    # iterate over list
    for idx, data in enumerate(my_list):
        if idx == 0:
            assert data == 1
        if idx == 1:
            assert data == 'hello'
        if idx == 2:
            assert data == 3
        if idx < 0 or idx > 2:
            raise Exception('Invalid number of elements')

    # access elements elementwise
    assert my_list[0] == 1
    assert my_list[1] == 'hello'
    assert my_list[2] == 3

    ######
    # check for exceptions

    # changing elements
    is_exception = False
    try:
        my_list[0] = 2
    except:
        is_exception = True
    if not is_exception:
        raise Exception('Changes to the list are not allowed!')

    # adding elements
    is_exception = False
    try:
        my_list += 2
    except:
        is_exception = True
    if not is_exception:
        raise Exception('Changes to the list are not allowed!')

    # adding elements
    is_exception = False
    try:
        my_list.append(2)
    except:
        is_exception = True
    if not is_exception:
        raise Exception('Changes to the list are not allowed!')


