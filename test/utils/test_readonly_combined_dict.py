import mpi_sim


def test_readonly_combined_dict():

    my_dict = mpi_sim.utils.ReadOnlyCombinedDict([{1: '1', 2: '2'}, {3: '3'}])

    assert len(my_dict) == 3

    # iterate over dict
    for key, data in my_dict.items():
        if key == 1:
            assert data == '1'
        if key == 2:
            assert data == '2'
        if key == 3:
            assert data == '3'
        if key < 1 or key > 3:
            raise Exception('Invalid number of elements')

    # iterate over dict
    for data in my_dict.values():
        assert data in ['1', '2', '3']

    # access elements elementwise
    assert my_dict[1] == '1'
    assert my_dict[2] == '2'
    assert my_dict[3] == '3'

    ######
    # check for exceptions

    # changing elements
    is_exception = False
    try:
        my_dict[1] = 2
    except:
        is_exception = True
    if not is_exception:
        raise Exception('Changes to the dict are not allowed!')

    # adding elements
    is_exception = False
    try:
        my_dict[0] = 2
    except:
        is_exception = True
    if not is_exception:
        raise Exception('Changes to the dict are not allowed!')



def test_copy_behavior():
    # check that that dictionary makes a shallow copy and not a deep copy

    class DummyClass:
        data = None

    obj1 = DummyClass()
    obj1.data = '1'

    obj2 = DummyClass()
    obj2.data = '2'

    obj3 = DummyClass()
    obj3.data = '3'

    dict_1 = {1: obj1, 2: obj2}
    dict_2 = {3: obj3}

    d = mpi_sim.utils.ReadOnlyCombinedDict([dict_1, dict_2])

    assert d[1].data == '1'

    obj1.data = 'changed'

    assert d[1].data == 'changed'
