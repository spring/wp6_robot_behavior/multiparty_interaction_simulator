import mpi_sim


def test_get_dict_items_by_type():

    class DummyClass1:
        name = None

    class DummyClass2:
        name = None

    class DummyClass3:
        name = None

    c1_1 = DummyClass1()
    c1_1.name = 'c1_1'

    c1_2 = DummyClass1()
    c1_2.name = 'c1_2'

    c2_1 = DummyClass2()
    c2_1.name = 'c2_1'

    c2_2 = DummyClass2()
    c2_2.name = 'c2_2'

    c3_1 = DummyClass3()
    c3_1.name = 'c3_1'

    c3_2 = DummyClass3()
    c3_2.name = 'c3_2'

    d = dict(
        c1_1 = c1_1,
        c1_2 = c1_2,
        c2_1 = c2_1,
        c2_2 = c2_2,
        c3_1 = c3_1,
        c3_2 = c3_2,
    )

    items = mpi_sim.utils.get_dict_items_by_type(d, int)
    assert len(items) == 0

    items = mpi_sim.utils.get_dict_items_by_type(d, DummyClass1)
    assert len(items) == 2
    assert c1_1 in items
    assert c1_2 in items

    items = mpi_sim.utils.get_dict_items_by_type(d, [DummyClass1, DummyClass2])
    assert len(items) == 4
    assert c1_1 in items
    assert c1_2 in items
    assert c2_1 in items
    assert c2_2 in items