import numpy as np
import mpi_sim
from mpi_sim.utils import sample_value
from mpi_sim.utils import sample_vector
from mpi_sim.utils import mutate_value
import numbers


# def test_p():
#
#     n = 100000
#
#     import time
#
#     nums = [1]
#
#     tic = time.time()
#     y = 0
#     for x in range(n):
#
#         if len(nums) == 1:
#             y += nums[0]
#         else:
#             y *= nums[0]
#
#     toc = time.time()
#     print('')
#     print(toc - tic)
#
#
#
#     tic = time.time()
#     y = 0
#     for x in range(n):
#         y += np.random.choice(nums)
#
#     toc = time.time()
#     print(toc - tic)


def test_RandomNumberSampling():

    # default
    rand_sampling = mpi_sim.utils.RandomNumberSampling()

    x = rand_sampling.rand()
    assert isinstance(x, numbers.Number)
    assert 0.0 <= x <= 1.0

    x = rand_sampling.rand(5)
    assert len(x) == 5
    assert np.all(np.logical_and(0.0 <= x, x <= 1))

    # repeat (by default true)
    rand_sampling = mpi_sim.utils.RandomNumberSampling(n_samples=3)

    x1 = rand_sampling.rand(3)
    x2 = rand_sampling.rand(3)
    np.testing.assert_array_equal(x1, x2)

    # no repeat (by default true)
    rand_sampling = mpi_sim.utils.RandomNumberSampling(n_samples=3, is_repeat=False)

    x1 = rand_sampling.rand(3)
    x2 = rand_sampling.rand(3)
    assert np.all(x1 != x2)




def test_sample_value():

    num_of_test = 10
    rand = np.random.RandomState()

    # check scalars
    for i in range(num_of_test):

        val = sample_value(rand, 1,)
        assert val == 1

        val = sample_value(rand, 1.3)
        assert val == 1.3

        val = sample_value(rand, -1.3)
        assert val == -1.3

    # check sampling of continuous (min, max)
    for i in range(num_of_test):

        min_max = (0, 1)
        val = sample_value(rand, min_max)
        assert min_max[0] <= val <= min_max[1]

        min_max = (-1, 1)
        val = sample_value(rand, min_max)
        assert min_max[0] <= val <= min_max[1]

        min_max = (40, 50)
        val = sample_value(rand, min_max)
        assert min_max[0] <= val <= min_max[1]

    # check sampling of continuous ('continuous', min, max)
    for i in range(num_of_test):

        min_max = ('continuous', 0, 1)
        val = sample_value(rand, min_max)
        assert val >= min_max[1] and val <= min_max[2] and int(val) != val

        min_max = ('continuous', -1, 1)
        val = sample_value(rand, min_max)
        assert val >= min_max[1] and val <= min_max[2] and int(val) != val

        min_max = ('continuous', 40, 50)
        val = sample_value(rand, min_max)
        assert val >= min_max[1] and val <= min_max[2] and int(val) != val

    # check sampling of discrete ('discrete', min, max)
    for i in range(num_of_test):

        min_max = ('discrete', 0, 1)
        val = sample_value(rand, min_max)
        assert val in [0, 1]

        min_max = ('discrete', -1, 1)
        val = sample_value(rand, min_max)
        assert val in [-1, 0, 1]

        min_max = ('discrete', 40, 50)
        val = sample_value(rand, min_max)
        assert val in [40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50]

    # check sampling of item from list
    for i in range(num_of_test):

        lst = [0, 1, 2, 3]
        val = sample_value(rand, lst)
        assert val in lst

        lst = ['a', 'bb', 'ccc', 'dddd']
        val = sample_value(rand, lst)
        assert val in lst

        lst = [3, '22', (33, 34)]
        val = sample_value(rand, lst)
        assert val in lst

    for i in range(num_of_test):

        descr = dict()
        descr['type'] = 'continuous'
        descr['min'] = 0
        descr['max'] = 1
        val = sample_value(rand, descr)
        assert val >= descr['min'] and val <= descr['max'] and int(val) != val

        descr['type'] = 'continuous'
        descr['min'] = -10
        descr['max'] = 10
        val = sample_value(rand, descr)
        assert val >= descr['min'] and val <= descr['max'] and int(val) != val

        descr['type'] = 'discrete'
        descr['min'] = 0
        descr['max'] = 1
        val = sample_value(rand, descr)
        assert val in [0,1]

        descr['type'] = 'discrete'
        descr['min'] = 10
        descr['max'] = 15
        val = sample_value(rand, descr)
        assert val in [10,11,12,13,14,15]

    # userdefined function
    my_func = lambda m_rnd, a, b: a-b
    descr = ('function', my_func, 100, 50)
    val = sample_value(rand, descr)
    assert val == descr[2] - descr[3]

    my_func = lambda m_rnd, a, b: a-b
    descr = ('func', my_func, 100, 50)
    val = sample_value(rand, descr)
    assert val == descr[2] - descr[3]


def test_sample_discrete_value():

    # check if all possible discrete values can be found in the samples
    num_of_test = 10
    rand = np.random.RandomState(seed=2)

    descr = dict()
    descr['type'] = 'discrete'
    descr['min'] = 0
    descr['max'] = 2

    values = []

    for i in range(num_of_test):
        values.append(sample_value(rand, descr))

    assert 0 in values
    assert 1 in values
    assert 2 in values


def test_mutate_value():

    # gauss continuous

    val = mutate_value(1)
    assert val == 1

    val = mutate_value(1, config={'distribution': 'gauss', 'sigma': 0})
    assert val == 1

    for _ in range(100):
        val = mutate_value(1, config={'distribution': 'gauss', 'sigma': 1, 'min': 0.9, 'max': 1.1})
        assert 0.9 <= val <= 1.1

    val = mutate_value(1, distribution='gauss', sigma=0)
    assert val == 1

    for _ in range(100):
        val = mutate_value(1, distribution='gauss', sigma=1, min=0.9, max=1.1)
        assert 0.9 <= val <= 1.1

    # gauss discrete
    val = mutate_value(1.1, type='discrete')
    assert val == 1

    val = mutate_value(1.1, type='discrete', distribution='gauss', sigma=0)
    assert val == 1

    for _ in range(100):
        val = mutate_value(1, type='discrete', distribution='gauss', sigma=3, min=-0.5, max=2.5)
        assert val in [0, 1, 2]

    # vector
    val = mutate_value([1.1, 2.2], type='discrete')
    assert val == [1, 2]

    val = mutate_value(np.array([1.1, 2.2]), type='discrete')
    assert np.all(val == np.array([1, 2]))

    # mutation factor
    val = mutate_value(1, mutation_factor=0, config={'distribution': 'gauss', 'sigma': 1})
    assert val == 1


def test_sample_vector():

    rnd = np.random.RandomState()

    repetitions = 10

    for idx in range(repetitions):
        config = (3, (0, 2))
        vec = sample_vector(rnd, config)
        assert len(vec) == 3
        assert np.all(np.array(vec) >= 0) and np.all(np.array(vec) <= 2)

    for idx in range(repetitions):
        config = ((2,4), (-1, 2))
        vec = sample_vector(rnd, config)
        assert 2 <= len(vec) <= 4
        assert np.all(np.array(vec) >= -1) and np.all(np.array(vec) <= 2)