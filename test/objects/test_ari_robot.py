import mpi_sim
import numpy as np


def test_ari_navigation_go_to_human_simple():

    simulation = mpi_sim.Simulation(
        visible_area=((0., 10.), (0., 10.)),
        objects=[
            {'type': 'Wall', 'position': [6., 3.], 'orientation': 0., 'length': 6.},  # east
            {'type': 'Wall', 'position': [0., 3.], 'orientation': 0., 'length': 6.},  # west
            {'type': 'Wall', 'position': [3., 6.0], 'orientation': np.pi / 2, 'length': 6.2},  # south
            {'type': 'Wall', 'position': [3., 0.0], 'orientation': np.pi / 2, 'length': 6.2},  # north
        ],
        # processes=[{'type': 'GUI'}]
    )

    human = mpi_sim.objects.Human(position=[1., 1.], orientation=0)
    simulation.add_object(human)

    ari = mpi_sim.objects.ARIRobot(
        position=[4., 4.],
        orientation=np.pi,
    )
    simulation.add_object(ari)
    ari.navigation.set_go_towards_human(human)

    is_human_reached = False
    for step in range(700):
        simulation.step()

        if mpi_sim.utils.measure_center_distance(ari, human) < 1.5:
            is_human_reached = True
            break

    simulation.close()

    assert is_human_reached


def test_ari_navigation_go_to_human_with_obstacle():

    simulation = mpi_sim.Simulation(
        visible_area=((0., 10.), (0., 10.)),
        objects=[
            {'type': 'Wall', 'position': [6., 3.], 'orientation': 0., 'length': 6.},  # east
            {'type': 'Wall', 'position': [0., 3.], 'orientation': 0., 'length': 6.},  # west
            {'type': 'Wall', 'position': [3., 6.0], 'orientation': np.pi / 2, 'length': 6.2},  # south
            {'type': 'Wall', 'position': [3., 0.0], 'orientation': np.pi / 2, 'length': 6.2},  # north,
            {'type': 'Bench', 'position': [2., 3.7]}
        ],
        # processes=[{'type': 'GUI'}]
    )

    human = mpi_sim.objects.Human(position=[1., 1.], orientation=0)
    simulation.add_object(human)

    ari = mpi_sim.objects.ARIRobot(
        position=[5., 5.],
        orientation=np.pi / 2,
    )
    simulation.add_object(ari)
    ari.navigation.set_go_towards_human(human)

    is_human_reached = False
    for step in range(5000):
        simulation.step()

        if mpi_sim.utils.measure_center_distance(ari, human) < 1.5:
            is_human_reached = True
            break

    simulation.close()

    assert is_human_reached


def test_ari_navigation_go_to_group():

    simulation = mpi_sim.Simulation(
        visible_area=[[-10., 10.], [-10., 10.]],
        objects=[
            {'type': 'Wall', 'position': [6., 3.], 'orientation': 0., 'length': 6.},  # east
            {'type': 'Wall', 'position': [0., 3.], 'orientation': 0., 'length': 6.},  # west
            {'type': 'Wall', 'position': [3., 6.0], 'orientation': np.pi / 2, 'length': 6.2},  # south
            {'type': 'Wall', 'position': [3., 0.0], 'orientation': np.pi / 2, 'length': 6.2}  # north
        ],
        # processes=[{'type': 'GUI'}]
    )

    # add two humans (paul and irene)
    paul = mpi_sim.objects.Human(position=[1., 1.], orientation=0.)
    simulation.add_object(paul)
    irene = mpi_sim.objects.Human(position=[4., 1.], orientation=0.)
    simulation.add_object(irene)

    # add ARI
    ari = mpi_sim.objects.ARIRobot(position=[1., 5.], orientation=-np.pi / 2)
    simulation.add_object(ari)

    # create a group
    group = mpi_sim.scripts.GroupNavigation()
    simulation.add_script(group)
    group.add_agent([paul, irene])

    # define a goal for the group and let ari join the group by giving on person as target
    group.set_group_center([2.0, 3.0])
    ari.navigation.set_go_towards_human(paul)

    # run the simulation, until the script stops the simulation
    simulation.run(50)

    is_goal_reached = mpi_sim.utils.measure_center_distance(ari, [2.0, 3.0]) < 1.5

    simulation.close()

    assert is_goal_reached


def test_ari_navigation_set_goal_position():

    simulation = mpi_sim.Simulation(
        visible_area=((0., 10.), (0., 10.)),
        objects=[
            {'type': 'Wall', 'position': [6., 3.], 'orientation': 0., 'length': 6.},  # east
            {'type': 'Wall', 'position': [0., 3.], 'orientation': 0., 'length': 6.},  # west
            {'type': 'Wall', 'position': [3., 6.0], 'orientation': np.pi / 2, 'height': 6.2},  # south
            {'type': 'Wall', 'position': [3., 0.0], 'orientation': np.pi / 2, 'height': 6.2},  # north
        ],
        # processes=[{'type': 'GUI'}]
    )

    ari = mpi_sim.objects.ARIRobot(
        position=[4., 4.],
        orientation=- 3 / 4 * np.pi,
    )
    simulation.add_object(ari)
    ari.navigation.set_go_towards_position([2., 2.], 0.)

    is_goal_reached = False
    for step in range(2000):
        simulation.step()

        if mpi_sim.utils.measure_center_distance(ari, [2., 2.]) < 0.2 and np.abs(mpi_sim.utils.angle_difference(ari.orientation, ari.navigation.state.goal_orientation)) < 0.2:
            is_goal_reached = True
            break

    simulation.close()

    assert is_goal_reached


def test_ari_navigation_go_to_human_with_obstacles():

    simulation = mpi_sim.Simulation(
        visible_area=((0., 10.), (0., 10.)),
        objects=[
            {'type': 'Wall', 'position': [6., 3.], 'orientation': 0., 'height': 6.},  # east
            {'type': 'Wall', 'position': [0., 3.], 'orientation': 0., 'height': 6.},  # west
            {'type': 'Wall', 'position': [3., 6.0], 'orientation': np.pi / 2, 'height': 6.2},  # south
            {'type': 'Wall', 'position': [3., 0.0], 'orientation': np.pi / 2, 'height': 6.2},  # north,
            {'type': 'Bench', 'position': [4.7, 2.7], 'orientation': -1.2},
            {'type': 'Bench', 'position': [2., 3.7]}
        ],
        # processes=[{'type': 'GUI'}]
    )

    human = mpi_sim.objects.Human(position=[1., 1.], orientation=0.)
    simulation.add_object(human)

    ari = mpi_sim.objects.ARIRobot(
        position=[5., 5.],
        orientation=np.pi,
    )
    simulation.add_object(ari)
    ari.navigation.set_go_towards_human(human)

    is_human_reached = False
    for step in range(10000):
        simulation.step()

        if mpi_sim.utils.measure_center_distance(ari, human) < 1.5:
            is_human_reached = True
            break

    simulation.close()

    assert is_human_reached


def test_ari_navigation_go_to_human_with_obstacles_humans():

    simulation = mpi_sim.Simulation(
        visible_area=((0., 10.), (0., 10.)),
        objects=[
            {'type': 'Wall', 'position': [6., 3.], 'orientation': 0., 'height': 6.},  # east
            {'type': 'Wall', 'position': [0., 3.], 'orientation': 0., 'height': 6.},  # west
            {'type': 'Wall', 'position': [3., 6.0], 'orientation': np.pi / 2, 'height': 6.2},  # south
            {'type': 'Wall', 'position': [3., 0.0], 'orientation': np.pi / 2, 'height': 6.2},  # north,
            {'type': 'Bench', 'position': [4.7, 3.2], 'orientation': -1.2},
            {'type': 'Bench', 'position': [1.5, 4.2]}
        ],
        # processes=[{'type': 'GUI'}]
    )

    human1 = mpi_sim.objects.Human(position=[1., 1.], orientation=0.)
    human2 = mpi_sim.objects.Human(position=[2.5, 2.], orientation=-np.pi*3/4)
    human3 = mpi_sim.objects.Human(position=[3., 1.2], orientation=np.pi*1/4)
    simulation.add_object([human1, human2, human3])

    ari = mpi_sim.objects.ARIRobot(
        position=[5., 5.],
        orientation=np.pi,
    )
    simulation.add_object(ari)
    ari.navigation.set_go_towards_human(human1)

    is_human_reached = False
    for step in range(1000):
        simulation.step()

        if mpi_sim.utils.measure_center_distance(ari, human1) < 1.7:
            is_human_reached = True
            break

    simulation.close()

    assert is_human_reached


def test_ari_human_detector():

    # create the simulation
    sim = mpi_sim.Simulation(
        visible_area=((-10, 10), (-10, 10)),
        # processes=[{'type': 'GUI'}]  # add for debugging
    )

    # add chairs and other objects
    human_north = mpi_sim.objects.Human(position=(0, 5))
    human_north_id = sim.add_object(human_north)

    human_north_behind_other_human = mpi_sim.objects.Human(position=(0, 10))
    sim.add_object(human_north_behind_other_human)

    human_south = mpi_sim.objects.Human(position=(0, -5))
    human_south_id = sim.add_object(human_south)

    human_east_one = mpi_sim.objects.Human(position=(4, 0.1))
    human_east_one_id = sim.add_object(human_east_one)

    human_east_two = mpi_sim.objects.Human(position=(6, -1))
    human_east_two_id = sim.add_object(human_east_two)

    human_west_one = mpi_sim.objects.Human(position=(-3, 0.1))
    human_west_one_id = sim.add_object(human_west_one)

    human_west_two = mpi_sim.objects.Human(position=(-5, -1))
    human_west_two_id = sim.add_object(human_west_two)

    # add a couch with a chair in front and behind it
    bench_northeast = mpi_sim.objects.Bench(position=(5, 5))
    sim.add_object(bench_northeast)

    human_northeast_behind_bench = mpi_sim.objects.Human(position=(7, 7))
    human_northeast_behind_bench_id = sim.add_object(human_northeast_behind_bench)

    human_northeast_before_bench = mpi_sim.objects.Human(position=(5, 4))
    human_northeast_before_bench_id = sim.add_object(human_northeast_before_bench)

    # add a wall and a human behind that ari should not see
    wall_northwest = mpi_sim.objects.Wall(position=(-2, 2), orientation=np.pi/2, width=0.2, length=2)
    sim.add_object(wall_northwest)

    human_northeast_behind_wall = mpi_sim.objects.Human(position=(-2, 2.5))
    sim.add_object(human_northeast_behind_wall)

    # add the agent
    ari = mpi_sim.objects.ARIRobot(
        position = (0, 0),
        orientation = 0,
    )
    sim.add_object(ari)

    # Nothing detected before a step
    assert len(ari.human_detector.detected_objects) == 0
    assert len(ari.human_detector.detected_object_ids) == 0

    # do a step and check what is detected
    sim.step()

    assert len(ari.human_detector.detected_objects) == 5
    assert human_north in ari.human_detector.detected_objects
    assert human_east_one in ari.human_detector.detected_objects
    assert human_west_one in ari.human_detector.detected_objects
    assert human_northeast_before_bench in ari.human_detector.detected_objects
    assert human_northeast_behind_bench in ari.human_detector.detected_objects

    assert len(ari.human_detector.detected_object_ids) == 5
    assert human_north_id in ari.human_detector.detected_object_ids
    assert human_east_one_id in ari.human_detector.detected_object_ids
    assert human_west_one_id in ari.human_detector.detected_object_ids
    assert human_northeast_before_bench_id in ari.human_detector.detected_object_ids
    assert human_northeast_behind_bench_id in ari.human_detector.detected_object_ids

    #############################################
    # Rotate the agent by a few degree to the east
    ari.box2d_body.angle = -25 * np.pi / 180  # rotate by 25 degree clockwise

    sim.step()

    assert len(ari.human_detector.detected_objects) == 5
    assert human_north in ari.human_detector.detected_objects
    assert human_east_one in ari.human_detector.detected_objects
    assert human_east_two in ari.human_detector.detected_objects
    assert human_northeast_before_bench in ari.human_detector.detected_objects
    assert human_northeast_behind_bench in ari.human_detector.detected_objects

    assert len(ari.human_detector.detected_object_ids) == 5
    assert human_north_id in ari.human_detector.detected_object_ids
    assert human_east_one_id in ari.human_detector.detected_object_ids
    assert human_east_two_id in ari.human_detector.detected_object_ids
    assert human_northeast_before_bench_id in ari.human_detector.detected_object_ids
    assert human_northeast_behind_bench_id in ari.human_detector.detected_object_ids

    #############################################
    # Rotate the agent by a few degree to the west
    ari.box2d_body.angle = 25 * np.pi / 180  # rotate by 25 degree anti clockwise

    sim.step()

    assert len(ari.human_detector.detected_objects) == 5
    assert human_north in ari.human_detector.detected_objects
    assert human_west_one in ari.human_detector.detected_objects
    assert human_west_two in ari.human_detector.detected_objects
    assert human_northeast_before_bench in ari.human_detector.detected_objects
    assert human_northeast_behind_bench in ari.human_detector.detected_objects

    assert len(ari.human_detector.detected_object_ids) == 5
    assert human_north_id in ari.human_detector.detected_object_ids
    assert human_west_one_id in ari.human_detector.detected_object_ids
    assert human_west_two_id in ari.human_detector.detected_object_ids
    assert human_northeast_before_bench_id in ari.human_detector.detected_object_ids
    assert human_northeast_behind_bench_id in ari.human_detector.detected_object_ids

    #############################################
    # Rotate the agent to turn south
    ari.box2d_body.angle = np.pi  # rotate by 10 degree clockwise

    sim.step()

    assert len(ari.human_detector.detected_objects) == 3
    assert human_south in ari.human_detector.detected_objects
    assert human_east_two in ari.human_detector.detected_objects
    assert human_west_two in ari.human_detector.detected_objects

    assert len(ari.human_detector.detected_object_ids) == 3
    assert human_south_id in ari.human_detector.detected_object_ids
    assert human_east_two_id in ari.human_detector.detected_object_ids
    assert human_west_two_id in ari.human_detector.detected_object_ids

    sim.close()
