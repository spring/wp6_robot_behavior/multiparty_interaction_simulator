import mpi_sim as sim
import numpy as np
from numpy.testing import assert_array_equal, assert_array_almost_equal, assert_raises


def test_set_position_and_orientation_directly():

    init_position = [1.0, 1.0]
    init_orientation = 0.0

    set_position = [2.0, 2.0]
    set_orientation = 1.0

    # create simulation
    simulation = sim.Simulation()

    # add human
    human1_obj = sim.objects.Human(
        position=init_position,
        orientation=init_orientation
    )
    simulation.add_object(human1_obj, id=1)

    # check position and orientation
    assert_array_equal(human1_obj.position, init_position)
    assert human1_obj.orientation == init_orientation

    # do a single step
    simulation.step()

    # check position and orientation
    assert_array_equal(human1_obj.position, init_position)
    assert human1_obj.orientation == init_orientation

    # set new position
    human1_obj.position = set_position

    # check position and orientation, should the same
    assert_array_equal(human1_obj.position, set_position)
    assert human1_obj.orientation == init_orientation

    # do a single step
    simulation.step()

    # check position and orientation, must be changed
    assert_array_equal(human1_obj.position, set_position)
    assert human1_obj.orientation == init_orientation

    # set new orientation
    human1_obj.orientation = set_orientation

    # check position and orientation
    assert_array_equal(human1_obj.position, set_position)
    assert human1_obj.orientation == set_orientation

    # do a single step
    simulation.step()

    # check position and orientation, must be changed
    assert_array_equal(human1_obj.position, set_position)
    assert human1_obj.orientation == set_orientation


def test_set_position_and_orientation_velocity():

    init_position = [1.0, 1.0]
    init_orientation = 0.0

    position_velocity = [1.0, 0.0]
    orientation_velocity = 0.1

    init_position_velocity = [0.0, 0.0]
    init_orientation_velocity = 0.0

    # create simulation
    simulation = sim.Simulation()

    # add human
    human1_obj = sim.objects.Human(
        position=init_position,
        orientation=init_orientation
    )
    simulation.add_object(human1_obj, id=1)

    # check position and orientation
    assert_array_equal(human1_obj.position, init_position)
    assert human1_obj.orientation == init_orientation

    # check position and orientation velocity
    assert_array_almost_equal(human1_obj.position_velocity, init_position_velocity)
    assert human1_obj.orientation_velocity == init_orientation_velocity

    # do a single step
    simulation.step()

    # check position and orientation
    assert_array_equal(human1_obj.position, init_position)
    assert human1_obj.orientation == init_orientation

    # check position and orientation velocity
    assert_array_almost_equal(human1_obj.position_velocity, init_position_velocity)
    assert human1_obj.orientation_velocity == init_orientation_velocity

    # set new position
    human1_obj.position_velocity = position_velocity

    # check position and orientation, should the same
    assert_array_equal(human1_obj.position, init_position)
    assert human1_obj.orientation == init_orientation

    # check position and orientation velocity
    assert_array_almost_equal(human1_obj.position_velocity, position_velocity)
    assert human1_obj.orientation_velocity == init_orientation_velocity

    # do a single step
    simulation.step()

    # check position and orientation velocity, should be changed
    assert_array_almost_equal(human1_obj.position_velocity, position_velocity, decimal=1)
    assert human1_obj.orientation_velocity == 0.0

    # check if they are different
    assert_raises(AssertionError, assert_array_equal, human1_obj.position, init_position)
    assert human1_obj.orientation == init_orientation

    # check position and orientation velocity, should be changed
    assert_array_almost_equal(human1_obj.position_velocity, position_velocity, decimal=1)
    assert human1_obj.orientation_velocity == 0.0

    # set new orientation velocity
    human1_obj.orientation_velocity = orientation_velocity

    # check position and orientation velocity, should be changed
    assert_array_almost_equal(human1_obj.position_velocity, position_velocity, decimal=1)
    assert_array_almost_equal(human1_obj.orientation_velocity, orientation_velocity, decimal=1)

    # do a step
    simulation.step()

    # check position and orientation, must be changed
    assert_raises(AssertionError, assert_array_equal, human1_obj.position, init_position)
    assert_raises(AssertionError, assert_array_equal, human1_obj.orientation, init_orientation)

    # stop velocity
    human1_obj.orientation_velocity = None
    human1_obj.position_velocity = None

    # do a step
    simulation.step()

    # check position and orientation velocity, should be changed
    assert_array_almost_equal(human1_obj.position_velocity, [0.0, 0.0])
    assert_array_almost_equal(human1_obj.orientation_velocity, 0.0)

    old_pos = human1_obj.position
    old_ori = human1_obj.orientation

    simulation.step()

    # check position and orientation velocity, should be changed
    assert_array_almost_equal(human1_obj.position, old_pos)
    assert_array_almost_equal(human1_obj.orientation, old_ori)


def test_set_goal_position_and_orientation():

    init_position = [1.0, 1.0]
    init_orientation = 0.0

    goal_position = [2.0, 2.0]
    goal_orientation = 0.5

    # create simulation
    simulation = sim.Simulation()

    # add human
    human1_obj = sim.objects.Human(
        position=init_position,
        orientation=init_orientation
    )
    simulation.add_object(human1_obj, id=1)

    # check position and orientation
    assert_array_equal(human1_obj.position, init_position)
    assert human1_obj.orientation == init_orientation

    # do a single step
    simulation.step()

    # check position and orientation
    assert_array_equal(human1_obj.position, init_position)
    assert human1_obj.orientation == init_orientation

    # set the goal position
    human1_obj.navigation.goal_position = goal_position
    human1_obj.navigation.goal_orientation = goal_orientation

    # check position and orientation
    assert_array_equal(human1_obj.navigation.goal_position, goal_position)
    assert human1_obj.navigation.goal_orientation == goal_orientation

    for _ in range(100):
        simulation.step()

    # human should have moved
    # check position and orientation
    assert_array_almost_equal(human1_obj.position, goal_position, decimal=1)
    assert_array_almost_equal(human1_obj.orientation, goal_orientation, decimal=1)


def test_reset_and_removal_of_humans():
    """Tests if the simulation resets correctly if objects are added and removed before a reset."""

    permanent_human1_init_position = [1.0, 1.0]
    permanent_human1_init_orientation = 0.0

    permanent_human2_init_position = [2.0, 2.0]
    permanent_human2_init_orientation = 0.0

    non_permanent_human1_init_position = [3.0, 3.0]
    non_permanent_human1_init_orientation = 0.0

    non_permanent_human2_init_position = [4.0, 4.0]
    non_permanent_human2_init_orientation = 0.0

    # create simulation
    simulation = sim.Simulation()

    # add permanent humans
    permanent_human1_obj = sim.objects.Human(
        position=permanent_human1_init_position,
        orientation=permanent_human1_init_orientation
    )
    simulation.add_object(permanent_human1_obj, id=1)

    permanent_human2_obj = sim.objects.Human(
        position=permanent_human2_init_position,
        orientation=permanent_human2_init_orientation
    )
    simulation.add_object(permanent_human2_obj, id=2)

    # check if initial position is correctly set
    np.testing.assert_array_equal(permanent_human1_init_position, permanent_human1_obj.position)
    # np.testing.assert_array_equal(permanent_human1_init_orientation, permanent_human1_obj.orientation)
    np.testing.assert_array_equal(permanent_human2_init_position, permanent_human2_obj.position)
    # np.testing.assert_array_equal(permanent_human2_init_orientation, permanent_human2_obj.orientation)

    # set a goal position for the humans, so that they move
    permanent_human1_obj.goal_position = [5.0, 5.0]
    permanent_human1_obj.goal_orientation = 1.0
    permanent_human2_obj.goal_position = [5.0, 5.0]
    permanent_human2_obj.goal_orientation = 1.0

    # do 2 steps to allow the human to move
    simulation.run(n_steps=2)

    # check if position of human has changed
    # assert not np.array_equal(permanent_human1_init_position, permanent_human1_obj.position)
    # assert not np.array_equal(permanent_human1_init_orientation, permanent_human1_obj.orientation)
    # assert not np.array_equal(permanent_human2_init_position, permanent_human2_obj.position)
    # assert not np.array_equal(permanent_human2_init_orientation, permanent_human2_obj.orientation)

    # remove permanent_human2
    simulation.remove_object(permanent_human2_obj)

    # check if permanent_human2 was removed
    assert 2 not in simulation.entities
    assert permanent_human2_obj.position is None
    # assert permanent_human2_obj.orientation is None

    # add a new non-permanant human, and give it the same id as for the permanent_human_2
    non_permanent_human1_obj = sim.objects.Human(
        position=non_permanent_human1_init_position,
        orientation=non_permanent_human1_init_orientation
    )
    simulation.add_object(non_permanent_human1_obj, id=2)

    non_permanent_human2_obj = sim.objects.Human(
        position=non_permanent_human2_init_position,
        orientation=non_permanent_human2_init_orientation
    )
    simulation.add_object(non_permanent_human2_obj, id=3)

    # check if initial positions are correctly set
    np.testing.assert_array_equal(non_permanent_human1_init_position, non_permanent_human1_obj.position)
    # np.testing.assert_array_equal(non_permanent_human1_init_orientation, non_permanent_human1_obj.orientation)
    np.testing.assert_array_equal(non_permanent_human2_init_position, non_permanent_human2_obj.position)
    # np.testing.assert_array_equal(non_permanent_human2_init_orientation, non_permanent_human2_obj.orientation)

    # set goals for the non-permanent humans
    non_permanent_human1_obj.goal_position = [5.0, 5.0]
    non_permanent_human1_obj.goal_orientation = 1.0
    non_permanent_human2_obj.goal_position = [5.0, 5.0]
    non_permanent_human2_obj.goal_orientation = 1.0

    # do 2 steps to allow the human to move further
    simulation.run(n_steps=2)

    # check if non_permanent_humans have moved
    # assert not np.array_equal(non_permanent_human1_init_position, non_permanent_human1_obj.position)
    # assert not np.array_equal(non_permanent_human1_init_orientation, non_permanent_human1_obj.orientation)
    # assert not np.array_equal(non_permanent_human2_init_position, non_permanent_human2_obj.position)
    # assert not np.array_equal(non_permanent_human2_init_orientation, non_permanent_human2_obj.orientation)

    # remove non_permanent_human2_obj
    simulation.remove_object(non_permanent_human2_obj)

    # check if non_permanent_human2_obj was removed
    assert 3 not in simulation.entities
    assert non_permanent_human2_obj.position is None
    # assert non_permanent_human2_obj.orientation is None

    # reset the simulation
    simulation.reset()

    # check if correct id's exists
    assert 1 in simulation.entities
    assert 2 in simulation.entities
    assert 3 not in simulation.entities

    # check if permanent humans are reset correctly set
    # np.testing.assert_array_equal(permanent_human1_init_position, permanent_human1_obj.position)
    # np.testing.assert_array_equal(permanent_human1_init_orientation, permanent_human1_obj.orientation)
    # np.testing.assert_array_equal(permanent_human2_init_position, permanent_human2_obj.position)
    # np.testing.assert_array_equal(permanent_human2_init_orientation, permanent_human2_obj.orientation)

    # check if non-permanent humans are still deleted
    assert non_permanent_human1_obj.position is None
    # assert non_permanent_human1_obj.orientation is None
    assert non_permanent_human2_obj.position is None
    # assert non_permanent_human2_obj.orientation is None

    # run simulation again
    simulation.run(n_steps=2)

    # check if position of permanent humans has changed
    # assert not np.array_equal(permanent_human1_init_position, permanent_human1_obj.position)
    # assert not np.array_equal(permanent_human1_init_orientation, permanent_human1_obj.orientation)
    # assert not np.array_equal(permanent_human2_init_position, permanent_human2_obj.position)
    # assert not np.array_equal(permanent_human2_init_orientation, permanent_human2_obj.orientation)

    simulation.close()