import mpi_sim
from Box2D import b2CircleShape
import numpy as np


class DummyAgentObject(mpi_sim.Object):

    @staticmethod
    def default_config():
        dc = mpi_sim.Object.default_config()
        dc.position = (0, 0)
        dc.orientation = 1
        dc.properties.append('dynamic')
        return dc


    @property
    def position(self):
        return self.box2d_body.position


    @property
    def orientation(self):
        return self.box2d_body.angle


    def _create_in_box2d_world(self, box2d_world):
        self.box2d_body = box2d_world.CreateStaticBody(
            position=self.config.position,
            angle=self.config.orientation,
            shapes=b2CircleShape(radius=.5),
            userData={'name': self.id}
        )

        # create another fixture in front of the main body, to see if it gets ignored
        self.box2d_body.CreatePolygonFixture(
            box=(0.2, 0.2, (0, 1), 0),
        )

        return self.box2d_body


def test_different_rotations():

    # create the simulation
    sim = mpi_sim.Simulation(
        visible_area=((-10, 10), (-10, 10)),
        # processes=[{'type': 'GUI'}]  # add for debugging
    )

    # add chairs and other objects
    chair_north = mpi_sim.objects.Chair(position=(0, 5))
    chair_north_id = sim.add_object(chair_north)

    chair_north_behind_other_chair = mpi_sim.objects.Chair(position=(0, 10))
    sim.add_object(chair_north_behind_other_chair)

    chair_south = mpi_sim.objects.Chair(position=(0, -5))
    chair_south_id = sim.add_object(chair_south)

    chair_east_one = mpi_sim.objects.Chair(position=(4, 0.1))
    chair_east_one_id = sim.add_object(chair_east_one)

    chair_east_two= mpi_sim.objects.Chair(position=(6, -1))
    chair_east_two_id = sim.add_object(chair_east_two)

    chair_west_one = mpi_sim.objects.Chair(position=(-3, 0.1))
    chair_west_one_id = sim.add_object(chair_west_one)

    chair_west_two = mpi_sim.objects.Chair(position=(-5, -1))
    chair_west_two_id = sim.add_object(chair_west_two)

    # add a couch with a chair in front and behind it
    bench_northeast = mpi_sim.objects.Bench(position=(5, 5))
    sim.add_object(bench_northeast)

    chair_northeast_behind_bench = mpi_sim.objects.Bench(position=(7, 7))
    sim.add_object(chair_northeast_behind_bench)

    chair_northeast_before_bench = mpi_sim.objects.Chair(position=(5, 4))
    chair_northeast_before_bench_id = sim.add_object(chair_northeast_before_bench)

    # a bench that should be ignored
    bench_south_ignore = mpi_sim.objects.Bench(position=(0, -2))
    sim.add_object(bench_south_ignore)

    # add the agent
    agent = DummyAgentObject(
        position=(0, 0),
        orientation=0,
        components=[dict(
            type=mpi_sim.components.ObjectDetector,
            name='detector',
            object_filter=dict(types=mpi_sim.objects.Chair),  # detect chairs
            ignore_objects_filter=dict(objects=bench_south_ignore),  # ignore the south bench
            fov=np.pi  # fielf of view from -pi/2 to pi/2
        )]
    )
    sim.add_object(agent)


    # Nothing detected before a step
    assert len(agent.detector.detected_objects) == 0
    assert len(agent.detector.detected_object_ids) == 0

    # do a step and check what is detected
    sim.step()

    assert len(agent.detector.detected_objects) == 4
    assert chair_north in agent.detector.detected_objects
    assert chair_east_one in agent.detector.detected_objects
    assert chair_west_one in agent.detector.detected_objects
    assert chair_northeast_before_bench in agent.detector.detected_objects

    assert len(agent.detector.detected_object_ids) == 4
    assert chair_north_id in agent.detector.detected_object_ids
    assert chair_east_one_id in agent.detector.detected_object_ids
    assert chair_west_one_id in agent.detector.detected_object_ids
    assert chair_northeast_before_bench_id in agent.detector.detected_object_ids

    #############################################
    # Rotate the agent by a few degree to the east
    agent.box2d_body.angle = -25 * np.pi/180  # rotate by 25 degree clockwise

    sim.step()

    assert len(agent.detector.detected_objects) == 4
    assert chair_north in agent.detector.detected_objects
    assert chair_east_one in agent.detector.detected_objects
    assert chair_east_two in agent.detector.detected_objects
    assert chair_northeast_before_bench in agent.detector.detected_objects

    assert len(agent.detector.detected_object_ids) == 4
    assert chair_north_id in agent.detector.detected_object_ids
    assert chair_east_one_id in agent.detector.detected_object_ids
    assert chair_east_two_id in agent.detector.detected_object_ids
    assert chair_northeast_before_bench_id in agent.detector.detected_object_ids

    #############################################
    # Rotate the agent by a few degree to the west
    agent.box2d_body.angle = 25 * np.pi/180  # rotate by 25 degree anti clockwise

    sim.step()

    assert len(agent.detector.detected_objects) == 4
    assert chair_north in agent.detector.detected_objects
    assert chair_west_one in agent.detector.detected_objects
    assert chair_west_two in agent.detector.detected_objects
    assert chair_northeast_before_bench in agent.detector.detected_objects

    assert len(agent.detector.detected_object_ids) == 4
    assert chair_north_id in agent.detector.detected_object_ids
    assert chair_west_one_id in agent.detector.detected_object_ids
    assert chair_west_two_id in agent.detector.detected_object_ids
    assert chair_northeast_before_bench_id in agent.detector.detected_object_ids

    #############################################
    # Rotate the agent to turn south
    agent.box2d_body.angle = np.pi  # rotate by 10 degree clockwise

    sim.step()

    assert len(agent.detector.detected_objects) == 3
    assert chair_south in agent.detector.detected_objects
    assert chair_east_two in agent.detector.detected_objects
    assert chair_west_two in agent.detector.detected_objects

    assert len(agent.detector.detected_object_ids) == 3
    assert chair_south_id in agent.detector.detected_object_ids
    assert chair_east_two_id in agent.detector.detected_object_ids
    assert chair_west_two_id in agent.detector.detected_object_ids

    sim.close()


def test_max_distance():

    # create the simulation
    sim = mpi_sim.Simulation(
        visible_area=((-10, 10), (-10, 10)),
        # processes=[dict(type='GUI', position=(0, 5))]  # add for debug
    )

    # add the agent
    agent = DummyAgentObject(
        position=(0, 0),
        orientation=0,
        components=[dict(
            type=mpi_sim.components.ObjectDetector,
            name='detector',
            object_filter=dict(types=mpi_sim.objects.Chair),
            fov=np.pi,
            max_distance=5
        )]
    )
    sim.add_object(agent)

    # add chairs
    chair_inside_distance = mpi_sim.objects.Chair(position=(-2, 4))
    chair_inside_distance_id = sim.add_object(chair_inside_distance)

    chair_outside_distance = mpi_sim.objects.Chair(position=(-2, 6))
    sim.add_object(chair_outside_distance)

    # Nothing detected before a step
    assert len(agent.detector.detected_objects) == 0
    assert len(agent.detector.detected_object_ids) == 0

    # do a step and check what is detected
    sim.step()

    assert len(agent.detector.detected_objects) == 1
    assert chair_inside_distance in agent.detector.detected_objects

    assert len(agent.detector.detected_object_ids) == 1
    assert chair_inside_distance_id in agent.detector.detected_object_ids

    sim.close()