import mpi_sim as sim
import numpy as np
from numpy.testing import assert_array_almost_equal

def test_speech_generator():

    # TODO: test the generator during a full simulation as part of a human agent

    speech_generator = sim.components.SpeechGenerator(name='speech')
    human = sim.objects.Human(components=[speech_generator])
    simulation = sim.Simulation()
    simulation.add_object(human)

    ##########################################################################
    # 1. Sentence

    content = 'Hello. I am ARI.'
    act = 'WELCOME'
    speech_generator.say(content, act)

    assert speech_generator.current_speech is None
    assert speech_generator.current_speech_content is None
    assert speech_generator.current_speech_act is None

    spoken_content = ''
    n_steps_1_start = 0
    n_steps_1_end = 0
    for step in range(100):
        simulation.step()

        assert speech_generator.current_speech_content == content
        assert speech_generator.current_speech_act == act

        spoken_content += speech_generator.current_speech

        if step == 1:
            assert speech_generator.previous_speech_act is None
            assert speech_generator.previous_speech_content is None

        if spoken_content == content:
            break

        n_steps_1_end += 1

    assert speech_generator.previous_speech_act == 'WELCOME'
    assert speech_generator.previous_speech_content == 'Hello. I am ARI.'

    # speech has to be nothing after the sentence
    simulation.step()
    assert speech_generator.current_speech is None
    assert speech_generator.current_speech_content is None
    assert speech_generator.current_speech_act is None

    # check history
    assert speech_generator.history[-1].content == content
    assert speech_generator.history[-1].act == act
    assert speech_generator.history[-1].start_step == n_steps_1_start
    assert speech_generator.history[-1].end_step == n_steps_1_end
    assert_array_almost_equal(speech_generator.history[-1].start_time, n_steps_1_start * simulation.step_length)
    assert_array_almost_equal(speech_generator.history[-1].end_time, n_steps_1_end * simulation.step_length)

    #

    ##########################################################################
    # 2. Sentence

    content = 'How are long_word_with_potential_break'
    speech_generator.say(content)

    assert speech_generator.current_speech is None
    assert speech_generator.current_speech_content is None
    assert speech_generator.current_speech_act is None

    spoken_content = ''
    n_steps_2_start = simulation.step_counter + 1
    n_steps_2_end = simulation.step_counter + 1
    for step in range(100):
        simulation.step()

        assert speech_generator.current_speech_content == content
        assert speech_generator.current_speech_act == 'UNKNOWN'

        spoken_content += speech_generator.current_speech

        if step == 1:
            assert speech_generator.previous_speech_act == 'WELCOME'
            assert speech_generator.previous_speech_content == 'Hello. I am ARI.'

        if spoken_content == content:
            break

        n_steps_2_end += 1

    assert speech_generator.previous_speech_act == 'UNKNOWN'
    assert speech_generator.previous_speech_content == 'How are long_word_with_potential_break'

    # speech has to be nothing after the sentence
    simulation.step()
    assert speech_generator.current_speech is None
    assert speech_generator.current_speech_content is None
    assert speech_generator.current_speech_act is None

    # check history
    assert speech_generator.history[-1][0] == 'How are long_word_with_potential_break'
    assert speech_generator.history[-1][1] == 'UNKNOWN'

    assert speech_generator.history[-1].content == 'How are long_word_with_potential_break'
    assert speech_generator.history[-1].act == 'UNKNOWN'
    assert speech_generator.history[-1].start_step == n_steps_2_start
    assert speech_generator.history[-1].end_step == n_steps_2_end
    assert_array_almost_equal(speech_generator.history[-1].start_time, n_steps_2_start * simulation.step_length)
    assert_array_almost_equal(speech_generator.history[-1].end_time, n_steps_2_end * simulation.step_length)

    assert speech_generator.history[-2].content == 'Hello. I am ARI.'
    assert speech_generator.history[-2].act == 'WELCOME'
    assert speech_generator.history[-2].start_step == n_steps_1_start
    assert speech_generator.history[-2].end_step == n_steps_1_end
    assert_array_almost_equal(speech_generator.history[-2].start_time, n_steps_1_start * simulation.step_length)
    assert_array_almost_equal(speech_generator.history[-2].end_time, n_steps_1_end * simulation.step_length)