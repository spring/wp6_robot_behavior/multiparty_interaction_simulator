import mpi_sim
import numpy as np
from numpy.testing import assert_array_almost_equal


def test_default_gaze_generator():

    sim = mpi_sim.Simulation()

    human1 = mpi_sim.objects.Human(position=[0.0, 0.0], orientation=0.0)
    human2 = mpi_sim.objects.Human(position=[0.0, 1.0], orientation=0.0)
    human3 = mpi_sim.objects.Human(position=[1.0, 0.0], orientation=0.0)
    human4 = mpi_sim.objects.Human(position=[1.0, 1.0], orientation=0.0)
    sim.add_object([human1, human2, human3, human4])

    gaze_gen = mpi_sim.components.GazeGenerator()
    human1.add_component(gaze_gen)

    assert human1.gaze.direction == 0.0
    assert human1.gaze.target is None

    for _ in range(100):
        sim.step()
        assert -np.pi <= human1.gaze.direction <= np.pi
        assert human1.gaze.direction != 0.0
        assert human1.gaze.target is None

    sim.close()


def test_gaze_generator_direction_sampling():

    sim = mpi_sim.Simulation()

    human1 = mpi_sim.objects.Human(position=[0.0, 0.0], orientation=0.0)
    sim.add_object([human1])

    ##########################
    # set by config

    config = dict(
        direction_sampling = dict(
            prefered_direction=0.1,
            direction_std=0.001
        )
    )

    gaze_gen = mpi_sim.components.GazeGenerator(config=config)
    human1.add_component(gaze_gen)

    assert human1.gaze.direction == 0.0
    assert human1.gaze.target is None

    for _ in range(100):
        sim.step()
        assert 0.08 <= human1.gaze.direction <= 0.12
        assert human1.gaze.target is None


    ##########################
    # set by setter function

    gaze_gen.set_direction_sampling(prefered_direction=-0.1, direction_std=0.001)

    for _ in range(100):
        sim.step()
        assert -0.12 <= human1.gaze.direction <= -0.08
        assert human1.gaze.target is None

    sim.close()


def test_gaze_generator_target_sampling():

    sim = mpi_sim.Simulation()

    human1 = mpi_sim.objects.Human(position=[0.0, 0.0], orientation=0.0)
    human2 = mpi_sim.objects.Human(position=[0.0, 1.0], orientation=0.0)
    human3 = mpi_sim.objects.Human(position=[1.0, 0.0], orientation=0.0)
    human4 = mpi_sim.objects.Human(position=[1.0, 1.0], orientation=0.0)
    sim.add_object([human1, human2, human3, human4])

    ##########################
    # set by config

    config = dict(
        target_sampling = dict(
            main_targets = [human2],
            side_targets = [human3, human4.id],
            main_target_pr = 0.8
        )
    )

    gaze_gen = mpi_sim.components.GazeGenerator(config=config)
    human1.add_component(gaze_gen)

    assert human1.gaze.direction == 0.0
    assert human1.gaze.target is None

    for _ in range(100):
        sim.step()

        assert human1.gaze.target == human2 or human1.gaze.target == human3 or human1.gaze.target == human4

        if human1.gaze.target == human2:
            assert human1.gaze.direction == 0.0

        if human1.gaze.target == human3:
            assert human1.gaze.direction == -np.pi/2

        if human1.gaze.target == human4:
            assert human1.gaze.direction == -np.pi/4

    ##########################
    # set by setter function

    human1.gaze.set_target_sampling(main_targets=human2, side_targets=human3, main_target_pr=1.0)
    for _ in range(100):
        sim.step()

        assert human1.gaze.target == human2
        assert human1.gaze.direction == 0.0