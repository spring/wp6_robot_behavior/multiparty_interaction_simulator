import mpi_sim
import numpy.testing as np_test
from Box2D import b2PolygonShape, b2CircleShape
import numpy as np


def manual_test():

    # create the simulation
    sim = mpi_sim.Simulation(
            visible_area = ((-6,6),(-6,6)),
            
            objects=[
                {'type': 'Wall', 'position': [-5.1, 0], 'orientation': np.pi, 'length': 10.2},
                {'type': 'Wall', 'position': [5.1, 0], 'orientation': 0., 'length': 10.2},
                {'type': 'Wall', 'position': [0., 5.1], 'orientation': np.pi / 2, 'length': 10.2},
                {'type': 'Wall', 'position': [0., -5.1], 'orientation': -np.pi / 2, 'length': 10.2},
                #{'type': 'RoundTable',  'id': 10, 'position': [1., 0], 'radius': 0.5}
            ],
            processes=[{'type': 'GUI'}]
        )


    agent = mpi_sim.objects.ARIRobot( # type: ignore
            components=[dict(
            type=mpi_sim.components.RayCastSensor,
            name = 'raycast',
            field_of_view = 360,
            delta_angle = 3,
            ray_max_range = 3,
            noise = False,
            noise_ratio = 0.5,
            draw_raycast = True
            )],
            position = (-2, 0),
            orientation = -np.pi/2
        )
    
    sim.add_object(agent)
    sim.set_reset_point()
    sim.reset()
    for _ in range(30):
        for _ in range(22):
            agent.forward_velocity = 0.1
            agent.orientation_velocity = 0.1
            sim.step()
        agent.raycast.raycast_distances
    

def test_3_rays_with_walls():

    # create the simulation
    sim = mpi_sim.Simulation(
            visible_area = ((-6,6),(-6,6)),
            
            objects=[
                {'type': 'Wall', 'position': [-5.1, 0], 'orientation': np.pi, 'length': 10.2},
                {'type': 'Wall', 'position': [5.1, 0], 'orientation': 0., 'length': 10.2},
                {'type': 'Wall', 'position': [0., 5.1], 'orientation': np.pi / 2, 'length': 10.2},
                {'type': 'Wall', 'position': [0., -5.1], 'orientation': -np.pi / 2, 'length': 10.2},
            ],
        )


    agent = mpi_sim.objects.ARIRobot( # type: ignore
            components=[dict(
            type=mpi_sim.components.RayCastSensor,
            name = 'raycast',
            field_of_view = 180,
            delta_angle = 90,

            draw_raycast = False
            )],
            position = (0,0),
            orientation = 0
        )
    
    sim.add_object(agent)
    sim.set_reset_point()
    sim.reset()
    sim.step()
    np.testing.assert_almost_equal(agent.raycast.raycast_distances, [5,5,5], decimal = 2)
    np.testing.assert_almost_equal(agent.raycast.raycast_angle_degrees, [90,0,-90], decimal = 2)

def test_3_rays_without_walls():

    # create the simulation
    sim = mpi_sim.Simulation(
            visible_area = ((-6,6),(-6,6)),
        )


    agent = mpi_sim.objects.ARIRobot( # type: ignore
            components=[dict(
            type=mpi_sim.components.RayCastSensor,
            name = 'raycast',
            field_of_view = 180,
            delta_angle = 90,
            ray_max_range = 20,
            draw_raycast = False
            )],
            position = (0,0),
            orientation = 0
        )
    
    sim.add_object(agent)
    sim.set_reset_point()
    sim.reset()
    sim.step()
    np.testing.assert_almost_equal(agent.raycast.raycast_distances, [20,20,20], decimal = 2)
    np.testing.assert_almost_equal(agent.raycast.raycast_angle_degrees, [90,0,-90], decimal = 2)

if __name__ == "__main__":
#     test_3_rays_with_walls()
#     test_3_rays_without_walls()
    manual_test()