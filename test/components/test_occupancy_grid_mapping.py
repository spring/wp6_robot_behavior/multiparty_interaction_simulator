import mpi_sim
import numpy.testing as np_test
from Box2D import b2PolygonShape, b2CircleShape
import numpy as np


class DummyAgentObject(mpi_sim.Object):

    @staticmethod
    def default_config():
        dc = mpi_sim.Object.default_config()
        dc.position = (0, 0)
        dc.orientation = 1
        dc.properties.append('dynamic')
        return dc


    @property
    def position(self):
        return self.box2d_body.position


    @property
    def orientation(self):
        return self.box2d_body.angle


    def _create_in_box2d_world(self, box2d_world):
        self.box2d_body = box2d_world.CreateStaticBody(
            position=self.config.position,
            angle=self.config.orientation,
            shapes=b2CircleShape(radius=.5),
        )
        return self.box2d_body


class PolygonObject(mpi_sim.Object):

    @staticmethod
    def default_config():
        dc = mpi_sim.Object.default_config()
        dc.position = (0, 0)
        dc.width = 1
        dc.height = 1
        dc.properties.append('static')
        return dc


    def _create_in_box2d_world(self, box2d_world):
        return box2d_world.CreateStaticBody(
            position=self.config.position,
            shapes=b2PolygonShape(box=(self.config.width, self.config.height)),
        )


def test_no_rotation():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 10), (0, 5)))

    agent = DummyAgentObject(
        position=(0, 0),
        orientation=0,
        components=[dict(
            type=mpi_sim.components.OccupancyGridMapping,
            name='mapping',
            object_filter=dict(properties='static'),
            depth=3.,
            resolution=1.,
        )]
    )

    sim.add_object(agent)
    sim.add_object(PolygonObject(position=(1,1), width=1.5, height=1.5))

    # check default values before doing the mapping
    expected_global = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
    ])
    expected_global = np.flip(expected_global, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_global, agent.mapping.global_map)
    assert agent.mapping.global_map_shape == agent.mapping.global_map.shape
    assert agent.mapping.global_map_area == ((0, 10), (0, 5))

    expected_local = np.array([
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local = np.flip(expected_local, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_local, agent.mapping.local_map)
    assert agent.mapping.local_map_shape == agent.mapping.local_map.shape
    assert agent.mapping.local_map_area == ((-3, 3), (-3, 3))

    #######################################

    # update both maps
    agent.mapping.update_global_map()
    agent.mapping.update_local_map()

    expected_global = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
    ])
    expected_global = np.flip(expected_global, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_global, agent.mapping.global_map)
    assert agent.mapping.global_map_shape == agent.mapping.global_map.shape
    assert agent.mapping.global_map_area == ((0, 10), (0, 5))

    expected_local = np.array([
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local = np.flip(expected_local, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_local, agent.mapping.local_map)
    assert agent.mapping.local_map_shape == agent.mapping.local_map.shape
    assert agent.mapping.local_map_area == ((-3, 3), (-3, 3))


def test_east_rotation():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 10), (0, 5)))

    agent = DummyAgentObject(
        position=(0, 0),
        orientation=-np.pi / 2,
        components=[dict(
            type=mpi_sim.components.OccupancyGridMapping,
            name='mapping',
            object_filter=dict(properties='static'),
            depth=3.,
            resolution=1.,
        )]
    )

    sim.add_object(agent)
    sim.add_object(PolygonObject(position=(1,1), width=1.5, height=1.5))

    # update both maps
    agent.mapping.update_global_map()
    agent.mapping.update_local_map()

    expected_global = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
    ])
    expected_global = np.flip(expected_global, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_global, agent.mapping.global_map)
    assert agent.mapping.global_map_shape == agent.mapping.global_map.shape
    assert agent.mapping.global_map_area == ((0, 10), (0, 5))

    expected_local = np.array([
        [0., 1., 1., 1., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local = np.flip(expected_local, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_local, agent.mapping.local_map)
    assert agent.mapping.local_map_shape == agent.mapping.local_map.shape
    assert agent.mapping.local_map_area == ((-3, 3), (-3, 3))


def test_west_rotation():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 10), (0, 5)))

    agent = DummyAgentObject(
        position=(0, 0),
        orientation=np.pi / 2,
        components=[dict(
            type=mpi_sim.components.OccupancyGridMapping,
            name='mapping',
            object_filter=dict(properties='static'),
            depth=3.,
            resolution=1.,
        )]
    )

    sim.add_object(agent)
    sim.add_object(PolygonObject(position=(1,1), width=1.5, height=1.5))

    # update both maps
    agent.mapping.update_global_map()
    agent.mapping.update_local_map()

    expected_global = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
    ])
    expected_global = np.flip(expected_global, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_global, agent.mapping.global_map)
    assert agent.mapping.global_map_shape == agent.mapping.global_map.shape
    assert agent.mapping.global_map_area == ((0, 10), (0, 5))

    expected_local = np.array([
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 1., 1., 1.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local = np.flip(expected_local, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_local, agent.mapping.local_map)
    assert agent.mapping.local_map_shape == agent.mapping.local_map.shape
    assert agent.mapping.local_map_area == ((-3, 3), (-3, 3))


def test_south_rotation():

    # create the simulation
    sim = mpi_sim.Simulation(visible_area=((0, 10), (0, 5)))

    agent = DummyAgentObject(
        position=(0, 0),
        orientation=np.pi,
        components=[dict(
            type=mpi_sim.components.OccupancyGridMapping,
            name='mapping',
            object_filter=dict(properties='static'),
            depth=3.,
            resolution=1.,
        )]
    )

    sim.add_object(agent)
    sim.add_object(PolygonObject(position=(1,1), width=1.5, height=1.5))

    # update both maps
    agent.mapping.update_global_map()
    agent.mapping.update_local_map()

    expected_global = np.array([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.],
    ])
    expected_global = np.flip(expected_global, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_global, agent.mapping.global_map)
    assert agent.mapping.global_map_shape == agent.mapping.global_map.shape
    assert agent.mapping.global_map_area == ((0, 10), (0, 5))

    expected_local = np.array([
        [0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 1., 1., 1., 0., 0.],
        [0., 0., 0., 0., 0., 0.],
    ])
    expected_local = np.flip(expected_local, axis=0).transpose()
    np_test.assert_array_almost_equal(expected_local, agent.mapping.local_map)
    assert agent.mapping.local_map_shape == agent.mapping.local_map.shape
    assert agent.mapping.local_map_area == ((-3, 3), (-3, 3))
