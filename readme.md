# Multi-Agent Interaction Simulator

Version 0.0.9 (23.06.2024)

Simulator for multi-agent interaction scenarios in a 2D environment.

Technical documentation of the features: [Deliverable D6.4: Software for Generating
Multi-Party Situated Interactions](https://spring-h2020.eu/wp-content/uploads/2023/07/SPRING_D6.4_Software_for_generating_multi_party_situated_interactions_VFinal_16.06.2022.pdf) (Chapter 3)

## Setup

### Requirements

Requires Python 3.8.

Required packages (will be installed automatically):
 * box2d >= 2.3.10
 * pygame >= 2.1.2
 * numpy >= 1.22.4
 * six >= 1.16.0
 * scipy >= 1.8.1
 * social_mpc (git@gitlab.inria.fr:spring/wp6_robot_behavior/Motor_Controller.git)
 * pygco (git@gitlab.inria.fr:robotlearn/pygco.git)

### Installation

    pip install .

If the ARINavigation component (social_mpc) should be used then it has to be extra installed.

    ./pull_submodules.sh
    pip install ./modules/Motor_Controller


#### Development Environment

It is recommended to use a conda environment or virtualenv for development.
The required packages can be found in *requirements.txt*.
To install the packages:

    pip install -r requirements.txt


## Description

The documentation is not up to date and misses many elements.
Please have a look at the example *examples\welcome_scenario_01.py* and *examples\welcome_scenario_02.py* that show how the simulator can be used.
Note: Currently there is a bug with the Navigation module of ARI.
As a result *examples\welcome_scenario_02.py* does not work properly.

The simulator is composed of a 2D world (simulated via PyBox2D) that has several *objects* (walls, furniture, agents) and can be controlled via *scripts*.
Objects have different properties and components.
The components are used to control the agents, generate their behavior (gaze, pose, emotions), and how they sense their environment.
Components can be added in a modular way.
The scripts control the simulation and give the agents commands which they execute.
Additionally, the simulation can have extra *processes* which are for example used for the visualization.

The following figure provides an overview of the simulator components:
![overview](doc/images/simulator.drawio.png "a title")

### General

Each part of the simulator is defined by a python class.
All classes use a similar interface for their configuration.
They must have a static *default_config()* method that returns the default configuration in form of a dictionary.
Furthermore, their constructors take as arguments config dictionaries that modify the default configuration or a list of arguments that define specific configuration properties.
A mechanism that provides then the config as a property (*self.config*) will be provided by all base class, for example the *SimObject* or *Script* base class.
See the example for more information. 

Example:

    import sim

    class ExampleObject(sim.Object):
    
        # defines the default configuration for this object
        @staticmethod 
        def default_config():
            config = super().default_config()
            config.init_position = [1.0, 3.0]
            return config
            
        def __init__(self, config=None, **kwargs):
            # provide the configuration to the base class, which creates the config property
            super().__init__(config, **kwargs)
            
            # the configuration can be accessed via the self.config property 
            self.box2d_objects[0].pos = self.config.init_position
            
The configuration of the object can be set by two ways.

1\) Providing a configuration dictionary:

    config = dict(init_position=[4., 3.])
    obj = ExampleObject(config)
    
2\) Giving the config as arguments to the constructor:

    obj = ExampleObject(init_position=[4., 3.])



### Simulation

The *Simulation* provides a 2D environment in which object (walls, furniture, humans, robots, ...) can interact with each other.
The simulation has a *reset()*, *step()*, and *run(n_steps, stop_condition)* method to execute the simulation.
The simulation holds lists of objects, scripts, and processes. 
Each object and script have a unique ID (integer) and a *reset()* and *step()* function.
Objects contain *components* that control their behavior.
Components also have a *reset()* and *step()* method. 
During each step of the simulation, at first, the physical simulation is executed (based on the Box2D objects that are affiliated with each object).
Afterwards, the step functions of the generator and sensor components of each object are called.
Followed by the step functions of the objects, and finally by the step functions of the scripts. 

Additionally to objects and scripts the simulation also holds processes and logging functionality.
Processes represent background processes that are available to all components or visualization tools.

Properties:
 * objects: Dictionary of objects.
 * scripts: Dictionary of scripts.
 * processes: List of background processes such as visualizations. 

Methods:
 * reset(): resets the simulation to the initial configured scene
 * step(): steps the simulation by one time step
 * run(n_steps, stop_condition): Runs the simulation for n_steps number of steps or until the stop_confition (function reference) is true.
 * add_object(obj, id): adds a new object to the simulation
 * remove_object(id): removes the object with the given id from the simulation
 * add_script(script, id)
 * remove_script(script, id) 
 * add_process(process, execution_order): Adds a process to the simulation. Allows to specific when it is executed, for example at the beginning of an step or the end.

Creating and running a simulation:

    # import the sim package and the configuration from the config.py file
    import sim
    import config
    
    # create the simulation and provide it the configuration
    simulation = sim.Simulation(config=config.world_config)
    
    # run the simulation for 10000 steps
    simulation.run(n_steps=10000)

#### Configuration

All aspects of the simulation can be configured using python files that contain the configuration as dictionaries.
This allows to have maximum flexibility in configuring the simulation.
The configuration file needs a dictionary *world_config* that contains the necessary information. 

Example of the *config.py*:

    world_config = {
        # size of the world in meters, here a rectangular room with 15x10m 
        'size': [[0., 15.],[0., 10.]],
    
        # folders with possible object classes
        'object_directories': ['./objects/']
        
        # folders with possible script classes
        'script_directories': ['./scripts/']
        
        # folders with possible script classes
        'component_directories': ['./components/']
        
        # folders with possible process classes
        'component_directories': ['./processes/']
                    
        # list of objects that are loaded
        'objects': [
            
            # walls (IDs: 1xxx)
            {'id': 1000, 'name': 'wall', 'position': [0., 0.], 'dimension': [0.1, 5.1], 'angle': 0.0 }
            {'id': 1001, 'name': 'wall', 'position': [0., 0.], 'dimension': [0.1, 5.1], 'angle': 0.0 }
            {'id': 1002, 'name': 'wall', 'position': [0., 0.], 'dimension': [0.1, 5.1], 'angle': 0.0 }
            {'id': 1003, 'name': 'wall', 'position': [0., 0.], 'dimension': [0.1, 5.1], 'angle': 0.0 }
    
            # furniture (IDs: 2xxx)
            {'id': 2000, 'name': 'table', 'position': [0., 0.], 'dimension': [0.1, 5.1], 'angle': 0.0 }
            {'id': 2001, 'name': 'chair', 'position': [0., 0.], 'dimension': [0.1, 5.1], 'angle': 0.0 }
            {'id': 2002, 'name': 'vending_machine', 'position': [0., 0.], 'dimension': [0.1, 5.1], 'angle': 0.0 }            
      
            # human agents (IDs: 3xxx)
            {'id': 3000, 'name': 'simple_human', 'position': [0., 0.], 'height': 1.75, 'angle': 0.0 }
            {'id': 3001, 'name': 'simple_human', 'position': [0., 0.], 'height': 1.63, 'angle': 0.0 }
     
            # robot agents (IDs: 4xxx)
            {'id': 4000, 'name': 'ari', 'position': [0., 0.],  'angle': 0.0 }   
      ]  
      
      # list of scripts that should be executed (IDs: 1xxxx)
      'scripts': [
            {'id': 10000, 'name': 'welcome_scenario_01'} 
      ]
      
      # processes such as the gui visualization
      'processes': [
            {'name': 'PyGameGUI', resolution=[1024, 768]}
      ]
    }

The *name* of objects and scripts are associated to python files with the same names.
Some basic objects and scripts are available with simulator.
Custom scripts, objects, and components can be imported by providing their directory under the *object_directories*, *script_directories*, and *component_directories* attributes.


#### Lifetime of Objects, Scripts, and Processes

Objects and Scripts can be reset to a reset point using the reset() method of the simulation.
To provide this functionality, entities can be in 3 different states:
 * non-permanent: 
    The entity was added to the simulation after a reset point was created. 
    If the simulation is reset, the entity will be deleted.
    Also note, if a non-permanent entity is removed, it will be deleted from the simulation.
 * permanent: 
    The entity was added before a reset point was created, and will be reset to its state of the creation of the reset point.
 * permanent-removed: 
    The entity was added before a reset point was created, and will be reset to its state at the creation of the reset point.
    At the current moment, the entity was removed from the simulation and is inactive, i.e. its step method will not be called.

Which state on entity is in can be identified using its `is_permanent` and `is_removed` properties.
Entities will change their status in the following manner:

                                                               |----------(reset)---------|                 
                                                               |                          |      
                                                               v                          |      
        --(add)--> NON-PERMANENT --(create reset point)--> PERMANENT --(remove)--> PERMANENT-REMOVED
                        |                                                                 |
                        |-------(remove/reset)------> DELETED <---(create reset point)----|     
    

Simulation entities (Objects, Components, Scripts and Processes) have several methods that are called by the simulation at specific points (creation, step, removal, ...) of the simulation.
They can be overwritten to control the entities behavior.


### Objects 

Each physical entity in the simulation (walls, furniture, humans, robots) are objects. 
Objects can be either specified in the world configuration or created via the Simulation.

Each object inherits from the base class *SimObject* and has some base attributes:
 * *id* (get): Unique identifier (integer) for the object.
 * *name* (get): Name of the object, e.g. 'simple_human' or 'wall'.
 * *pos* (get/set): Position of the object in the world. (nparray with [x, y])
 * *components* (get): Dictionary of components that define the functionality of the object, e.g. a pose generator for human agents.
 * *box2d_objects* (get): List of box2d objects that are associated with the object
 * *simulation* (get): Reference to the Simulation in which the object exists.
 
Methods:
 * add_component(component, name): adds a new component to the object. (Note: for the current version, components can only be added, not removed)
* _add():
* _create_in_box2d_world(box2d_world)
* _save_reset_state():
* _load_reset_state()
* _reset():
* _step():
* `_make_permanent()`: Called when object is made permanent.
* `_remove()`: Called when object is removed from simulation.
* _deactivate_in_box2d_world()
* _unremove()
* _activate_in_box2d_world()
* _close()
* _destroy_in_box2d_world()

Components can be either accessed via the *components* dictionary of by using their names similar to object properties.

#### Usage
 
Adding a new object to the simulation:

    human_agent = sim.agents.SimpleHuman(id=3100, pos=[1., 2.], angle=20.)
    simulation.add_object(human_agent)   

Adding and accessing a component:

    # generate a component and add it to an agent
    component = SpeechGenerator()
    human_agent.add_component(component, 'speech')
    
    # accessing the component via its name, here to say a welcome speech act 
    human_agent.speech.say('WELCOME', 'Bonjour. Je m\'appele Chris.')

#### List of provided Objects

 - ARI: ARI robot with its basic movement and sensor components.
 - Human: Human agent that has a social navigation and several generator components.
 - Wall
 - Table
 - Chair
 - VendingMachine?

### Object Components

Object components provide the functionality of objects.

Each component is inherited from the base class *ObjectComponent*.
Each component has the following properties:
 * *name* (get): String to identify the component.
 * *type* (get): 'generator' or 'sensor' (the step function of generators are called before that of sensors)
 * *active* (get/set): If the component is active or not. (True or False)
 * *object* (get): Reference to the Object under which the component exists.

Methods:
* _add():
* _save_reset_state():
* _load_reset_state()
* _reset():
* _step():
* _make_permanent():
* _remove():
* _unremove()
* _close()

Adding a component to an object:

    human_agent = sim.agents.SimpleHuman(id=3100, pos=[1., 2.], angle=20.)
    
    gaze_generator = sim.generators.GazeGeneration(randomness=0.2)
    human_agent.add_component(gaze_generator)

#### List of Components

This section lists components that are provided with the base simulation.
First, components for human agents are listed, followed by components for the ARI agent.

Human agent components:
 - SocialNavigationGenerator
 - SpeechGenerator
 - EmotionGenerator
 - AttentionGenerator?
 - GazeGenerator
 - PoseGenerator

ARI components:
 - ARINavigation
 - MappingAndLocalization (2D occupancy map, robot position and orientation, human position and orientation)
 - SemanticMapping (Semantic map with object descriptions, for example: table, chair, ...)
 - HumanTraitSensor (Sex, Age)
 - HumanActionSensor (Standing, Walking, Sitting, ...)
 - SpeechSensor (Speech Act, Speech Content)
 - EmotionSensor (Emotions such as anger, happy)
 - AttentionSensor? (Who does a human attend to?)
 - GazeSensor (Gaze direction)
 - PoseSensor (3D Pose)

### Scripts

Scripts are used to control the objects (things and agents) in the simulation.
Several scripts can be added to the simulator.

Each script is a python class and inherits from the base class *Script* and has a *reset* and *step* method.
The step method is called after each environment step.

Properties:
 * *id*: Unique Identifier.
 * *simulation*: Reference to the simulation.

Methods:
 * *reset()*: Called when the simulation resets.  
 * *step()*: Called after each simulation step.
* _add():
* _save_reset_state():
* _load_reset_state()
* _reset():
* _step():
* _make_permanent():
* _remove():
* _unremove()
* _close()


#### Example

The following example is for a script that creates a human agent at the beginning of the simulation.
Then it commands the agent to go to the ARI robot, which is supposed to have ID 4000.
When the agent reached ARI, it says a WELCOME message with the content 'Hello ARI.'.
Afterwards it waits until ARI answered and says a 'GOODBYE' message to it before leaving.

    import sim
    
    class WelcomeScenario01(sim.Script):
        @staticmethod 
        def default_config():
            config = super().default_config()
            config.target_robot_id = 4000
            config.start_pos = [1., 2.]
            config.end_pos = [6., 3.]
            return config
            
        def __init__(self, config=None, **kwargs):
            super().__init__(config, **kwargs)
            self.human = None
            self.status = None
            
        def reset(self):
            '''Create a human and let him walk to ARI.'''
            self.status = 'walk_to_ari'
            
            self.human = sim.object.SimpleHuman(pos=self.config.start_pos)
            self.simulation.add_object(self.human)
            
            # get a reference to the ARI agent in the simulation
            self.ari = self.simulation.objects[self.config.target_robot_id]
            
            # command the human agent to go to ARI
            self.human.navigation.set_goal(self.ari)
                        
        def step(self):
            '''Let the human agent walk to ARI, say hello, wait for a reply, say goodbye, wait for a reply, and leave.'''

            # if ARI is reached: say hello
            if self.status == 'walk_to_ari' and self.human.navigation.is_goal_reached: 
                self.human.speech.say('WELCOME', 'Hello ARI.')
                self.status = 'say_hello'
                
            # if ARI answered with a welcome message: say goodbye
            elif self.status == 'say_hello' and self.ari.speech.last_speech_act == 'WELCOME'
                self.human.speech.say('GOODBYE', 'Goodbye ARI.')
                self.status = 'say_good_bye'
            
            # if ARI finished his goodbye message, go away
            elif self.status == 'say_good_bye' and self.ari.speech.last_speech_act == 'GOODBYE'
                self.human.navigation.set_goal(self.config.end_pos)                
                self.status = 'go_away'
                
            # finish the simulation when the agent reached its goal
            elif self.status == 'go_away' and self.human.navigation.is_goal_reached:
                self.simulation.stop()
            
                
            
#### List of provided Scripts

This section lists scripts that are provided with the base simulation.
 - GroupNavigation

##### GroupNavigation

The *GroupNavigation* script can be used to control several agents as a consistent group.
A group has a certain group center around which the group will assemble. 
The group will have different parameters to control its parameters, such as their preferred distance to the group center which will be described in the future. 

Properties:
 * *agents*: List of agents that are part of the group and whos movement will be controlled by it.
 
Methods:
 * *add_agent(agent, id)*: Adds an agent to the group which is then controlled by it.
 * *remove_agent(agent, id)*: Removes an agent from the group.
 * *set_group_center(position)*: Sets the groups center position, to which all agents will move and gather around.  

Creating a group and setting its center position:

    # create agents and add them to the simulation
    human_agent_1 = sim.agents.SimpleHuman(id=3100, pos=[1., 2.], angle=20.)
    human_agent_2 = sim.agents.SimpleHuman(id=3101, pos=[4., 5.], angle=-20.)
    simulation.add_agent([human_agent_1, human_agent_2])
    
    # create a group and add it to the simulation
    group_1 = sim.scripts.SocialGroup(id=10001)
    simulation.add_script(group_1)
    
    # add the human agents to the group and set the group center
    group_1.add_agent([human_agent_1, human_agent_2])
    group_1.set_group_center([7., 3.])
    
    
### Processes

Processes represent background processes that are called every step of the simulation to perform various tasks.
For this purpose the Simulation holds a list of processes which have a *reset()* and *step()* method.
A process can be for example used to visualize the simulation using a GUI.
Furthermore, some objects, components, scripts, or other processes might make use of the same computational process.
For example, to compute the 2D map for the mapping and localization component of ARI and to visualize the map via the GUI, the map is created via pygame.
To avoid to create a map twice (for the component and the visualization), it is possible to create a common process that creates the map once per step and which can then be used by the component and the visualization.
An object, component, script, or process can then check if its necessary process exits already in the simulation.
If not, it creates the necessary process.
If the process exists already, then it can use it.

#### Interface

* _add():
* _save_reset_state():
* _load_reset_state()
* _reset():
* _step():
* _make_permanent():
* _remove():
* _unremove()
* _close()

#### List of provided Processes

 - MappingProcess (creates a 2D map of all the objects in the simulated environment)
 - PyGameGUI : The visualization provides a 2D overview of the environment with all objects (their associated box2d objects).
Visualization of specific features or actions is possible, such as the gaze direction of people or their current emotion.
 - Commandline (necessary?) 
   

### Data Logging

Data logging can be done in a first version individually by the scripts.
If needed we will provide an API to log and save specific information from specific objects and components. 


### ROS Integration

If needed it is possible to integrate the simulator with ROS.
In this case we have to write wrappers for each sensor component that provides its information after each step() as a ROS topic.
Furthermore, we have to provide wrappers for the ARINavigation component that takes ROS messages as input and executes them when the next step() is executed. 