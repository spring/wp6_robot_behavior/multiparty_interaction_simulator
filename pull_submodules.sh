#!/bin/bash

# both do the same, but one might sometimes fail
git submodule update --init --recursive
git pull --recurse-submodules
