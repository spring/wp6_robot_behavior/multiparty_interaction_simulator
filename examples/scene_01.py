#!/usr/bin/env python
# -*- coding: utf-8 -*-

from operator import ge
import mpi_sim as sim
from mpi_sim.core.module_library import get_object
from mpi_sim.objects import ari_robot
from mpi_sim.processes.gui import GUI
import numpy as np
import random


def main():
    emotion = ['Sad', 'Bored', 'Sleepy', 'Excited', 'Happy', 'Pleased', 'Relaxed', 'Peaceful', 'Calm', 'Annoying', 'Angry', 'Nervous']
    gender = ['F', 'M']
    age_min = 0
    age_max = 99
    world_config = {
        'visible_area': [[-10., 10.], [-10., 10.]],
        'objects': [
            {'type': 'Wall', 'id': 1, 'position': [6., 0], 'orientation': 0., 'length': 10.},
            {'type': 'Wall', 'id': 2, 'position': [-6., 0], 'orientation': 0., 'length': 10.},
            {'type': 'Wall', 'id': 3, 'position': [0., 4.9], 'orientation': np.pi/2, 'length': 12.2},
            {'type': 'Wall', 'id': 4, 'position': [0., -4.9], 'orientation': np.pi/2, 'length': 12.2,},
            {'type': 'Wall', 'id': 7, 'position': [2., 3.5], 'orientation': 0, 'length': 3.0,},
            {'type': 'Wall', 'id': 8, 'position': [2., -2.], 'orientation': 0, 'length': 6.0,},
            {'type': 'Bench', 'id': 5, 'position': [4., 4.3], 'orientation': 0},
            {'type': 'Bench', 'id': 6, 'position': [4., -4.3], 'orientation': np.pi},
            {'type': 'Bench', 'id': 9, 'position': [-4.8, 3.7], 'orientation': np.pi/4},
            {'type': 'RoundTable', 'id': 10, 'position': [-4., -2.], 'radius': 0.8},
            {'type': 'Chair', 'id': 12, 'position': [-5., -1.2], 'orientation': np.pi*0.27},
            {'type': 'Chair', 'id': 13, 'position': [-3.3, -1.], 'orientation': -np.pi*0.2},
            {'type': 'Chair', 'id': 14, 'position': [-5.1, -2.7], 'orientation': np.pi*0.7},
            {'type': 'Chair', 'id': 15, 'position': [-4., -3.3], 'orientation': np.pi},
            {'type': 'Chair', 'id': 16, 'position': [-2.8, -2.4], 'orientation': -np.pi*0.62},
            {'type': 'Bench', 'id': 17, 'position': [1.4, 3.4], 'orientation': -np.pi/2},
            {'type': 'Chair', 'id': 18, 'position': [1.4, -3.85], 'orientation': -np.pi*0.65},
            {'type': 'Chair', 'id': 19, 'position': [0.5, -4.4], 'orientation': np.pi},
            {'type': 'Chair', 'id': 20, 'position': [1.5, -2.9], 'orientation': -np.pi*0.5},
            {'type': 'Human', 'id': 30, 'position': [-1., 4], 'orientation': np.pi/6, 'emotion': random.choice(emotion), 'gender': random.choice(gender), 'age': random.randint(age_min, age_max), 'wearing_mask': bool(random.getrandbits(1))},
            {'type': 'Human', 'id': 33, 'position': [-2., 3], 'orientation': -np.pi/6, 'emotion': random.choice(emotion), 'gender': random.choice(gender), 'age': random.randint(age_min, age_max), 'wearing_mask': bool(random.getrandbits(1))},
            {'type': 'Human', 'id': 40,  'position': [-1., -1], 'orientation': -np.pi/6, 'emotion': random.choice(emotion), 'gender': random.choice(gender), 'age': random.randint(age_min, age_max), 'wearing_mask': bool(random.getrandbits(1)), 
                                                    'key_control': True, 'components': [{'type': 'SpeechGenerator'}]},
            {'type': 'ARIRobot', 'id': 50,  'position': [3., 2], 'orientation': np.pi*0.75, 'key_control': True}
        ],
        'processes': [
            {'type': 'GUI'},
        ],
        # 'scripts': [{'type': 'GroupNavigation'}],
    }

    simulation = sim.Simulation(world_config=world_config)
    human_11 = sim.objects.Human(
        position=[-1.0, 2.0],
        orientation=0.0,
        emotion=random.choice(emotion),
        gender=random.choice(gender),
        age=random.randint(age_min, age_max),
        wearing_mask=bool(random.getrandbits(1))
    )
    simulation.add_object(human_11)

    ari_robot = simulation.objects[50]
    # ari_robot.navigation.set_go_towards_human(30)
    # ari_robot.navigation.set_human_look_at(30)
    #ari_robot.navigation.set_go_towards_position([-4., 3.], 0.)

    group_1 = sim.scripts.GroupNavigation()
    simulation.add_script(group_1, id=1001)
    assert group_1.id == 1001
    group_1.add_agent([simulation.objects[33], simulation.objects[30]])
    group_1.set_group_center(position=[-1.7, 3.5])
    group_1.add_agent(human_11)

    # do 2 steps to allow the human to move

    simulation.run(n_steps=10)
    simulation.objects[33].speech.say('Hi', 'WELCOME')

    simulation.run(n_steps=10)
    simulation.objects[30].speech.say('Hello', 'WELCOME')

    simulation.run()

    simulation.close()


if __name__ == '__main__':
    main()
