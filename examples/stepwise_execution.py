import mpi_sim
import numpy as np
import time


NUM_OF_RESETS = 3
NUM_OF_MAX_STEPS = 1000

if __name__ == '__main__':

    # create an empty room
    simulation = mpi_sim.Simulation(
        visible_area=[[-10., 10.], [-10., 10.]],
        objects=[
            {'type': 'Wall', 'position': [6., 0], 'orientation': 0., 'length': 10.},
            {'type': 'Wall', 'position': [-6., 0], 'orientation': 0., 'length': 10.},
            {'type': 'Wall', 'position': [0., 4.9], 'orientation': np.pi / 2, 'length': 12.2},
            {'type': 'Wall', 'position': [0., -4.9], 'orientation': np.pi / 2, 'length': 12.2}
        ],
        processes=[{'type': 'GUI'}]
    )

    # add two humans
    human_1 = mpi_sim.objects.Human(position=[0., 0.], orientation=0.)
    human_1.navigation.goal_position = [1, 1]
    human_1.navigation.goal_orientation = - np.pi / 4
    simulation.add_object(human_1)

    human_2 = mpi_sim.objects.Human(position=[2., 2.], orientation=3 / 4 * np.pi)
    simulation.add_object(human_2)

    # set a reset point
    simulation.set_reset_point()

    for _ in range(NUM_OF_RESETS):

        simulation.reset()

        for _ in range(NUM_OF_MAX_STEPS):

            simulation.step()

            time.sleep(0.04)

            # stop the repetition when the human reaches its goal
            if mpi_sim.utils.measure_center_distance(human_1, [1, 1]) < 0.2:
                break

    simulation.stop()
