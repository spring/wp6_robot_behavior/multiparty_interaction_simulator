import gym
import exputils as eu
import mpi_sim
import numpy as np


class GymEnvironmentExample(gym.Env):
    r"""Example of using the MPI simulation as an gym environment to train an agent human aware navigation.

    The environment has several human agents that ARI has to avoid while navigating to a goal position.
    """

    @staticmethod
    def default_config():
        return eu.AttrDict(
            n_max_steps=5000,
            start_position = [-4., -4.],
            goal_position = [4., 4.],
            goal_position_radius = 0.1,
            n_humans = 4,
            humans_start_region = [[-3., 3.], [-3., 3.]], # [[x_min, x_max], [y_min, y_max]]
            humans_goal_region = [[-4., 4.], [-4., 4.]], # [[x_min, x_max], [y_min, y_max]]
            human_goal_position_radius = 0.1,
            show_gui = False,
            max_real_time_factor = 0.0,
        )


    def __init__(self, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.cur_step = None

        processes_config = []
        if self.config.show_gui:
            processes_config.append({'type': 'GUI'})

        # create simulation
        self.simulation = mpi_sim.Simulation(
            world_size=[[-5., 10.], [-5., 5.]],
            max_real_time_factor=self.config.max_real_time_factor,
            objects=[
                {'type': 'Wall', 'position': [6., 0], 'orientation': 0., 'length': 10.},
                {'type': 'Wall', 'position': [-6., 0], 'orientation': 0., 'length': 10.},
                {'type': 'Wall', 'position': [0., 4.9], 'orientation': np.pi / 2, 'length': 12.2},
                {'type': 'Wall', 'position': [0., -4.9], 'orientation': np.pi / 2, 'length': 12.2}
            ],
            processes=processes_config
        )

        # create an ari agent, that does mapping
        self.agent = mpi_sim.objects.ARIRobot(
            components=[mpi_sim.objects.ARIRobot.default_mapping_config()],
            position=self.config.start_position,
            orientation=0.
        )
        self.simulation.add_object(self.agent)

        # place holder for the humans and their goals
        self.humans = []
        self.human_goals = []

        self.simulation.set_reset_point()

        # define spaces
        self.state_space = gym.spaces.Dict(
            occupancy_grid = gym.spaces.Box(
                low=0.0, high=1.0, shape=self.agent.mapping.local_map_shape
            ),
            agents = gym.spaces.Sequence(gym.spaces.Tuple((
                gym.spaces.Text(min_length=1, max_length=1, charset={'h', 'a'}),
                gym.spaces.Box(0.0, np.inf),
                gym.spaces.Box(-np.pi, np.pi)
            )))
        )
        self.action_space = gym.spaces.Discrete(4)
        self.reward_range = (-np.inf, 0.0)


    def reset(self, seed = None):
        observation, info = None, None

        # erase old humans
        self.humans = []
        self.human_goals = []

        self.cur_step = 0

        # set a reset point before the first reset and execution
        if not self.simulation.has_reset_point:
            self.simulation.set_reset_point()
        else:
            self.simulation.reset()

        # create humans with their start condition and goal position

        rand_nums = np.random.rand(self.config.n_humans, 3)

        min_start_x, max_start_x = self.config.humans_start_region[0]
        min_start_y, max_start_y = self.config.humans_start_region[1]

        # add humans with random positions
        for h_idx in range(self.config.n_humans):
            # sample start pos
            start_x_pos = min_start_x + rand_nums[h_idx][0] * (max_start_x - min_start_x)
            start_y_pos = min_start_y + rand_nums[h_idx][1] * (max_start_y - min_start_y)
            start_orientation = -np.pi + rand_nums[h_idx][2] * 2 * np.pi

            # add human to simulation
            human = mpi_sim.objects.Human(
                position=[start_x_pos, start_y_pos],
                orientation=start_orientation
            )
            self.simulation.add_object(human)

            # sample a goal for it
            goal = self.sample_human_goal_position(human)

            self.humans.append(human)
            self.human_goals.append(goal)

            human.navigation.goal_position = goal[:2]
            human.navigation.goal_orientation = goal[2]

        self.agent.box2d_body.position = [3,3]

        return observation, info


    def step(self, action):

        reward = 0.0

        self.cur_step += 1

        # set ari's action:
        #   0: stop
        #   1: go straight
        #   2: go right
        #   3: go left
        if action == 0:
            linear_velocity = 0.0
            angular_velocity = 0.0
        elif action == 1:
            linear_velocity = 1.0
            angular_velocity = 0.0
        elif action == 2:
            linear_velocity = 0.8
            angular_velocity = -1.0
        elif action == 3:
            linear_velocity = 0.8
            angular_velocity = 1.0
        else:
            raise ValueError('Unknown action {!r}!'.format(action))

        self.agent.forward_velocity = linear_velocity
        self.agent.orientation_velocity = angular_velocity

        # simulation step
        self.simulation.step()

        # create the list of agents and humans
        agent_list = []
        agent_list.append(['a', np.array(self.agent.position), self.agent.orientation])

        for human_idx, human in enumerate(self.humans):
            agent_list.append(['h', np.array(human.position), human.orientation])

            # distance of ari to humans
            # humans have a radius of 0.25 and ari of ~0.22
            # thus give punishment if too close
            dist_of_ari_to_human = mpi_sim.utils.measure_center_distance(self.agent, human)
            if dist_of_ari_to_human < 0.75:
                reward -= 10.0

            # check if the human reached its goal position, if yes sample a new one
            human_goal = self.human_goals[human_idx]
            human_distance_to_goal = mpi_sim.utils.measure_center_distance(human, [human_goal[0], human_goal[1]]) # distance to x,y goal
            if human_distance_to_goal <= self.config.human_goal_position_radius:
                new_goal_position = self.sample_human_goal_position(human)
                self.human_goals[human_idx] = new_goal_position

                human.navigation.goal_position = new_goal_position[:2]
                human.navigation.goal_orientation = new_goal_position[2]

        observation = eu.AttrDict(
            occupancy_grid = self.agent.mapping.local_map,
            agents = agent_list,
        )

        # calc dist to goal reward: inverse distance to the goal
        distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.config.goal_position)
        reward += -1.0 * distance_to_goal

        terminated = False
        if distance_to_goal <= self.config.goal_position_radius:
            terminated = True

        truncated = False
        if self.cur_step >= self.config.n_max_steps:
            truncated = True

        info = {}

        return observation, reward, terminated, truncated, info


    def sample_human_goal_position(self, human):

        rand_nums = np.random.rand(3)

        min_goal_x, max_goal_x = self.config.humans_goal_region[0]
        min_goal_y, max_goal_y = self.config.humans_goal_region[1]

        goal_x_pos = min_goal_x + rand_nums[0] * (max_goal_x - min_goal_x)
        goal_y_pos = min_goal_y + rand_nums[1] * (max_goal_y - min_goal_y)
        goal_orientation = -np.pi + rand_nums[2] * 2 * np.pi

        human.goal_position = [goal_x_pos, goal_y_pos]
        human.goal_orientation = goal_orientation

        return goal_x_pos, goal_y_pos, goal_orientation


if __name__ == '__main__':

    env = GymEnvironmentExample(show_gui=True, max_real_time_factor=None)

    # episodes
    for _ in range(3):
        env.reset()

        # steps
        for _ in range(100):
            transition = env.step(env.action_space.sample())