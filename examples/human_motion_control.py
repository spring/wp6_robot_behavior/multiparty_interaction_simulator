#!/usr/bin/env python
# -*- coding: utf-8 -*-
import mpi_sim as sim
import numpy as np


def run_scenario_01():
    world_config = {
        'visible_area': [[-10., 10.], [-10., 10.]],
        'objects': [
            {'type': 'Wall', 'id': 1, 'position': [6., 0], 'orientation': 0., 'length': 10.},
            {'type': 'Wall', 'id': 2, 'position': [-6., 0], 'orientation': 0., 'length': 10.},
            {'type': 'Wall', 'id': 3, 'position': [0., 4.9], 'orientation': np.pi/2, 'length': 12.2},
            {'type': 'Wall', 'id': 4, 'position': [0., -4.9], 'orientation': np.pi/2, 'length': 12.2,},
        ],
        'processes': [
            {'type': 'GUI'},
        ],
        'max_real_time_factor' : 1.0,
    }

    simulation = sim.Simulation(world_config=world_config)

    human_10 = sim.objects.Human(
        position=[1.5, 1.],
        orientation=0.,
    )
    simulation.add_object(human_10, id=10)

    human_11 = sim.objects.Human(
        position=[0., 0.],
        orientation=0.,
        goal_distance_threshold = 0.2,
        step_length = 0.05,
    )
    simulation.add_object(human_11, id=11)
    human_11.navigation.goal_position = [3, 3]
    human_11.navigation.goal_orientation = 3 / 4 * np.pi

    human_10.speech.say('hello')

    simulation.run(n_steps=100)

    human_11.speech.say('hi')

    simulation.run(n_steps=200)

    simulation.close()


if __name__ == '__main__':
    run_scenario_01()