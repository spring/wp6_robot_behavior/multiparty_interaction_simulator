import mpi_sim
import numpy as np

# TODO: the script does not work anymore, something changed within the MPC

class GroupDiscussionScript(mpi_sim.Script):

    def _add(self):
        """Define what to do when script is added to simulation."""

        # add two humans (paul and irene)
        self.paul = mpi_sim.objects.Human(position=[1., 1.], orientation=0.)
        self.simulation.add_object(self.paul)
        self.irene = mpi_sim.objects.Human(position=[4., 1.], orientation=0.)
        self.simulation.add_object(self.irene)

        # add ARI
        self.ari = mpi_sim.objects.ARIRobot(position=[1., 5.], orientation=-np.pi * 3 / 4)
        self.simulation.add_object(self.ari)

        # create a group
        self.group = mpi_sim.scripts.GroupNavigation()
        self.simulation.add_script(self.group)
        self.group.add_agent([self.paul, self.irene])

        # define a goal for the group and let ari join the group by giving on person as target
        self.group.set_group_center([2.0, 3.0])
        self.ari.navigation.set_go_towards_human(self.paul)

        self.status = 'grouping'

    def _step(self):
        """Define what to do in each time step."""

        # if ARI is reached: ask if he can help
        if self.status == 'grouping' and mpi_sim.utils.measure_center_distance(self.irene, self.ari) < 1.6:
            self.ari.speech.say('Hello, I am ARI. Can I help you?', act='QUESTION:HELP')
            self.status = 'ask_help'

        # ask ARI for the time
        elif self.status == 'ask_help' and self.ari.speech.previous_speech_act == 'QUESTION:HELP':
            self.irene.speech.say('Hello. Yes. What is the time?', act='QUESTION:TIME')
            self.status = 'ask_time'

        # after the question is asked, let ARI answer
        elif self.status == 'ask_time' and self.irene.speech.previous_speech_act == 'QUESTION:TIME':
            self.ari.speech.say('It is 14:30.', act='ANSWER:TIME')
            self.status = 'answer_time'

        # after ARI answered, thank it
        elif self.status == 'answer_time' and self.ari.speech.previous_speech_act == 'ANSWER:TIME':
            self.irene.speech.say('Thank you. Good bye.', 'GOODBYE')
            self.status = 'thank_ari'

        # finally, let ARI leave
        elif self.status == 'thank_ari' and self.irene.speech.previous_speech_act == 'GOODBYE':
            self.ari.speech.say('Good Bye', act='GOODBYE')
            self.ari.navigation.set_go_towards_position([4., 4.], -3 / 4 * np.pi)
            self.status = 'leave'

        # stop the simulation after ari left
        elif self.status == 'leave' and mpi_sim.utils.measure_center_distance(self.ari, [4., 4.]) < 0.5:
            simulation.stop()


# create an empty room
simulation = mpi_sim.Simulation(
    visible_area = [[0., 10.], [0., 10.]],
    objects = [ 
        {'type': 'Wall', 'position': [6., 3.], 'orientation': 0., 'length': 6.}, # east
        {'type': 'Wall', 'position': [0., 3.], 'orientation': 0., 'length': 6.}, # west
        {'type': 'Wall', 'position': [3., 6.0], 'orientation': np.pi / 2, 'length': 6.2}, # south
        {'type': 'Wall', 'position': [3., 0.0], 'orientation': np.pi / 2, 'length': 6.2} # north
    ],
    processes = [{'type': 'GUI'}],
    max_real_time_factor = 1.0
)

# create the script and add it to the simulation
script = GroupDiscussionScript()
simulation.add_script(script)

# run the simulation, until the script stops the simulation
simulation.run()
simulation.close()
