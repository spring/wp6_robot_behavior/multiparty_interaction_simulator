import mpi_sim
import numpy as np


class AskTimeScript(mpi_sim.Script):

    def _add(self):
        """Define what to do when script is added to simulation."""

        # add a human and ARI
        self.human = mpi_sim.objects.Human(position=[0., 0.], orientation=0.)
        self.simulation.add_object(self.human)

        self.ari = mpi_sim.objects.Human(position=[2., 2.], orientation=3 / 4 * np.pi)
        self.simulation.add_object(self.ari)

        # let the human walk in front of ARI
        self.human.navigation.goal_position = [1, 1]
        self.human.navigation.goal_orientation = - np.pi / 4
        self.status = 'join_ari'


    def _step(self):
        """Define what to do in each time step."""

        # if ARI is reached: ask for the time
        if self.status == 'join_ari' and mpi_sim.utils.measure_center_distance(self.human, self.ari) < 1.5:
            self.human.speech.say('Hello ARI. What is the time.', act='QUESTION:TIME')
            self.status = 'ask_time'

        # after the question is asked, let ARI answer
        elif self.status == 'ask_time' and self.human.speech.previous_speech_act == 'QUESTION:TIME':
            self.ari.speech.say('Hello. It is 14:30.', act='ANSWER:TIME')
            self.status = 'answer_time'

        # after ARI answered, thank it
        elif self.status == 'answer_time' and self.ari.speech.previous_speech_act == 'ANSWER:TIME':
            self.human.speech.say('Thank you. Good bye.', 'THANKING')
            self.status = 'thank_ari'

        # finally, leave ARI
        elif self.status == 'thank_ari' and self.human.speech.previous_speech_act == 'THANKING':
            self.human.navigation.goal_position = [0, 0]
            self.status = 'leave'

        # stop the simulation after the human left
        elif self.status == 'leave' and mpi_sim.utils.measure_center_distance(self.human, [0.0]) < 0.5:
            simulation.stop()


# create an empty room
simulation = mpi_sim.Simulation(
    visible_area = [[-10., 10.],[-10., 10.]],
    objects = [ 
        {'type': 'Wall', 'position': [6., 0], 'orientation': 0., 'length': 10.},
        {'type': 'Wall', 'position': [-6., 0], 'orientation': 0., 'length': 10.},
        {'type': 'Wall', 'position': [0., 4.9], 'orientation': np.pi/2, 'length': 12.2},
        {'type': 'Wall', 'position': [0., -4.9], 'orientation': np.pi/2, 'length': 12.2}
    ],
    processes = [{'type': 'GUI'}],
    max_real_time_factor = 1.0
)

# create the script and add it to the simulation
script = AskTimeScript()
simulation.add_script(script)

# run the simulation, until the script stops the simulation
simulation.run()
simulation.close()
