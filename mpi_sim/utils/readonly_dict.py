import collections


class ReadOnlyDict(collections.Mapping):

    def __init__(self, data=None):

        if data is None:
            self._data = dict()
        else:
            self._data = data


    def __getitem__(self, key):
        return self._data[key]


    def __len__(self):
        return len(self._data)


    def __iter__(self):
        return iter(self._data)
