import mpi_sim
import numpy as np
import cv2
from collections import namedtuple
import mpi_sim.utils.truong_2018 as ssn


SocialMap = namedtuple('SocialMap', ['map', 'area', 'resolution'])

def get_social_map_shape(area, resolution):
    r"""Calculates the size of the social map that can be created via create_social_map().

   Args:
        area (tuple of floats): Area of the simulation that should be mapped in ((x_min, x_max), (y_min, y_max)).
        resolution (float): Resolution in meter per cell.

    Returns:
        Shape of the map as a tuple: (width, height).
    """

    x_width = area[0][1] - area[0][0]
    y_width = area[1][1] - area[1][0]

    width = int(x_width / resolution)
    height = int(y_width / resolution)
    shape = (width, height)

    return shape

def get_social_map_resolution(area, shape):
    r"""Calculates the resolution of the social map that can be created via create_social_map().

   Args:
        area (tuple of floats): Area of the simulation that should be mapped in ((x_min, x_max), (y_min, y_max)).
        shape (tuple): Shape of the map as a tuple: (width, height)

    Returns:
        Resolution in meter per cell. (float)
    """

    x_width = area[0][1] - area[0][0]
    #y_width = area[1][1] - area[1][0]

    (width, height) = shape
    
    resolution = int(x_width / width)
    #resolution = int(y_width / height)

    return resolution


def transform_position_to_map_coordinate(position, map_area, map_resolution):
    r"""Transform a position (x, y) in the simulation to a coordinate (x, y) of a map of the simulation.

    Args:
        position (x,y): x,y - Position that should be transformed.
        map_area (tuple): Area ((x_min, x_max), (y_min, y_max)) of the simulation that the map is depicting.
        map_resolution (float): Resolution of the map in m per cells.

    Returns: Map coordinate as a tuple (x, y).
    """
    x_min = map_area[0][0]
    y_min = map_area[1][0]
    x = int((position[0] - x_min) / map_resolution)
    y = int((position[1] - y_min) / map_resolution)
    return x, y


def transform_map_coordinate_to_position(map_coordinate, map_area, map_resolution):
    r"""Transform a map coordinate (x, y) of a map to a position (x, y) in the simulation.

    Args:
        map_coordinate (x,y): x,y - Map coordinate that should be transformed.
        map_area (tuple): Area ((x_min, x_max), (y_min, y_max)) of the simulation that the map is depicting.
        map_area (float): Resolution of the map in m per cells.

    Returns: Position as a tuple (x, y).
    """
    x_min = map_area[0][0]
    y_min = map_area[1][0]
    x = map_coordinate[0] * map_resolution + x_min
    y = map_coordinate[1] * map_resolution + y_min
    return x, y


def create_social_map(simulation, area=None, resolution=0.05, human_velocities=None, flag_imported_global_map=False):
    r"""Creates an social map.

    The size of the map depends on the visible_map_area of the simulation and the resolution of the map.
    The map encodes the x-y position via map[x,y].

    Args:
        simulation (mpi_sim.Simulation): Simulation for which a map should be created.
        area (tuple):   Area of the simulation that should be mapped in ((x_min, x_max), (y_min, y_max)). (Default: None)
                        If None, it uses the simulation.visible_area.
        resolution (float): Resolution in meter per cell (Default: 0.05).
        human_velocities (list) : List of [x,y] velocities of humans

    Returns:
        social map in form of a SocialMap (namedtuple with map, area, and resolution).
    """

    if area is None:
        area = simulation.visible_area

    # the size of the map depends on the world size and the resolution

    # visible_area: ((min_x, max_x), (min_y, max_y))
    x_width = area[0][1] - area[0][0]
    y_width = area[1][1] - area[1][0]
    width = x_width / resolution
    height = y_width / resolution

    

    object_filter = dict(types=mpi_sim.objects.Human)

    bodies = simulation.box2d_simulation.get_bodies(
        object_types=object_filter.get('types', None),
        object_ids=object_filter.get('ids', None),
        object_properties= object_filter.get('properties', None),
        )
    
    persons = []
    for id, bodie in enumerate(bodies) :
        x, y = np.array(bodie.position)
        theta = np.pi/2 + bodie.angle
        vx,vy = 0,0
        if human_velocities is not None:
            if len(human_velocities) > 0 :
                vx = human_velocities[id][0]
                vy = human_velocities[id][1]
        v = np.sqrt(vx**2 + vy**2)
        persons.append((x ,y, theta, v, False))


    x_coordinates = np.linspace(area[0][1], area[0][0], int(width))
    y_coordinates = np.linspace(area[1][1], area[1][0], int(height))
    x_mesh, y_mesh = np.meshgrid(x_coordinates, y_coordinates)
    
    if not flag_imported_global_map:
        global_social_map = np.flip(np.flip(ssn.calc_eps(x_mesh, y_mesh, persons), axis=1), axis=0).T
    else :
        global_social_map = np.flip(ssn.calc_eps(x_mesh, y_mesh, persons), axis=1)

    map = SocialMap(map=global_social_map, area=area, resolution=resolution)

    return map


def create_local_perspective_social_map(global_map, depth=5.0, position=(0,0), orientation=0.0, from_given_center=None, transform_position_to_map_coordinate=transform_position_to_map_coordinate):
    r"""Creates a local perspective of an social map.

    Args:
        global_map (SocialMap): Global social map which is the source for the local perspective.
        depth (float): Viewing depth of the map in meters. The map size is 2 * depth. (Default: 5)
        position (tuple of floats): Position (x,y) of the local map center in the global map. (Default: (0,0))
        orientation (float): Orientation of the map in rad. (Default: 0)
                             0 for north, -pi/2 for east, pi/2 for west, pi for south

    Returns: Local perspective of the social map in form of a SocialMap (named tuple with map, area, and resolution)

    """

    # see https://theailearner.com/tag/cv2-warpperspective/
    # Attention: opencv uses a different coordinate frame to us, thus the code additionally transforms the coordinate frames by
    # having the opposite rotation direction, and using transpose operations

    # TODO: there should be a way to avoid the transpose operations, by choosing a different points that should be

    local_map_length = int(depth * 2 / global_map.resolution)
    
    if from_given_center is None :
        pos_in_map = transform_position_to_map_coordinate(
            position,
            map_area=global_map.area,
            map_resolution=global_map.resolution
        )
    else :
        pos_in_map = transform_position_to_map_coordinate(
            center_position=from_given_center,
            position=position,
            map_resolution=global_map.resolution,
        )

    orientation = mpi_sim.utils.constraint_angle(orientation)

    # rotation in for cv operator is in opposite direction
    hvect = np.array([np.cos(-orientation), -np.sin(-orientation)])
    vvect = np.array([np.sin(-orientation), np.cos(-orientation)])
    # print('hvect', hvect)
    # print('vvect', vvect)

    # source points in pixel space
    rect_ll = pos_in_map - 0.5 * hvect * local_map_length - 0.5 * vvect * local_map_length
    rect_ul = pos_in_map - 0.5 * hvect * local_map_length + 0.5 * vvect * local_map_length
    rect_ur = pos_in_map + 0.5 * hvect * local_map_length + 0.5 * vvect * local_map_length
    rect_lr = pos_in_map + 0.5 * hvect * local_map_length - 0.5 * vvect * local_map_length
    src_pts = np.vstack([rect_ll, rect_ul, rect_ur, rect_lr]).astype(np.float32)
    # print('src_pts', src_pts)

    # destination points in pixel space
    dst_pts = np.array(
        [[0, 0],
         [0, local_map_length],
         [local_map_length, local_map_length],
         [local_map_length, 0]],
        dtype=np.float32
    )
    # print('dst_pts', dst_pts)

    m = cv2.getPerspectiveTransform(src_pts, dst_pts)
    # use transpose operations to bring image into coordinate frame of cv and then back to our coordinate system
    local_map = cv2.warpPerspective(
        global_map.map.T,
        m,
        (local_map_length, local_map_length)
    ).T

    return SocialMap(map=local_map, resolution=global_map.resolution, area=None)
