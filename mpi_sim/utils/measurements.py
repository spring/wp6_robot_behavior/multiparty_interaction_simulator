import numpy as np
import mpi_sim
import collections
import Box2D
from Box2D import b2CircleShape


def measure_center_distance(p1, p2):
    """Measures the distance between the center of two objects, bodies, or points in meters."""

    if isinstance(p1, (mpi_sim.Object, Box2D.b2Body)):
        pos1 = p1.position
    else:
        pos1 = np.array(p1)

    if isinstance(p2, (mpi_sim.Object, Box2D.b2Body)):
        pos2 = p2.position
    else:
        pos2 = np.array(p2)

    return np.linalg.norm(pos1 - pos2)


DistanceInfo = collections.namedtuple('DistanceInfo', ['distance', 'point_a', 'point_b'])


def measure_distance(p1, p2, simulation=None):
    """Measures the minimum distance between two points or objects (their bodies and shapes) in meters."""

    if not isinstance(p1, mpi_sim.Object) and not isinstance(p2, mpi_sim.Object):
        point_a = np.array(p1)
        point_b = np.array(p2)
        distance = np.linalg.norm(point_a - point_b)

    elif isinstance(p1, mpi_sim.Object) and isinstance(p2, mpi_sim.Object):

        # go through all bodies and their shape combinations of the two objects and check their distances
        distance = np.inf
        point_a = None
        point_b = None

        for body_a in p1.box2d_bodies:
            for fixture_a in body_a.fixtures:

                # fixture must have a shape
                if not fixture_a.shape:
                    continue

                for body_b in p2.box2d_bodies:
                    for fixture_b in body_b.fixtures:

                        # fixture must have a shape
                        if not fixture_b.shape:
                            continue

                        b2_dist_info = Box2D.b2Distance(
                            shapeA=fixture_a.shape,
                            shapeB=fixture_b.shape,
                            transformA=body_a.transform,
                            transformB=body_b.transform
                        )

                        if b2_dist_info.distance < distance:
                            distance = b2_dist_info.distance
                            point_a = np.array(b2_dist_info.pointA)
                            point_b = np.array(b2_dist_info.pointB)

    else:
        # To measure the distance between an object and a position we create a temporary object
        # Then we measure the distance between the new temporary object and the original object
        if simulation is None:
            raise UserWarning('The distance measurement between an object and a position needs the simulation')
        
        # In case the object is given in firt or second position
        if isinstance(p1, mpi_sim.Object) :
            _object = p1
            _point = p2
        else :
            _object = p2
            _point = p1

        distance = np.inf
        point_a = None
        point_b = None

        # Creating an object to measure the distance from
        fake_dot_obj = mpi_sim.objects.RoundTable(
            position=_point,
            orientation=0,
            radius = 0.000001,
        )
        simulation.add_object(fake_dot_obj)

        # Measure the min distance for each bodies and each fixture
        for body_a in fake_dot_obj.box2d_bodies:
            for fixture_a in body_a.fixtures:

                # fixture must have a shape
                if not fixture_a.shape:
                    continue

                for body_b in _object.box2d_bodies:
                    for fixture_b in body_b.fixtures:

                        # fixture must have a shape
                        if not fixture_b.shape:
                            continue

                        b2_dist_info = Box2D.b2Distance(
                            shapeA=fixture_a.shape,
                            shapeB=fixture_b.shape,
                            transformA=body_a.transform,
                            transformB=body_b.transform
                        )

                        if b2_dist_info.distance < distance:
                            distance = b2_dist_info.distance
                            point_a = np.array(b2_dist_info.pointA)
                            point_b = np.array(b2_dist_info.pointB)
        
        # Remove the fake object from simulation
        simulation.remove_object(fake_dot_obj)
        
        # Correct the distance measurement w.r.t to its radius
        distance += 0.000001

    return DistanceInfo(distance, point_a, point_b)


def constraint_angle(angle, min_value=-np.pi, max_value=np.pi):
    """Constrains the given angle between -pi and pi."""

    length = max_value - min_value

    # if angle > max_value:
    #     diff = angle - max_value
    #     new_angle = min_value + (diff % length)
    # elif angle < min_value:
    #     diff = min_value - angle
    #     new_angle = max_value - (diff % length)
    # else:
    #     new_angle = angle
    new_angle = np.where(angle > max_value, min_value + ((angle - max_value) % length), angle)
    new_angle = np.where(angle < min_value, max_value - ((min_value - angle) % length), new_angle)

    return new_angle


def angle_difference(angle1, angle2):
    """Calculates the difference between two angles. Takes into account that the angle has a discontinuity at -pi <-> pi.
    The difference can be negative or positive which encodes the direction in which they differ.

    Examples:   angle1 = 0.0, angle2 = -0.1 --> diff = -0.1
                angle1 = 0.0, angle2 = 0.1  --> diff = 0.1
                angle1 = 3.0, angle2 = -3.0 --> diff = 0.0
    """
    return constraint_angle(angle2 - angle1)


def measure_relative_angle(x, y):
    """Measures the relative angle between two objects or points.

    Args:
        x (Object or point): Object (mpi_sim.Object with position and orientation property) or point ([x, y, angle]) of the source.)
        y (Object or point): Object (mpi_sim.Object with position and orientation property) or point ([x, y]) of the target.

    Returns: The angle can be positive or negative indicating the direction.
    """

    if isinstance(x, mpi_sim.core.BaseEntity):
        from_position = x.position
        from_orientation = x.orientation
    else:
        from_position = np.array(x[0:2])
        from_orientation = x[2]

    if isinstance(y, mpi_sim.core.BaseEntity):
        to_position = y.position
    else:
        to_position = np.array(y)

    position_diff = to_position - from_position

    if position_diff[0] == 0. and position_diff[1] == 0.:
        angle = 0.
    else:
        angle_between_p1_and_p2 = -np.arctan2(position_diff[0], position_diff[1])
        angle = constraint_angle(angle_between_p1_and_p2 - from_orientation)

    return angle




