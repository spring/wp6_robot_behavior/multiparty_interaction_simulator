import mpi_sim
import numbers
import numpy as np

class RandomNumberSampling:

    @staticmethod
    def default_config():
        dc = mpi_sim.AttrDict()
        dc.n_samples = 10000
        dc.is_repeat = True
        return dc


    def __init__(self, config=None, **kwargs):
        self.config = mpi_sim.combine_dicts(kwargs, config, self.default_config())

        if self.config.n_samples is None or self.config.n_samples <= 0:
            self.sample_idx = None
            self.samples = None
        else:
            self.sample_idx = 0
            self.samples = np.random.rand(self.config.n_samples)


    def rand(self, n_samples=1):

        if self.sample_idx + n_samples > len(self.samples) - 1:
            self.sample_idx = 0
            if not self.config.is_repeat:
                self.samples = np.random.rand(self.config.n_samples)

        samples = self.samples[self.sample_idx:self.sample_idx+n_samples]

        self.sample_idx += n_samples

        if n_samples == 1:
            samples = samples[0]

        return samples


def sample_value(rnd=None, config=None):
    """Samples scalar values depending on the provided properties."""

    if rnd is None:
        rnd = np.random.RandomState()

    val = None

    if isinstance(config, numbers.Number): # works also for booleans
        val = config

    elif config is None:
        val = rnd.rand()

    elif isinstance(config, tuple):

        if config[0] == 'continuous':
            val = config[1] + (rnd.rand() * (config[2] - config[1]))

        elif config[0] == 'discrete':
            val = rnd.randint(config[1], config[2] + 1)

        elif config[0] == 'function' or config[0] == 'func':
            val = config[1](rnd, *config[2:])    # call function and give the other elements in the tupel as paramters

        elif len(config) == 2:
            val = config[0] + (rnd.rand() * (config[1] - config[0]))

        else:
            raise ValueError('Unknown parameter type {!r} for sampling!', config[0])

    elif isinstance(config, list):
        val = config[rnd.randint(len(config))] # do not use choice, because it does not work with tuples

    elif isinstance(config, dict):
        if config['type'] == 'discrete':
            val = rnd.randint(config['min'], config['max'] + 1)

        elif config['type'] == 'continuous':
            val = config['min'] + (rnd.rand() * (config['max'] - config['min']))

        elif config['type'] == 'boolean':
            val = bool(rnd.randint(0,1))

        else:
            raise ValueError('Unknown parameter type {!r} for sampling!', config['type'])

    return val


def mutate_value(val, mutation_factor=1.0, rnd=None, config=None, **kwargs):

    new_val = val

    if isinstance(val, list) or isinstance(val, np.ndarray):
        for idx in range(np.shape(val)[0]):
            new_val[idx] = mutate_value(new_val[idx], mutation_factor=mutation_factor, rnd=rnd, config=config, **kwargs)
    else:

        if rnd is None:
            rnd = np.random.RandomState()

        if config is None:
            config = kwargs
        else:
            config = {**config, **kwargs}

        if config and isinstance(config, dict):

            if 'distribution' in config:
                if config['distribution'] == 'gauss' or config['distribution'] == 'gaussian' or config['distribution'] == 'normal':
                    new_val = rnd.normal(val, config['sigma'] * max(0, mutation_factor))
                else:
                    raise ValueError('Unknown parameter type {!r} for mutation!', config['type'])

            if 'min' in config:
                new_val = max(new_val, config['min'])

            if 'max' in config:
                new_val = min(new_val, config['max'])

            if 'type' in config:
                if config['type'] == 'discrete':
                    new_val = np.round(new_val)
                elif config['type'] == 'continuous':
                    pass
                else:
                    raise ValueError('Unknown parameter type {!r} for mutation!', config['type'])

    return new_val


def sample_vector(rnd=None, config=None):

    val = None

    if isinstance(config, tuple):

        vector_length = int(sample_value(rnd, config[0]))

        val = [None]*vector_length
        for idx in range(vector_length):
            val[idx] = sample_value(rnd, config[1])

    else:
        raise ValueError('Unknown config type for sampling of a vactor!')

    return np.array(val)

