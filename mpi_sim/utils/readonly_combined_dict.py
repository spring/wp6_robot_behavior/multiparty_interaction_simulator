import collections


class ReadOnlyCombinedDict(collections.Mapping):
    r"""A read only combination of several list that can be iterated and where items can be accessed, but not added or deleted."""

    def __init__(self, data=None):

        if data is None:
            self._data = []
        elif isinstance(data, list):
            self._data = data
        else:
            raise ValueError('data must be a list of dictionaries!')


    def __getitem__(self, key):

        item = None
        for cur_dict in self._data:
            item = cur_dict.get(key)
            if item is not None:
                break

        if item is None:
            raise KeyError(key)

        return item


    def __len__(self):
        total_len = 0
        for cur_dict in self._data:
            total_len += len(cur_dict)
        return total_len


    def __iter__(self):
        return self._next_generator()


    def _next_generator(self):
        for sub_dict in self._data:
            for key in sub_dict.keys():
                yield key


    #
    #
    #
    # def __iter__(self):
    #     return self._next_generator()
    #
    #
    # def _next_generator(self):
    #     for sub_list in self.data:
    #         for elem in sub_list:
    #             yield elem
    #
    #
    # def __getitem__(self, item):
    #
    #     cur_max_idx = 0
    #     responsible_list_idx = -1
    #     while cur_max_idx <= item:
    #         cur_max_idx += len(self.data)
    #         responsible_list_idx += 1
    #
    #     if cur_max_idx < item:
    #         raise IndexError('Index {} out of range! Max size: {}.'.format(item, len(self)))
    #
    #     # return item, use indexing from end of list (list[-n])
    #     return self.data[responsible_list_idx][item - cur_max_idx]
    #
    #
    # def __len__(self):
    #
    #     n_elements = 0
    #     for list in self.data:
    #         n_elements += len(list)
    #
    #     return n_elements