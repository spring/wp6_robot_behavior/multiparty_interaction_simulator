from Box2D import b2Vec2
import numpy as np


# TODO: identify which of these functions can be removed, as they are not used


def update_turn(keys, desired_ang_speed, max_speed):
    if keys:
        if 'left' in keys:
            desired_ang_speed = max_speed
        elif 'right' in keys:
            desired_ang_speed = -max_speed
        else:
            return 0.
    return desired_ang_speed


def update_drive(keys, desired_linear_speed, mobile_obj, max_speed):
    # find the current speed in the forward direction
    current_forward_normal = mobile_obj.GetWorldVector((0, 1))
    # current_speed = self.forward_velocity.dot(current_forward_normal)

    if keys:
        if 'up' in keys:
            desired_linear_speed = max_speed
        elif 'down' in keys:
            desired_linear_speed = -max_speed
        else:
            return b2Vec2(0., 0.)
    return current_forward_normal * desired_linear_speed


def wheel_speed2lin_ang_speed(w_r, w_l, wheel_diameter, track_length):
    """
    Convert the right and left wheel speed of a non-holonomic mobile base to
    its linear and angular speed
    """
    linear_speed = np.pi*wheel_diameter/2*(w_r + w_l)
    angular_speed = np.pi*wheel_diameter/track_length*(w_r - w_l)
    return linear_speed, angular_speed


def lin_ang_speed2wheel_speed(linear_speed, angular_speed, wheel_diameter,
                             track_length):
    """
    Convert linear and angular speed of a non-holonomic mobile base to its right
    and left wheel speed
    """
    w_r = (2*linear_speed + angular_speed*track_length)/(2*np.pi*wheel_diameter)
    w_l = (2*linear_speed - angular_speed*track_length)/(2*np.pi*wheel_diameter)
    return w_r, w_l


def check_vel_limits(config, action):
        lin = action[0]
        ang = action[1]
        if lin > 0. and lin > config.max_forward_speed:
            lin = config.max_forward_speed
        if lin < 0. and lin < config.max_backward_speed:
            lin = config.max_backward_speed
        if np.fabs(ang) > config.max_ang_vel:
            if ang > 0.:
                ang = config.max_ang_vel
            else:
                ang = -config.max_ang_vel
        return lin, ang


def box2d_coor_2_social_force_coor(points, max_xy_sf_coor=[10., 10.], max_xy_box2d_coor=[5., 5.]):
    """
    Assume that:
    - Social force coordinates inducles bewteen [0., 0.] and [max_xy_sf_coor]
    - Box2d coordinates inducles bewteen [-max_xy_box2d_coor] and [max_xy_box2d_coor]
    """
    assert(min(max_xy_sf_coor) > 0)
    assert(min(max_xy_box2d_coor) > 0)
    b_x, b_y  = max_xy_sf_coor[0]/2,  max_xy_sf_coor[1]/2
    a_x, a_y = b_x/max_xy_box2d_coor[0], b_y/max_xy_box2d_coor[1]
    return [a_x*points[0]+b_x, a_y*points[1]+b_y]


def social_force_coor_2_box2d_coor(points, max_xy_sf_coor=[10., 10.], max_xy_box2d_coor=[5., 5.]):
    """
    Assume that:
    - Social force coordinates inducles bewteen [0., 0.] and [max_xy_sf_coor]
    - Box2d coordinates inducles bewteen [-max_xy_box2d_coor] and [max_xy_box2d_coor]
    """
    assert(min(max_xy_sf_coor) > 0)
    assert(min(max_xy_box2d_coor) > 0)
    b_x, b_y  = max_xy_sf_coor[0]/2,  max_xy_sf_coor[1]/2
    a_x, a_y = b_x/max_xy_box2d_coor[0], b_y/max_xy_box2d_coor[1]
    return [(points[0]-b_x)/a_x, (points[1]-b_y)/a_y]


def sfm_frame_to_box2d_frame_ang(angle):
    return constraint_angle(-angle - np.pi/2)


def constraint_angle(angle, min_value=-np.pi, max_value=np.pi):

    length = max_value - min_value

    # if angle > max_value:
    #     diff = angle - max_value
    #     new_angle = min_value + (diff % length)
    # elif angle < min_value:
    #     diff = min_value - angle
    #     new_angle = max_value - (diff % length)
    # else:
    #     new_angle = angle
    new_angle = np.where(angle > max_value, min_value + ((angle - max_value) % length), angle)
    new_angle = np.where(angle < min_value, max_value - ((min_value - angle) % length), new_angle)
    return new_angle


def closest_node(node, nodes):
    nodes = np.asarray(nodes)
    deltas = nodes - node
    dist_2 = np.einsum('ij,ij->i', deltas, deltas)
    return np.argmin(dist_2)


def calc_linear_velocity_from_forward_velocity(desired_forward_velocity, box2d_body):
    # find the current speed in the forward direction
    current_forward_normal = box2d_body.GetWorldVector((0, 1))
    return current_forward_normal * desired_forward_velocity