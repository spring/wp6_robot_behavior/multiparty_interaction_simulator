class ReadOnlyCombinedList:
    r"""A read only combination of several list that can be iterated and where items can be accessed, but not added or deleted."""

    def __init__(self, data=None):

        if data is None:
            self.data = []
        elif isinstance(data, list):
            self.data = data
        else:
            raise ValueError('data must be a list of lists!')


    def __iter__(self):
        return self._next_generator()


    def _next_generator(self):
        for sub_list in self.data:
            for elem in sub_list:
                yield elem


    def __getitem__(self, item):

        cur_max_idx = 0
        responsible_list_idx = -1
        while cur_max_idx <= item:
            cur_max_idx += len(self.data)
            responsible_list_idx += 1

        if cur_max_idx < item:
            raise IndexError('Index {} out of range! Max size: {}.'.format(item, len(self)))

        # return item, use indexing from end of list (list[-n])
        return self.data[responsible_list_idx][item - cur_max_idx]


    def __len__(self):

        n_elements = 0
        for list in self.data:
            n_elements += len(list)

        return n_elements