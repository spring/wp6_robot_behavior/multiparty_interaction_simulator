import numpy as np
import copy
import inspect

import mpi_sim
from mpi_sim.utils.attrdict import combine_dicts
# try to import torch, so that its seed can be set by the seed() - function

def dict_equal(dict1, dict2):
    """Checks if two dictionaries are equal. Allows to check numpy arrays as content."""

    if not isinstance(dict1, dict) and not isinstance(dict2, dict):
        return dict1 == dict2

    if isinstance(dict1, dict) and not isinstance(dict2, dict):
        return False

    if not isinstance(dict1, dict) and isinstance(dict2, dict):
        return False

    # compare if set of keys is the same
    if set(dict1.keys()) != set(dict2.keys()):
        return False

    # compare all values
    for key in dict1.keys():

        # use special comparison for numpy arrays
        if isinstance(dict1[key], np.ndarray):
            if not np.array_equal(dict1[key], dict2[key]):
                return False
        else:
            if dict1[key] != dict2[key]:
                return False

    return True


def list_equal(list_a, list_b):
    """
    Checks if the content of two lists are equal. Can also handle numpy arrays as content.

    :param list_a: List 1.
    :param list_b: List 2.
    :return: True if the content of both lists are equal, otherwise False.
    """

    if len(list_a) != len(list_b):
        return False

    for idx in range(len(list_a)):

        if type(list_a[idx]) != type(list_b[idx]):
            return False
        if isinstance(list_a[idx], list):
            if not list_equal(list_a[idx], list_b[idx]):
                return False
        elif isinstance(list_a[idx], tuple):
            if not list_equal(list_a[idx], list_b[idx]):
                return False
        elif isinstance(list_a[idx], np.ndarray):
            if not np.array_equal(list_a[idx], list_b[idx], equal_nan=True):
                return False
        elif list_a[idx] != list_b[idx]:
            return False

    return True



def replace_str_from_dict(string, dictionary, pattern_format='{}'):
    """Replaces string occurances that are given as a dictionary."""
    out_string = string

    for key_name, new_str in dictionary.items():
        out_string = out_string.replace(pattern_format.format(key_name),
                                        '{}'.format(new_str))

    return out_string



def str_to_slices(slices_str):
    """
    Creates a slices for lists and numpy arrays from a string.

    >>> out = str_to_slices('[:,-1,0]')
    >>> print(out) # [slice(None), slice(-1), slice(0)]

    :param slice_str: String that describes the slices.
    :return: List of slices.
    """

    # remove all spaces
    slices_str = slices_str.replace(' ', '')

    if slices_str[0] != '[' or slices_str[-1] != ']':
        raise ValueError('Wrong slice format given. Needs to start witt\'[\' and end with \']\'!')

    # seperate each sub slice
    slice_strings = []
    cur_slice_str = ''
    is_inner_idx_list = False
    for c in slices_str[1:-1]:
        if c == ',' and not is_inner_idx_list:
            slice_strings.append(cur_slice_str)
            cur_slice_str = ''
        elif c == '[':
            is_inner_idx_list = True
            cur_slice_str += c
        elif c == ']':
            is_inner_idx_list = False
            cur_slice_str += c
        else:
            cur_slice_str += c
    slice_strings.append(cur_slice_str)

    slices = []
    for slice_str in slice_strings:

        if slice_str == ':':
            # slice_str: '[:]'
            slices.append(slice(None))
        elif ':' in slice_str:
            # slice_str: '[3:]' or '[3:5]' or '[3:10:2]'
            slice_params = []
            slice_str_params = slice_str.split(':')
            for str_p in slice_str_params:
                if str_p != '':
                    slice_params.append(int(str_p))
                else:
                    slice_params.append(None)

            slices.append(slice(*slice_params))
        else:
            if slice_str[0] == '[' and slice_str[-1] == ']':
                # list with indexes for numpy array indexing, e.g. '[[1,2,4]]'
                idxs = []
                for idx_str in slice_str[1:-1].split(','):
                   if idx_str != '':
                       idxs.append(int(idx_str))
                slices.append(idxs)
            else:
                # slice_str: '[3]'
                slices.append(int(slice_str))

    return slices


def get_dict_variable(base_dict, variable_str):
    """
    Retrieves items from sub-dictionaries in a dictionary using a single string.

    >>> d = {'sub_dict_1': {'sub_dict_2': {'item_1': 0}}}
    >>> print(get_dict_variable(d, 'sub_dict_1.sub_dict_2.item_1'))

    Allows also to index items in lists.

    >>> d = {'sub_dict_1': {'item_list': ['a', 'b', 'c']}}
    >>> print(get_dict_variable(d, 'sub_dict_1.item_list[0]'))
    >>> print(get_dict_variable(d, 'sub_dict_1.item_list[-1]'))

    :param base_dict: Dictionary with sub-dictionaries.
    :param variable_str: Path to item. Uses '.' to split sub dictionary keys and the item key.
    :return: Item value.
    """

    # TODO: Feature - allow lists of lists, e.g. 'sub_var.var[:][1]'

    # get the string describing the first sub element in the given variable string
    variable_subelement_strings = variable_str.split('.')
    cur_subelement_str = variable_subelement_strings[0]

    # check if the sub element string contains slices:
    if '[' in cur_subelement_str:
        slice_start = cur_subelement_str.find('[')

        sub_var_name = cur_subelement_str[:slice_start]
        slices = str_to_slices(cur_subelement_str[slice_start:])

        cur_value = base_dict[sub_var_name]

        if isinstance(cur_value, np.ndarray):
            cur_value = cur_value[tuple(slices)]
        else:
            for slice_obj in slices:
                try:
                    cur_value = cur_value[slice_obj]
                except TypeError:
                    raise IndexError()
    else:
        cur_value = base_dict[cur_subelement_str]

    if len(variable_subelement_strings) > 1:
        # not all sub elements processed yet
        cur_value = get_dict_variable(cur_value, '.'.join(variable_subelement_strings[1:]))

    return cur_value


def call_function_from_config(config, *args, func_attribute_name='func', **argv):
    """Calls a function that is defined as a config dictionary or AttrDict."""

    if isinstance(config, dict) and func_attribute_name in config:

        func_handle = config[func_attribute_name]

        function_arguments = copy.deepcopy(config)
        del function_arguments[func_attribute_name]
        function_arguments = combine_dicts(argv, function_arguments)

        return func_handle(*args, **function_arguments)

    elif callable(config):
        return config(*args, **argv)

    else:
        return config


def create_object_from_config(config, *args, **argv):
    """Creates a class object that is defined as a config dictionary or AttrDict."""
    return call_function_from_config(config, *args, func_attribute_name='cls', **argv)


def str_to_list(str, delimiter=','):
    """
    Converts a string of elements into a list.
    Example: 'ds1.var1, ds2.var2[:,-1]' -->  ['ds1.var1', 'ds2.var2[:,-1]']

    :param str: Input string.
    :param delimiter: Delimiter character used to split the string. (default=',')
    :return List with splitted string elements.
    """

    # do nothing if already a list is given
    if isinstance(str, list):
        return str

    elem_list = []

    # remove outer bracket if one exists
    str = str.strip()
    if str:
        if str[0] == '[' and str[-1] == ']':
            str = str[1:-1]

        bracket_level = 0

        cur_str = ''
        for c in str:
            if c == delimiter and bracket_level <= 0:
                elem_list.append(cur_str.strip())
                cur_str = ''
            elif c == '[':
                bracket_level += 1
                cur_str += c
            elif c == ']':
                bracket_level -= 1
                cur_str += c
            else:
                cur_str += c
        elem_list.append(cur_str.strip())

    return elem_list




def create_from_config(simulation, config):
    """Creates objects, scripts, and processes from the config.

    Args:
        simulation (Simulation): Simulation to which the objects, scripts, and processes are added.
        config (AttrDict): Configuration that can have entries for 'objects', 'scripts', and 'processes'.

    Returns:
         List with all object, scripts, and processes that have been created and added. Each entry is a tuple with the id/name and the object.
    """

    entities = []

    if 'objects' in config:
        for object_descr in config.objects:
            obj = simulation.module_library[object_descr.name](config=object_descr)
            id = simulation.add_object(obj, id=object_descr.get('id'), is_permanent=True)
            entities.append((id, obj))
    if 'scripts' in config:
        for script_descr in config.scripts:
            script = simulation.module_library[script_descr.name](config=script_descr)
            id = simulation.add_script(script, id=script_descr.get('id'), is_permanent=True)
            entities.append((id, script))
    if 'processes' in config:
        for process_descr in config.processes:
            process = simulation.module_library[process_descr.name](config=process_descr)
            name = simulation.add_process(process)
            entities.append((name, process))

    return entities


def find_item_in_dicts(key, *dictionaries):
    """Finds an item in in several dictionaries."""

    val = None

    for d in dictionaries:
        val = d.get(key)

        if val is not None:
            break

    return val


def get_dict_items_by_type(dictionary, type):
    r"""Returns all items in a dictionary that are object of a certain class.

    Args:
        dictionary (dict): Dictionary
        type (class or list of classes): class or tuple of classes.

    Returns: Entities of the given type or types. If none is found, it returns an empty list.
    """
    items = []

    if isinstance(type, list):
        type = tuple(type)

    for p in dictionary.values():
        if isinstance(p, type):
            items.append(p)

    return items


def as_list(x):
    if not isinstance(x, list):
        return [x]
    else:
        return x


def object_filter(obj, filter=None, **kwargs):

    default_filter = mpi_sim.AttrDict(
        objects = [],
        ids = [],
        types = [],
        properties = [],
    )
    filter = mpi_sim.combine_dicts(kwargs, filter, default_filter)

    # if no filters are given, then object is ok
    if not filter.objects and not filter.ids and not filter.types and not filter.properties:
        return True

    # make sure, that all inputs are lists

    if filter.objects is None:
        filter.objects = []
    elif isinstance(filter.objects, mpi_sim.Object):
        filter.objects = [filter.objects]
    elif not isinstance(filter.objects, list):
        raise ValueError('Argument objects must be either None, Object, or a list of Objects!')

    if filter.ids is None:
        filter.ids = []
    elif isinstance(filter.ids, int):
        filter.ids = [filter.ids]
    elif not isinstance(filter.ids, list):
        raise ValueError('Argument ids must be either None, int, or a list of ints!')

    if filter.types is None:
        filter.types = []
    elif inspect.isclass(filter.types):
        filter.types = [filter.types]
    elif not isinstance(filter.types, list):
        raise ValueError('Argument types must be either None, class, or a list of classes!')

    if filter.properties is None:
        filter.properties = []
    elif isinstance(filter.properties, str):
        filter.properties = [filter.properties]
    elif not isinstance(filter.properties, list):
        raise ValueError('Argument properties must be either None, string, or a list of strings!')

    # check if condition holds

    if filter.objects and obj in filter.objects:
        return True

    if filter.ids and obj.id in filter.ids:
        return True

    if filter.types and isinstance(obj, tuple(filter.types)):
        return True

    if filter.properties:
        for prop in filter.properties:
            if prop in obj.properties:
                return True

    # could not filter the object
    return False
