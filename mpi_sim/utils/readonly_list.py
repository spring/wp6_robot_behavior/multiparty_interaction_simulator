class ReadOnlyList:
    r"""A read only list that can be iterated and where items can be accessed, but not added or deleted."""

    def __init__(self, data=None):

        if data is None:
            self.data = []
        elif isinstance(data, list):
            self.data = data


    def __iter__(self):
        return self.data.__iter__()


    def __getitem__(self, item):
        return self.data[item]


    def __len__(self):
        return len(self.data)
