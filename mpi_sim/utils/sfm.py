#!/usr/bin/env python
# -*- coding: utf-8 -*-


## Software gym_box2d_diff_drive
##
## This file, corresponding to gym_box2d_diff_drive module of WP6, is part of a
## project that has received funding from the European Union’s Horizon 2020
## research and innovation programme under grant agreement No 871245.
##
## Copyright (C) 2021 by INRIA
## Authors : Alex Auternaud, Chris Reinke
## alex dot auternaud at inria dot fr
## chris dot reinke at inria dot fr


import numpy as np
# import gym.envs.classic_control.rendering as rendering
import time

class World:

    def __init__(self, **config):
        self.size = np.array(config.get('size', [10.0, 10.0])) # [width (x-direction), height (y-direction)]
        self.agents = []
        self.social_groups = []


    def add_agent(self, agent):
        self.agents.append(agent)
        agent.world = self


    def remove_agent(self, agent_idx):
        self.agents.pop(agent_idx)


    def add_social_group(self, social_group):
        self.social_groups.append(social_group)
        social_group.world = self


    def get_agents(self, center, radius):

        center = np.array(center)

        agents = []
        for agent in self.agents:
            if np.linalg.norm(agent.pos - center) <= radius:
                agents.append(agent)

        return agents


class SocialGroup:

    def __init__(self, **config):

        self.pos = np.array(config.get('pos', [0.0, 0.0]))
        self.o_space_radius = config.get('o_space_radius', 1.0)
        self.world = None
        self.agents = []


    def add_agent(self, agent):
        self.agents.append(agent)
        agent.social_group = self

    def remove_agent(self, agent_idx):
        self.agents.pop(agent_idx)

    def generate_new_pos(self):
        world_size = self.world.size.reshape((2,))
        self.pos = np.random.random_sample((2,))*world_size


class Agent:

    def __init__(self, **config):
        self.personal_distance = config.get('personal_distance', 1.0)  # delta_p, minimum distance to other agents
        self.public_distance = config.get('public_distance', 10.0) # public space around the agent

        self.pos = np.array(config.get('pos', [0.0, 0.0])) # x and y coordinate
        self.orientation = config.get('orientation', 0.0)
        self.is_moving = config.get('is_moving', True)

        self.max_movement_force = config.get('max_movement_force', 5.0)
        self.max_rotation_force = config.get('max_rotation_force', 5.0)

        self.world = None
        self.social_group = None # social group the agent is part

        self.f_repulsion = np.array([0.0, 0.0])
        self.f_equality = np.array([0.0, 0.0])
        self.f_cohesion = np.array([0.0, 0.0])
        self.f_position = np.array([0.0, 0.0])
        self.f_position_clipped = np.array([0.0, 0.0])

        self.d_equality = np.array([0.0, 0.0])
        self.d_cohesion = np.array([0.0, 0.0])
        self.d_orientation = np.array([0.0, 0.0])
        self.f_orientation = 0.0
        self.f_orientation_clipped = 0.0


    def calc_step(self):
        return self.calc_force_at_position(self.pos)


    def calc_force_at_position(self, position):

        # calculate all forces of the agent
        self.f_repulsion = self.calc_repulsion_force(position)
        [self.f_equality, self.d_equality] = self.calc_equality_force(position)
        [self.f_cohesion, self.d_cohesion] = self.calc_cohesion_force(position)

        # vector the agent wants to walk
        self.f_position = self.f_repulsion + self.f_equality + self.f_cohesion

        # allow only a max force
        f_length = np.linalg.norm(self.f_position)
        if f_length <= self.max_movement_force:
            self.f_position_clipped = self.f_position
        else:
            self.f_position_clipped = self.f_position * self.max_movement_force/f_length

        # vector that defines the direction in which the agent wants to look
        self.d_orientation = self.d_equality + self.d_cohesion
        # self.d_orientation = self.d_cohesion

        # calculate the angle of the direction vector
        self.f_orientation = self.calc_angle_of_agent_to_vector(position + self.d_orientation)
        self.f_orientation_clipped = np.clip(self.f_orientation, -self.max_rotation_force, self.max_rotation_force)
        #print("FORCE AT POSITION:", self.f_position_clipped, self.f_orientation_clipped)
        return self.f_position_clipped, self.f_orientation_clipped


    def calc_repulsion_force(self, position):

        f_repulsion = position * 0.0

        # identify other agents inside the personal distance of the agent
        agents = self.world.get_agents(position, self.personal_distance)
        if self in agents:
            agents.remove(self)

        if agents:
            other_agents_pos = np.array([agent.pos for agent in agents])   # [n_agent * 2 (x and y pos)] array
            vec_from_other_agents = other_agents_pos - position

            # identify smallest distance d_min to another agent
            distance_to_other_agents = np.linalg.norm(vec_from_other_agents, axis=1)
            d_min = np.min(distance_to_other_agents)

            # compute repulsion force
            R = np.sum(vec_from_other_agents, axis=0)
            f_repulsion = -(self.personal_distance - d_min)**2 * R / np.linalg.norm(R)
        #print("FORCE AT REPULSIVE:", f_repulsion)
        return f_repulsion


    def calc_equality_force(self, position):

        f_equality = position * 0.0
        d_equality = position * 0.0

        if self.social_group is not None and len(self.social_group.agents) > 1:

            # get position of all other agents that are not the current agent
            other_agents_pos = []
            for agent in self.social_group.agents:
                if agent != self:
                    other_agents_pos.append(agent.pos)
            other_agents_pos = np.array(other_agents_pos)

            # centroid of group (includes the current agent)
            c = 1 / len(self.social_group.agents) * (position + np.sum(other_agents_pos, axis=0))

            # mean distance of all members
            m = 1 / len(self.social_group.agents) * (np.linalg.norm(position - c) + np.sum(np.linalg.norm(other_agents_pos - c, axis=1)))

            f_equality = (1.0 - m/(np.linalg.norm(c - position))) * (c - position)

            d_equality = np.sum(other_agents_pos - position, axis=0)
        # print("FORCE AT EQUALITY:", f_equality, d_equality)
        return f_equality, d_equality


    def calc_cohesion_force(self, position):

        f_cohesion = position * 0.0
        d_cohesion = position * 1.0

        if self.social_group is not None:

            # agents_in_public_space = self.world.get_agents(position, self.public_distance)
            other_agents_pos = []
            for agent in self.social_group.agents:                
                other_agents_pos.append(agent.pos)
            other_agents_pos = np.array(other_agents_pos)
            
            # if self in agents_in_public_space:
            #     agents_in_public_space.remove(self) # remove agent itself

            # alpha = len(agents_in_public_space) / len(self.social_group.agents)
            alpha = 1  # alpha = len(other_agents_pos) / len(self.social_group.agents)
            f_cohesion = alpha * (1 - self.social_group.o_space_radius / np.linalg.norm(self.social_group.pos - position)) * (self.social_group.pos - position)

            # if agents_in_public_space:
            #     agents_in_public_space_pos = []
            #     for agent in agents_in_public_space:
            #         agents_in_public_space_pos.append(agent.pos)
            #     agents_in_public_space_pos = np.array(agents_in_public_space_pos)

            

            d_cohesion = np.sum(-other_agents_pos + self.social_group.pos, axis=0)
        # print("FORCE AT COHESION:", f_cohesion, d_cohesion)
        return f_cohesion, d_cohesion


    def calc_angle_of_agent_to_vector(self, vec):
        # calculates the angle in the coordinate frame of ARI.
        # in front: 0, clockwise: positive rad, counter-clockwise: negative rad

        x = vec[0] - self.pos[0]
        y = vec[1] - self.pos[1]

        if x == 0.0 and y == 0.0:
            angle = 0.0
        else:
            angle = -np.arctan2(y, x) - self.orientation

        # kepp angle between -pi and pi
        if angle > np.pi:
            angle = -np.pi + (angle - np.pi)
        elif angle < -np.pi:
            angle = np.pi + (angle + np.pi)

        return angle


class WorldSimulation(World):

    def __init__(self, **config):
        super().__init__(**config['world'])
        self.step_length = config['world'].get('step_length', 0.1)

        self.viewer_is_show_personal_space = config['world'].get('viewer_is_show_personal_space', True)
        self.viewer_is_show_public_space = config['world'].get('viewer_is_show_public_space', False)
        self.viewer_is_show_social_group = config['world'].get('viewer_is_show_social_group', True)

        self.viewer_is_show_f_repulsion = config['viewer'].get('is_show_f_repulsion', False)
        self.viewer_is_show_f_equality = config['viewer'].get('is_show_f_equality', False)
        self.viewer_is_show_f_cohesion = config['viewer'].get('is_show_f_cohesion', False)
        self.viewer_is_show_f_position = config['viewer'].get('is_show_f_position', False)
        self.viewer_is_show_f_position_clipped = config['viewer'].get('is_show_f_position_clipped', False)
        self.viewer_is_show_d_equality = config['viewer'].get('is_show_d_equality', False)
        self.viewer_is_show_d_cohesion = config['viewer'].get('is_show_d_cohesion', False)
        self.viewer_is_show_d_orientation = config['viewer'].get('is_show_d_orientation', False)

        self.viewer_position_force_visu_agent = config['viewer'].get('position_force_visu_agent', None)
        self.viewer_position_force_visu_grid = config['viewer'].get('position_force_visu_grid', [50, 50])

        # create agents and social groups according to the configuration
        if 'agents' in config:
            for agent_config in config['agents']:
                agent = Agent(**agent_config)
                self.add_agent(agent)

        if 'social_groups' in config:
            for group_config in config['social_groups']:
                group = SocialGroup(**group_config)
                self.add_social_group(group)

                if 'agents' in group_config:
                    for agent_idx in group_config['agents']:
                        group.add_agent(self.agents[agent_idx])

        self.viewer = None
        self.agent_position_force_grid = np.full((self.viewer_position_force_visu_grid[0],
                                                  self.viewer_position_force_visu_grid[1],
                                                  2),
                                                 np.nan)

    def run(self, refresh_rate_msec=100, n_steps=1000):

        self.render()

        for step in range(n_steps):
            self.do_step()

            if self.viewer_position_force_visu_agent is not None:
                # calculate for the given agent the forces
                self.calc_agent_position_force_grid()

            self.render()
            time.sleep(refresh_rate_msec/1000)


    def calc_agent_position_force_grid(self):

        x_positions = np.linspace(0.0, self.size[0], self.viewer_position_force_visu_grid[0])
        y_positions = np.linspace(0.0, self.size[1], self.viewer_position_force_visu_grid[1])

        agent = self.agents[self.viewer_position_force_visu_agent]

        for x_idx, x_pos in enumerate(x_positions):
            for y_idx, y_pos in enumerate(y_positions):
                f_position, _ = agent.calc_force_at_position(np.array([x_pos, y_pos]))
                self.agent_position_force_grid[x_idx, y_idx] = f_position


    def do_step(self):

        # calculate the forces for each agent that is moving
        for agent in self.agents:
            if agent.is_moving:
                # get forces on position and orientation of agent
                [f_movement, f_orientation] = agent.calc_step()

                # update postion but stay within the borders of the world
                agent.pos = agent.pos +  f_movement * self.step_length
                agent.pos[0] = np.clip(agent.pos[0], 0.0, self.size[0])
                agent.pos[1] = np.clip(agent.pos[1], 0.0, self.size[1])

                # update orientation, but stay within [-pi, pi]
                agent.orientation = agent.orientation +  f_orientation * self.step_length
                agent.orientation = self._constraint_angle(agent.orientation)            


    def _constraint_angle(self, angle, min_value=-np.pi, max_value=np.pi):

        length = max_value - min_value

        if angle > max_value:
            diff = angle - max_value
            new_angle = min_value + (diff % length)
        elif angle < min_value:
            diff = min_value - angle
            new_angle = max_value - (diff % length)
        else:
            new_angle = angle

        return new_angle


    # def render(self, mode='human'):
    #     screen_width = 1200
    #     screen_height = 1200

    #     agent_color = [.5, .5, .8]
    #     personal_space_color = [.8, .8, .8]
    #     public_space_color = [.8, .8, .8]

    #     social_group_color = [.8, .2, .2]

    #     scale = screen_width/self.size[0]

    #     agent_width = 0.5
    #     social_group_width = 0.2

    #     force_coeff = 10.0
    #     force_line_width = 3
    #     force_colors = {'f_repulsion': [.3, .3, .7],
    #                     'f_equality': [.7, .3, .3],
    #                     'f_cohesion': [.3, .7, .3],
    #                     'f_position': [.7, .3, .7],
    #                     'f_position_clipped': [.7, .3, .7],
    #                     'd_equality': [.7, .3, .7],
    #                     'd_cohesion': [.3, .7, .7],
    #                     'd_orientation': [.7, .7, .3]}

    #     def draw_position_force(agent, force_name):
    #         '''Draws a position force with the given name.'''
    #         if self.__getattribute__('viewer_is_show_' + force_name):
    #             agent_pos = agent.pos * scale
    #             f = agent.__getattribute__(force_name)
    #             force_pos = agent_pos + f * force_coeff
    #             f_equality_line = self.viewer.draw_polyline([agent_pos, force_pos])
    #             f_equality_line.set_color(*force_colors[force_name])
    #             f_equality_line.set_linewidth(force_line_width)

    #     if self.viewer is None:            
    #         self.viewer = rendering.Viewer(screen_width, screen_height)

    #         self.rendered_social_groups_transformers = []
    #         if self.viewer_is_show_social_group:

    #             for social_group in self.social_groups:
    #                 group_transform = rendering.Transform()

    #                 social_group_center = rendering.make_circle(social_group_width / 2 * scale)
    #                 social_group_center.set_color(*social_group_color)
    #                 social_group_center.add_attr(group_transform)
    #                 self.viewer.add_geom(social_group_center)

    #                 social_group_ospace = rendering.make_circle(social_group.o_space_radius * scale, filled = False)
    #                 social_group_ospace.set_color(*social_group_color)
    #                 social_group_ospace.add_attr(group_transform)
    #                 self.viewer.add_geom(social_group_ospace)

    #                 self.rendered_social_groups_transformers.append(group_transform)

    #         self.rendered_agents_transforms = []
    #         for agent in self.agents:

    #             agent_transform = rendering.Transform()

    #             agent_body = rendering.make_circle(agent_width / 2 * scale)
    #             agent_body.set_color(*agent_color)
    #             agent_body.add_attr(agent_transform)
    #             self.viewer.add_geom(agent_body)

    #             if self.viewer_is_show_personal_space:
    #                 personal_space = rendering.make_circle(agent.personal_distance * scale, filled=False)
    #                 personal_space.set_color(*personal_space_color)
    #                 personal_space.add_attr(agent_transform)
    #                 self.viewer.add_geom(personal_space)

    #             if self.viewer_is_show_public_space:
    #                 public_space = rendering.make_circle(agent.public_distance * scale, res=100, filled=False)
    #                 public_space.set_color(*public_space_color)
    #                 public_space.add_attr(agent_transform)
    #                 self.viewer.add_geom(public_space)

    #             # orientation of agent
    #             agent_orientation = rendering.make_polyline([[0,0], [agent_width/2 * scale, 0]])
    #             agent_orientation.set_linewidth(3)
    #             agent_orientation.add_attr(agent_transform)
    #             self.viewer.add_geom(agent_orientation)

    #             self.rendered_agents_transforms.append(agent_transform)

    #     if self.viewer_position_force_visu_agent is not None:
    #         x_positions = np.linspace(0.0, self.size[0], self.viewer_position_force_visu_grid[0])
    #         y_positions = np.linspace(0.0, self.size[1], self.viewer_position_force_visu_grid[1])

    #         for x_idx, x_pos in enumerate(x_positions):
    #             for y_idx, y_pos in enumerate(y_positions):
    #                 start_pos = np.array([x_pos, y_pos]) * scale
    #                 force_pos = start_pos + self.agent_position_force_grid[x_idx, y_idx] * force_coeff
    #                 f_line = self.viewer.draw_polyline([start_pos, force_pos])
    #                 f_line.set_linewidth(force_line_width)


    #     for agent_idx, agent in enumerate(self.agents):
    #         agent_pos = agent.pos * scale

    #         self.rendered_agents_transforms[agent_idx].set_translation(agent_pos[0], agent_pos[1])
    #         self.rendered_agents_transforms[agent_idx].set_rotation(-1.0 * agent.orientation) # mutiplication by -1.0 to have clockwise positive rad values

    #         #Forces
    #         draw_position_force(agent, 'f_repulsion')
    #         draw_position_force(agent, 'f_equality')
    #         draw_position_force(agent, 'f_cohesion')
    #         draw_position_force(agent, 'f_position')
    #         draw_position_force(agent, 'f_position_clipped')
    #         draw_position_force(agent, 'd_equality')
    #         draw_position_force(agent, 'd_cohesion')
    #         draw_position_force(agent, 'd_orientation')

    #     if self.viewer_is_show_social_group:
    #         for social_group_idx, social_group in enumerate(self.social_groups):
    #             self.rendered_social_groups_transformers[social_group_idx].set_translation(social_group.pos[0] * scale, social_group.pos[1] * scale)

    #     return self.viewer.render(return_rgb_array=mode == 'rgb_array')


if __name__ == '__main__':

    # World config
    config = {}
    config['world'] = {'size': np.array([10.0, 10.0])}

    # Agent config
    config['agents'] = [ {'pos': np.array([17.5, 7.5]), 'orientation': np.pi/4, 'is_moving': True},
                         {'pos': np.array([6.0, 6.0]), 'orientation': 0.0, 'is_moving': True},
                         {'pos': np.array([7.0, 7.0]), 'orientation': np.pi/2, 'is_moving': True},
                         {'pos': np.array([8.0, 6.0]), 'orientation': np.pi, 'is_moving': True} ]

    # social groups config
    config['social_groups'] = [{'pos': np.array([5.0, 5.0]), 'agents': np.array([0, 1, 2, 3])}]

    config['viewer'] = {'position_force_visu_agent': None}

    world_sim = WorldSimulation(**config)
    world_sim.run(refresh_rate_msec=500)
