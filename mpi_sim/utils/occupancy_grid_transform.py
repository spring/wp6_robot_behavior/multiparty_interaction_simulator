import mpi_sim
import numpy as np
import cv2

class MapTransform():
    @staticmethod
    def default_config():
        dc = mpi_sim.AttrDict()
        return dc

    def __init__(self, config=None, **kwargs):
        super(MapTransform, self).__init__(config=config, **kwargs)


    def __call__(self, map, simulation, bodies, default_occupancy_value, occupancy_value_depth):
        raise Warning('Not Implemented')


class RTABMAPlikeTransform(MapTransform):

    @staticmethod
    def default_config():
        dc = mpi_sim.AttrDict()
        return dc


    def __init__(self, config=None, **kwargs):
        self.config = mpi_sim.combine_dicts(kwargs, config, self.default_config())
    

    def __call__(self, map, simulation, bodies, default_occupancy_value, occupancy_value_depth):
        """Transforms the map to make it looking like a 2D inflated RTAB-Map."""

        new_map = np.copy(map.map)
        # Inflate the original map
        k = 3 #np.random.randint(1,3)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (k, k))
        inflate_map = cv2.dilate(new_map, kernel, iterations=1)
        # Complete the noisy_map when there is a 1 on the inflate map with a given probability of 4%
        sampled_map = np.zeros(np.shape(inflate_map))
        for x in range(len(inflate_map[0])):
            for y in range(len(inflate_map[1])):
                if inflate_map[x][y] == 1 and np.random.random() > 0.98:
                    sampled_map[x][y] = 1
        
        # Now dilate the sparse 1 on the new noisy map with a circular kernel
        k = 20 #np.random.randint(15,20)
        kernel_noisy_map = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (k,k))
        k = 10 #np.random.randint(5,10)
        final_noisy_map = np.clip(
                                cv2.dilate(sampled_map, kernel_noisy_map, iterations=1) + cv2.dilate(new_map, kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,  (k, k)), iterations=1), 
                            0, default_occupancy_value
        )
        new_occupancy_grid = np.where(final_noisy_map > 0.1, default_occupancy_value, np.min(map.map))

        return mpi_sim.utils.OccupancyGridMap(map=new_occupancy_grid, area=map.area, resolution=map.resolution)