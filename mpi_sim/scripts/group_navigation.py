import mpi_sim as sim
from collections import OrderedDict
from mpi_sim.core.script import Script
from mpi_sim.utils.misc import find_item_in_dicts
from mpi_sim.utils.sfm import WorldSimulation, Agent
from mpi_sim.utils.box2d_utils import box2d_coor_2_social_force_coor, social_force_coor_2_box2d_coor, sfm_frame_to_box2d_frame_ang
import numpy as np


class GroupNavigation(Script):
    
    @staticmethod
    def default_config():
        dc = Script.default_config()

        dc.position = [0.0, 0.0]
        dc.o_space_radius = 0.6  # social group parameter        
        dc.sigma = 0.02  # group center noise
        dc.person_merge_distance = 2.0  # threshold to integrate robot/human in the group

        return dc
    

    @property
    def position(self):
        if self.is_removed:
            return None
        else:
            return np.array(self._position)

    
    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.agents = OrderedDict()
        self._position = self.config.position
        self.social_force_group_sim = None

    
    def _reset(self):
        pass


    def _step(self):
        noise = np.random.normal(0, self.config.sigma, (2,))
        self.social_force_group_sim.social_groups[0].pos = box2d_coor_2_social_force_coor(self._position + noise)
        self.social_force_group_sim.do_step()
        for agent_sfm, (agents_box2d_id, agents_box2d_h) in zip(self.social_force_group_sim.social_groups[0].agents, self.agents.items()):                        
            if agent_sfm.is_moving and isinstance(agents_box2d_h, sim.mpi_sim.objects.Human):
                pos = social_force_coor_2_box2d_coor(points=agent_sfm.pos)
                orientation = sfm_frame_to_box2d_frame_ang(agent_sfm.orientation)
                agents_box2d_h.box2d_body.transform = ((float(pos[0]), float(pos[1])), float(orientation))


    def set_group_center(self, position):
        if not self.agents:
            return
        self._position = position
        self.social_force_group_sim = self.create_social_force_group_sim()

    
    def add_agent(self, agents):
        if isinstance(agents, (list, tuple)):            
            for agent in agents:
                self._add_object(agent)               
        else:
            self._add_object(agents)         
    

    def remove_agent(self, agents):
        if isinstance(agents, (list, tuple)):
            for agent in agents:
                self._remove_object(agent)               
        else:
            self._remove_object(agents)
    

    def _add_object(self, object):
        if not isinstance(object, (sim.mpi_sim.objects.Human, sim.mpi_sim.objects.ARIRobot)):
            raise ValueError('The object must be of type mpi_sim.Human or !')
        if self.exist_id(object.id):
            raise IndexError('An object with ID {} exists already!'.format(object.id))
        self.agents[object.id] = object
        if self.social_force_group_sim is not None:
            pos = box2d_coor_2_social_force_coor(points=object.position)
            orientation = np.radians(object.orientation)
            agent_config = {
                'pos': pos,
                'orientation': orientation,
                'is_moving': True,
                'personal_distance': object.config.personal_distance,
                'public_distance': object.config.public_distance,
                'max_movement_force': object.config.max_movement_force,
                'max_rotation_force':object.config.max_rotation_force
                }
            self.social_force_group_sim.add_agent(Agent(**agent_config))
            self.social_force_group_sim.social_groups[0].add_agent(self.social_force_group_sim.agents[-1])

    
    def _remove_object(self, object):
        if not isinstance(object, (sim.mpi_sim.objects.Human, sim.mpi_sim.objects.ARIRobot)):
            raise ValueError('The object must be of type mpi_sim.Human!')
        if not self.exist_id(object.id):
            raise IndexError('An object with ID {}  does not exist!'.format(object.id))
        if self.social_force_group_sim is not None:
            remove_id = list(self.agents.keys()).index(object.id)
            self.social_force_group_sim.remove_agent(remove_id)
            self.social_force_group_sim.social_groups[0].remove_agent(remove_id)
        self.agents.pop(object.id)
    

    def exist_id(self, id):
        """Checks if an ID exists already for an agent.

        Args:
            id (int): ID

        Returns:
            True if it exists, otherwise False.
        """

        val = find_item_in_dicts(id, self.agents)
        return val is not None
    

    def create_social_force_group_sim(self):
        config = {}
        size = self.simulation.visible_area
        config['world'] = {'size': [size[0][1] - size[0][0], size[1][1] - size[1][0]]}
        # Agent config
        config['agents'] = []
        for agent_id, agent in self.agents.items():
            pos = box2d_coor_2_social_force_coor(points=agent.position)
            orientation = np.radians(agent.orientation)
            config['agents'].append({
                'pos': pos,
                'orientation': orientation,
                'is_moving': True,
                'personal_distance': agent.config.personal_distance,
                'public_distance': agent.config.public_distance,
                'max_movement_force': agent.config.max_movement_force,
                'max_rotation_force':agent.config.max_rotation_force
                })
        # social groups config
        group_center_point = (
            box2d_coor_2_social_force_coor(
                self._position,
                max_xy_sf_coor=[10., 10.],
                max_xy_box2d_coor=[5., 5.])
        )
        config['social_groups'] = [{
            'pos': group_center_point,
            'agents': range(len(self.agents)),
            'o_space_radius': self.config.o_space_radius
        }]
        config['viewer'] = {}
        return WorldSimulation(**config)