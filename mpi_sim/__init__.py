import mpi_sim.core
import mpi_sim.objects
import mpi_sim.components
import mpi_sim.processes
import mpi_sim.scripts
import mpi_sim.utils

from mpi_sim.core.simulation import Simulation
from mpi_sim.core.object import Object
from mpi_sim.core.component import Component
from mpi_sim.core.sensor_component import SensorComponent
from mpi_sim.core.generator_component import GeneratorComponent
from mpi_sim.core.script import Script
from mpi_sim.core.process import Process

from mpi_sim.utils.attrdict import AttrDict
from mpi_sim.utils.attrdict import AutoAttrDict
from mpi_sim.utils.attrdict import DefaultAttrDict
from mpi_sim.utils.attrdict import DefaultFactoryAttrDict
from mpi_sim.utils.attrdict import combine_dicts

# import the module library last, so that it can find all other modules in the mpi_sim to load them
import mpi_sim.core.module_library

__version__ = '0.0.9'
