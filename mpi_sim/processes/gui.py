import mpi_sim
from mpi_sim.utils.attrdict import AttrDict
from mpi_sim.core.process import Process
import os
import pygame
from pygame import Color
from pygame.locals import QUIT, KEYDOWN, KEYUP, MOUSEBUTTONDOWN, MOUSEBUTTONUP, MOUSEMOTION, KMOD_LSHIFT
from Box2D import b2Color, b2Vec2, b2DrawExtended, b2PolygonShape, b2_persistState, b2_nullState, b2_addState, b2CircleShape, b2AABB, b2QueryCallback
from Box2D.examples.pgu import gui

# TODO (Feature): allow the gui to pause the simulation by adding a pause / unpause function to simulation (needs to take into account to not pause the gui)
# TODO (Feature): allow to control ari or humans, by clicking on them

# color schemes, (R, G, B [, alpha])
COLOR_SCHEME_LIGHT = AttrDict(
    background = Color(255, 255, 255),
    text = Color(0, 0, 0),
    border = Color(0, 0, 0),
    object = Color(100, 100, 100),
    ARIRobot = Color(200, 80, 80),
    Human = Color(20, 100, 100),
    contact_add = Color(90, 250, 90),
    contact_persist = Color(90, 90, 250),
    contact_normal = Color(230, 230, 120),
)

COLOR_SCHEME_DARK = AttrDict(
    background=Color(0, 0, 0),
    text=Color(255, 255, 255),
    border=Color(255, 255, 255),
    object=Color(150, 150, 150),
    ARIRobot=Color(200, 80, 80),
    Human=Color(20, 100, 100),
    contact_add=Color(90, 250, 90),
    contact_persist=Color(90, 90, 250),
    contact_normal=Color(230, 230, 120),
)


class GUI(Process):
    r"""GUI"""

    @staticmethod
    def default_config():
        dc = Process.default_config()
        dc.name = 'gui'

        dc.caption = 'Multiparty Interaction Simulation (v{})'.format(mpi_sim.__version__)

        dc.color_scheme = COLOR_SCHEME_LIGHT  # 'light', 'night'

        dc.screen_width = 1200
        dc.screen_height = 900
        dc.viewZoom = 70.0
        dc.viewCenter = None
        dc.move_world = True  # TODO: update move_world feature
        # dc.key_control = True   # maybe used later for key control
        dc.print_collision = True
        # wait_init_phase: False
        # sleep_init_time: 1.

        # Drawing
        dc.draw_agent_information = True  # should the agent type and its id be shown?
        dc.draw_object_information = False  # should the agent type and its id be shown?
        dc.draw_dialogue = True  # should the agent type and its id be shown?
        dc.draw_debug_information = False
        dc.draw_stats = False
        dc.draw_rtf = False
        dc.draw_menu = False             # toggle by pressing F1

        dc.point_size = 2.5             # pixel radius for drawing points
        dc.text_line_start = 30

        dc.agent_information = mpi_sim.AttrDict(
            x_offset = 25,  # y offset to the object
            y_offset = -10,  # y offset to the object
            x_padding = 10,  # y offset to the object
            y_padding = 5,  # y offset to the object
            border_color = None,
            background_color = None,
            filter = mpi_sim.AttrDict(types=[mpi_sim.objects.Human, mpi_sim.objects.ARIRobot]),  # what are agents?
        )

        dc.object_information = mpi_sim.AttrDict(
            x_offset = 25,  # y offset to the object
            y_offset = -10,  # y offset to the object
            x_padding = 10,  # y offset to the object
            y_padding = 5,  # y offset to the object
            border_color = None,
            background_color = None,
        )

        dc.popup_information = mpi_sim.AttrDict(
            x_offset = 25,  # y offset to the object
            y_offset = -10,  # y offset to the object
            x_padding = 10,  # y offset to the object
            y_padding = 5,  # y offset to the object
            border_color = 'border',
            background_color = 'background',
        )

        dc.menu = mpi_sim.AttrDict(
            checkboxes = [
                #('Warm Starting', 'enable_warm_starting'),
                #('Time of Impact', 'enable_continuous'),
                #('Sub-Stepping', 'enable_sub_stepping'),
                ('Draw', None),
                # ('Agent Info', 'draw_agent_information'),
                # ('Object Info', 'draw_object_information'),
                ('Dialogue', 'draw_dialogue'),
                # ('Debug Mode', 'draw_debug_information'),
                # ('Statistics', 'draw_stats'),
                ('RTF', 'draw_rtf'),
                #('Control', None),
                #('Pause', 'pause'),
                #('Single Step', 'singleStep')
            ],

            sliders = [
                #{'name': 'hz', 'text': 'Hertz', 'min': 5, 'max': 200},
            ]
        )

        return dc


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        if isinstance(self.config.color_scheme, str):
            if self.config.color_scheme == 'light':
                self.config.color_scheme = COLOR_SCHEME_LIGHT
            elif self.config.color_scheme == 'dark':
                self.config.color_scheme = COLOR_SCHEME_DARK
            else:
                raise ValueError('Unknown color scheme \'{}\'! Available: \'light\', \'dark\'.')

        self._mouse_world = None
        self._mouse_over_object = None  # object over which the mouse is currently
        self.human_speech_history = []
        self.human_speech_history_length = 12
        # Screen/rendering-related
        self._viewZoom = self.config.viewZoom
        self._viewCenter = None
        self._viewOffset = None
        self.screenSize = None
        self.r_mouse_down = False

        self._text_line = self.config.text_line_start

        self.offsetLine = 5
        self.offsetYRect = 10
        self.offsetXRect = 25

        self.font = None
        self.fps = 0
        self.time_infos_displayed = 4.

        # GUI-related (PGU)
        self._gui_app = None
        self._gui_table = None

        # setup keys
        keys = [s for s in dir(pygame.locals) if s.startswith('K_')]
        for key in keys:
            value = getattr(pygame.locals, key)
            setattr(Keys, key, value)

        os.environ['SDL_AUDIODRIVER'] = 'dsp'

        # Pygame Initialization
        pygame.init()
        pygame.display.set_caption(self.config.caption)

        # Screen and debug draw
        flags = pygame.DOUBLEBUF
        self.screen = pygame.display.set_mode((self.config.screen_width, self.config.screen_height), flags)
        self.screenSize = b2Vec2(*self.screen.get_size())

        self.renderer = PygameDraw(surface=self.screen, gui=self)


        self.font = pygame.font.Font(None, 15)

        # GUI Initialization
        self._gui_app = gui.App()
        self._gui_table = MenuTable(config=self.config.menu, settings=self.config)
        container = gui.Container(align=1, valign=-1)
        container.add(self._gui_table, 0, 0)
        self._gui_app.init(container)

        self.viewCenter = self.config.viewCenter

        self.key_map = {
            Keys.K_w: 'up',
            Keys.K_s: 'down',
            Keys.K_a: 'left',
            Keys.K_d: 'right',
            Keys.K_n: 'pan_left',
            Keys.K_m: 'pan_right',
            Keys.K_b: 'switch_raycast_mode'  # use this to toggle ray casts
        }


    def setCenter(self, value):
        """
        Updates the view offset based on the center of the screen.
        Tells the debug draw to update its values also.
        """
        if value is None:
            value = [0, 0]

        self._viewCenter = b2Vec2(*value)
        self._viewCenter *= self._viewZoom
        self._viewOffset = self._viewCenter - self.screenSize / 2


    def setZoom(self, zoom):
        self._viewZoom = zoom

    # TODO: write these properties out and rename them according to the python standard
    viewZoom = property(lambda self: self._viewZoom, setZoom, doc='Zoom factor for the display')
    viewCenter = property(lambda self: self._viewCenter / self._viewZoom, setCenter, doc='Screen center in camera coordinates')
    viewOffset = property(lambda self: self._viewOffset, doc='The offset of the top-left corner of the screen')


    def _add(self):
        self.simulation.box2d_world.renderer = self.renderer

        if self.config.viewCenter is None:
            x = self.simulation.visible_area[0][0] + (self.simulation.visible_area[0][1] - self.simulation.visible_area[0][0]) / 2
            y = self.simulation.visible_area[1][0] + (self.simulation.visible_area[1][1] - self.simulation.visible_area[1][0]) / 2
            self.viewCenter = [x, y]

        

    def get_color_for_body(self, body):

        obj = body.userData['object']
        obj_class_name = obj.__class__.__name__

        if obj_class_name in self.config.color_scheme:
            return self.config.color_scheme[obj_class_name]
        else:
            return self.config.color_scheme.object


    def _step(self):
        self.screen.fill(self.config.color_scheme.background)

        # Check keys that should be checked every loop (not only on initial keydown)
        if self.config.move_world:
            self._check_keys()

        # Reset the text line to start the text from the top
        self._text_line = self.config.text_line_start

        if self.config.draw_menu:
            # Update the settings based on the GUI
            self._gui_table.updateSettings(self.config)

        # if self.config.pause:
        #     if self.config.singleStep:
        #         self.config.singleStep = False
        #     if self.config.draw_stats:
        #         self._print("****PAUSED****")

        self.renderer.StartDraw()
        self._draw_bodies()

        if self.config.draw_debug_information:
            self._draw_debug_information()

        if self.config.draw_agent_information:
            self._draw_agent_information()

        if self.config.draw_object_information:
            self._draw_object_information()

        self._draw_popup_information()

        if self.config.draw_dialogue:
            self._draw_human_chat()

        self.renderer.EndDraw()

        if self.config.draw_rtf:
            self._print('Real Time Factor : {:.2f}'.format(self.simulation.real_time_factor))

        if self.config.draw_stats:
            self._print("bodies=%d contacts=%d joints=%d proxies=%d" %
                       (self.simulation.box2d_world.bodyCount, self.simulation.box2d_world.contactCount,
                        self.simulation.box2d_world.jointCount, self.simulation.box2d_world.proxyCount))

            self.using_contacts = True
            for point in self.simulation.box2d_simulation.points:
                if point['state'] != b2_nullState:
                    self._print('Collision between %s and %s at point (%.3f,%.3f)' % (
                                    point['fixtureA'].body.userData['name'],
                                    point['fixtureB'].body.userData['name'],
                                    point['position'][0],
                                    point['position'][1])
                    )

        if self.config.draw_menu:
            self._gui_table.updateGUI(self.config)

        self._check_events()
        if self.config.draw_menu:
            self._gui_app.paint(self.screen)

        pygame.display.flip()

    
    def _close(self):
        pass


    def _print(self, text, color=None, font=None, text_line=None, offset_line=None):
        """
        Draw some text at the top status lines
        and advance to the next line.
        """
        if color is None:
            color = self.config.color_scheme.text

        if not font:
            font = self.font
        if not text_line:
            text_line = self._text_line
            self._text_line += 15
        if not offset_line:
            offset_line = self.offsetLine
        self.screen.blit(font.render(text, True, color), (offset_line, text_line))
    

    def _draw_bodies(self):
        for body in self.simulation.box2d_world.bodies:
            for fixture in body.fixtures:
                if isinstance(fixture.shape, b2PolygonShape):
                    vertices = [body.transform * v for v in fixture.shape.vertices]
                    vertices = [(self.convert_world_to_screen(v[0], v[1])) for v in vertices]
                    self.renderer.DrawSolidPolygon(
                        vertices=vertices,
                        color=self.get_color_for_body(body)
                    )
                if isinstance(fixture.shape, b2CircleShape):
                    point = body.transform * fixture.shape.pos
                    axis = body.GetWorldVector((0, 1))
                    self.renderer.DrawSolidCircle(
                        center=self.convert_world_to_screen(point[0], point[1]),
                        radius=fixture.shape.radius,
                        color=self.get_color_for_body(body),
                        axis=axis
                    )


    def _draw_agent_information(self):
        for obj in self.simulation.objects.values():
            if mpi_sim.utils.object_filter(obj, filter=self.config.agent_information.filter):
                text_lines = ['{} : {}'.format(obj.__class__.__name__, obj.id)]
                self._draw_multi_line_text(text_lines, obj.position, self.config.agent_information)


    def _draw_object_information(self):
        for obj in self.simulation.objects.values():
            if not mpi_sim.utils.object_filter(obj, filter=self.config.agent_information.filter):
                text_lines = ['{} : {}'.format(obj.__class__.__name__, obj.id)]
                self._draw_multi_line_text(text_lines, obj.position, self.config.object_information)


    def _draw_popup_information(self):

        # do we point to an object ?
        obj = self._mouse_over_object
        if not obj:
            return

        # get the text that should be displayed, each list entry is a new line
        text_lines = []
        if isinstance(obj, mpi_sim.objects.Human):
            text_lines.append('{} : {}'.format(obj.__class__.__name__, obj.id))
            # text_lines.append('Speaking : Not Implemented')  # TODO: add information if the human is speaking
            text_lines.append('Emotion : {}'.format(obj.config.emotion))
            text_lines.append('Gender : {}'.format(obj.config.gender))
            text_lines.append('Age : {}'.format(obj.config.age))
            text_lines.append('Wearing a mask : {}'.format(obj.config.wearing_mask))
        elif isinstance(obj, mpi_sim.objects.ARIRobot):
            text_lines.append('{} : {}'.format(obj.__class__.__name__, obj.id))
            # text_lines.append('Speaking : Not Implemented')  # TODO: add information if ari is speaking
            # text_lines.append('Goal : Not Implemented')  # TODO: add information about aris navigation goal
        else:
            text_lines.append('{} : {}'.format(obj.__class__.__name__, obj.id))

        self._draw_multi_line_text(text_lines, position=obj.position, config=self.config.popup_information)


    def _draw_human_chat(self):

        # TODO (refactor): use new string drawing methods
        # TODO (feature): print dialog on bottom of simulation in an area that has a filled background

        current_simulation_step = self.simulation.step_counter

        # identify humans and if they said something
        for obj in self.simulation.get_objects_by_type([mpi_sim.objects.Human, mpi_sim.objects.ARIRobot]):
            if 'speech' in obj.components:
                speech_comp = obj.speech

                if speech_comp.current_speech_content is not None:

                    # did the human say something new?
                    if len(speech_comp.history) > 0 and speech_comp.history[-1].start_step == current_simulation_step:

                        # identify the type of entity that said something
                        if isinstance(obj, mpi_sim.objects.Human):
                            type_str = 'Human'
                        elif isinstance(obj, mpi_sim.objects.ARIRobot):
                            type_str = 'Robot'
                        else:
                            type_str = 'Unknown'

                        self.human_speech_history.append((obj.id, type_str, speech_comp.current_speech_content))

        delta = 15 
        if len(self.human_speech_history) > self.human_speech_history_length:
            del self.human_speech_history[:-self.human_speech_history_length]

        for h_id, type_str, content in reversed(self.human_speech_history):
            self.screen.blit(
                pygame.font.Font("freesansbold.ttf", 15).render(
                    '{} {}: {}'.format(type_str, h_id, content),
                    True,
                    self.config.color_scheme.text
                ),
                (self.offsetLine, self.screenSize[1] - delta)
            )
            delta += 15

        # title
        self.screen.blit(
            pygame.font.Font("freesansbold.ttf", 20).render('Dialogue', True, self.config.color_scheme.text),
            (self.offsetLine, self.screenSize[1] - (15 * self.human_speech_history_length + 30))
        )


    def _draw_debug_information(self):
        # convertVertices is only applicable when using b2DrawExtended.  It
        # indicates that the C code should transform box2d coords to screen
        # coordinates.
        self.renderer.flags = dict(
            drawShapes=False,
            drawJoints=True,
            drawAABBs=True,
            drawPairs=True,
            drawCOMs=True,
            convertVertices=isinstance(self.renderer, b2DrawExtended),
        )
        self.simulation.box2d_world.DrawDebugData()

        # Draw each of the contact points in different colors.
        for point in self.simulation.box2d_simulation.points:
            if point['state'] == b2_addState:
                self.renderer.DrawPoint(
                    self.renderer.to_screen(point['position']),
                    self.config.point_size,
                    self.config.color_scheme.contact_add
                )
            elif point['state'] == b2_persistState:
                self.renderer.DrawPoint(
                    self.renderer.to_screen(point['position']),
                    self.config.point_size,
                    self.config.color_scheme.contact_persist
                )

        for point in self.simulation.box2d_simulation.points:
            if point['state'] != b2_nullState:
                p1 = self.renderer.to_screen(point['position'])
                p2 = self.renderer.axisScale * point['normal'] + p1
                self.renderer.DrawSegment(p1, p2, self.config.color_scheme.contact_normal)


    def _draw_multi_line_text(self, text_lines, position, config):
        r"""Draws several (or a single) line of text."""
        # TODO (refactor): this function should be part of the renderer

        if text_lines is None or len(text_lines) == 0:
            return

        default_config = mpi_sim.AttrDict(
            x_offset = 0,  # y offset to the position
            y_offset = 0,  # y offset to the position
            x_padding = 0,
            y_padding = 5,
            text_color = 'text',
            border_color = None,
            background_color = None,
        )
        config = mpi_sim.combine_dicts(config, default_config)

        # identify the size of the pop up window
        popup_height = config.y_padding
        popup_width = 0

        rendered_text_lines = []
        for text_line in text_lines:
            rendered_text = self.font.render(text_line, True, self.config.color_scheme[config.text_color])
            rendered_text_lines.append(rendered_text)
            popup_width = max(popup_width, rendered_text.get_width() + 2 * config.x_padding)
            popup_height += rendered_text.get_height() + config.y_padding

        # draw the popup area
        pos = self.convert_world_to_screen(position[0], position[1])
        rect = pygame.Rect(pos[0] + config.x_offset, pos[1] + config.y_offset, popup_width, popup_height)
        if config.background_color:
            pygame.draw.rect(self.renderer.surface, self.config.color_scheme[config.background_color], rect, 0)  # filling
        if config.border_color:
            pygame.draw.rect(self.renderer.surface, self.config.color_scheme[config.border_color], rect, 1)  # border

        # draw the text
        x = rect[0] + config.x_padding
        y = rect[1] + config.y_padding
        for rendered_text_line in rendered_text_lines:
            self.screen.blit(rendered_text_line, (x, y))
            y += rendered_text_line.get_height() + config.y_padding


    def convert_world_to_screen(self, x, y):
        return b2Vec2(x * self.viewZoom - self.viewOffset.x,
                      self.screenSize.y - y * self.viewZoom + self.viewOffset.y)


    def convert_screen_to_world(self, x, y):
        return b2Vec2((x + self.viewOffset.x) / self.viewZoom,
                      ((self.screenSize.y - y + self.viewOffset.y) / self.viewZoom))


    # maybe used later for key control of agents
    # def Keyboard(self, key):
    #     key_map = self.key_map
    #     if key in key_map:
    #         self.simulation.box2d_simulation.pressed_keys.add(key_map[key])
    # def KeyboardUp(self, key):
    #     key_map = self.key_map
    #     if self.simulation.box2d_simulation.pressed_keys:
    #         if key in key_map:
    #             self.simulation.box2d_simulation.pressed_keys.remove(key_map[key])
            

    # def _print_contact(self, contact):
    #     if contact.worldManifold.points != ((0, 0), (0, 0)):
    #         print("Collision between %s and %s at point (%.3f,%.3f)" % (contact.fixtureA.body.userData['name'],
    #                                                                     contact.fixtureB.body.userData['name'],
    #                                                                     contact.worldManifold.points[0][0],
    #                                                                     contact.worldManifold.points[0][1]))

    def _check_events(self):
        """
        Check for pygame events (mainly keyboard/mouse events).
        Passes the events onto the GUI also.
        """       
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == Keys.K_ESCAPE):
                self.simulation.close()
                return False
            elif event.type == KEYDOWN:
                self._keyboard_event(event.key, down=True)
            elif event.type == KEYUP:
                self._keyboard_event(event.key, down=False)
            elif event.type == MOUSEBUTTONDOWN and self.config.move_world:
                p = self.convert_screen_to_world(*event.pos)
                if event.button == 1:  # left
                    mods = pygame.key.get_mods()
                    if mods & KMOD_LSHIFT:
                        self._shift_mouse_down(p)
                    else:
                        self._mouse_down(p)
                elif event.button == 2:  # middle
                    pass
                elif event.button == 3:  # right
                    self.r_mouse_down = True
                elif event.button == 4:
                    self.viewZoom *= 1.1
                elif event.button == 5:
                    self.viewZoom /= 1.1
            elif event.type == MOUSEBUTTONUP and self.config.move_world:
                p = self.convert_screen_to_world(*event.pos)
                if event.button == 3:  # right
                    self.r_mouse_down = False
                else:
                    pass
                    # self.MouseUp(p)
            elif event.type == MOUSEMOTION:
                p = self.convert_screen_to_world(*event.pos)
                self._mouse_move(p)

                if self.r_mouse_down:
                    self.viewCenter -= (event.rel[0] / 5.0, -event.rel[1] / 5.0)

            if self.config.draw_menu:
                self._gui_app.event(event)  # Pass the event to the GUI

        return True


    def _keyboard_event(self, key, down=True):
        """
        Internal keyboard event, don't override this.
        Checks for the initial keydown of the basic testbed keys. Passes the unused
        ones onto the test via the Keyboard() function.
        """
        if down:            
            if key == Keys.K_z and self.config.move_world:       # Zoom in
                self.viewZoom = min(1.1 * self.viewZoom, 200.0)
            elif key == Keys.K_x and self.config.move_world:     # Zoom out
                self.viewZoom = max(0.9 * self.viewZoom, 0.02)
            elif key == Keys.K_F1:    # Toggle drawing the menu
                self.config.draw_menu = not self.config.draw_menu
            elif key == Keys.K_F2:    # Do a single step
                self.config.singleStep = True
                if self.config.draw_menu:
                    self._gui_table.updateGUI(self.config)
            # elif self.config.key_control:              # maybe used for key control
            #     self.Keyboard(key)
        # elif self.config.key_control:
        #     self.KeyboardUp(key)


    def _check_keys(self):
        """
        Check the keys that are evaluated on every main loop iteration.
        I.e., they aren't just evaluated when first pressed down
        """

        pygame.event.pump()
        self.keys = keys = pygame.key.get_pressed()
        if keys[Keys.K_LEFT]:
            self.viewCenter -= (0.5, 0)
        elif keys[Keys.K_RIGHT]:
            self.viewCenter += (0.5, 0)

        if keys[Keys.K_UP]:
            self.viewCenter += (0, 0.5)
        elif keys[Keys.K_DOWN]:
            self.viewCenter -= (0, 0.5)

        if keys[Keys.K_HOME]:
            self.viewZoom = 1.0
            self.viewCenter = (0.0, 20.0)
    

    def _mouse_move(self, p):
        """
        Mouse moved to point p, in world coordinates.
        """
        self._mouse_world = p

        # Make a small box.
        aabb = b2AABB(lowerBound=p - (0.01, 0.01),
                      upperBound=p + (0.01, 0.01))

        # Query the world for overlapping shapes.
        query = MouseOverQueryCallback(p)
        self.simulation.box2d_world.QueryAABB(query, aabb)
        if query.fixture:
            self._mouse_over_object = query.fixture.body.userData['object']
            #self.query_userData['time'] = time.time()
        else:
            self._mouse_over_object = None
            #self.query_userData['time'] = None
    

    def _shift_mouse_down(self, p):
        """
        Indicates that there was a left click at point p (world coordinates)
        with the left shift key being held down.
        """
        self._mouse_world = p


    def _mouse_down(self, p):
        """
        Indicates that there was a left click at point p (world coordinates)
        """
        pass
        # # Create a mouse joint on the selected body (assuming it's dynamic)
        # # Make a small box.
        # aabb = b2AABB(lowerBound=p - (0.001, 0.001),
        #               upperBound=p + (0.001, 0.001))
        #
        # # Query the world for overlapping shapes.
        # query = fwQueryCallback(p)
        # self.simulation.box2d_world.QueryAABB(query, aabb)
        # if query.fixture:
        #     body = query.fixture.body
        #     body.awake = True


class PygameDraw(b2DrawExtended):
    """
    This draw class is used for the regular drawing of shapes and the debug drawing mode of Box2D.
    For the debug drawing mode it accepts callbacks from Box2D (which specifies what to draw) and handles all the rendering.
    """
    surface = None
    axisScale = 10.0

    def __init__(self, gui=None, **kwargs):
        b2DrawExtended.__init__(self, **kwargs)
        self.flipX = False
        self.flipY = True
        self.convertVertices = True
        self.test = gui


    def StartDraw(self):
        self.zoom = self.test.viewZoom
        self.center = self.test.viewCenter
        self.offset = self.test.viewOffset
        self.screenSize = self.test.screenSize


    def EndDraw(self):
        pass


    def DrawPoint(self, p, size, color):
        """
        Draw a single point at point p given a pixel size and color.
        """
        self.DrawCircle(p, size / self.zoom, color, drawwidth=0)


    def DrawAABB(self, aabb, color):
        """
        Draw a wireframe around the AABB with the given color.
        """
        points = [(aabb.lowerBound.x, aabb.lowerBound.y),
                  (aabb.upperBound.x, aabb.lowerBound.y),
                  (aabb.upperBound.x, aabb.upperBound.y),
                  (aabb.lowerBound.x, aabb.upperBound.y)]

        pygame.draw.aalines(self.surface, color, True, points)


    def DrawSegment(self, p1, p2, color):
        """
        Draw the line segment from p1-p2 with the specified color.
        """
        pygame.draw.aaline(self.surface, color, p1, p2)


    def DrawTransform(self, xf):
        """
        Draw the transform xf on the screen
        """
        p1 = xf.position
        p2 = self.to_screen(p1 + self.axisScale * xf.R.x_axis)
        p3 = self.to_screen(p1 + self.axisScale * xf.R.y_axis)
        p1 = self.to_screen(p1)
        pygame.draw.aaline(self.surface, (255, 0, 0), p1, p2)
        pygame.draw.aaline(self.surface, (0, 255, 0), p1, p3)


    def DrawCircle(self, center, radius, color, drawwidth=1):
        """
        Draw a wireframe circle given the center, radius, axis of orientation
        and color.
        """
        radius *= self.zoom
        if radius < 1:
            radius = 1
        else:
            radius = int(radius)

        pygame.draw.circle(self.surface, color, center, radius, drawwidth)


    def DrawSolidCircle(self, center, radius, axis, color):
        """
        Draw a solid circle given the center, radius, axis of orientation and
        color.
        """

        if isinstance(color, b2Color):
            color = Color(*color.bytes)

        radius *= self.zoom
        if radius < 1:
            radius = 1
        else:
            radius = int(radius)

        pygame.draw.circle(self.surface, color.correct_gamma(0.5), center, radius, 0)  # filling
        pygame.draw.circle(self.surface, color, center, radius, 1)  # border
        # pygame.draw.aaline(self.surface, (255, 0, 0), center,
        #                    (center[0] - radius * axis[0],
        #                     center[1] + radius * axis[1]))


    def DrawPolygon(self, vertices, color):
        """
        Draw a wireframe polygon given the screen vertices with the specified color.
        """
        if not vertices:
            return

        if len(vertices) == 2:
            pygame.draw.aaline(self.surface, color, vertices[0], vertices)
        else:
            pygame.draw.polygon(self.surface, color, vertices, 1)


    def DrawSolidPolygon(self, vertices, color):
        """
        Draw a filled polygon given the screen vertices with the specified color.
        """
        if not vertices:
            return

        if isinstance(color, b2Color):
            color = Color(*color.bytes)

        if len(vertices) == 2:
            pygame.draw.aaline(self.surface, color, vertices[0], vertices[1])
        else:
            pygame.draw.polygon(self.surface, color.correct_gamma(0.5), vertices, 0)
            pygame.draw.polygon(self.surface, color, vertices, 1)


class MenuTable(gui.Table):
    """
    Deals with the initialization and changing the settings based on the GUI
    controls. Callbacks are not used, but the checkboxes and sliders are polled
    by the main loop.
    """

    # TODO (Feature): fill the background behind the table for better visability

    form = None

    def __init__(self, config, settings, **params):
        # The framework GUI is just basically a HTML-like table
        # There are 2 columns right-aligned on the screen
        gui.Table.__init__(self, **params)
        self.form = gui.Form()

        self.config = config

        # fg = (255, 255, 255)

        # "Toggle menu"
        self.tr()
        self.td(gui.Label('F1: Toggle Menu', color=settings.color_scheme.text), align=1, colspan=2)

        for slider in self.config.sliders:
            # "Slider title"
            self.tr()
            self.td(gui.Label(slider['text'], color=settings.color_scheme.text), align=1, colspan=2)

            # Create the slider
            self.tr()
            e = gui.HSlider(getattr(settings, slider['name']), slider['min'], slider['max'], size=20, width=100, height=16, name=slider['name'])
            self.td(e, colspan=2, align=1)

        # Add each of the checkboxes.
        for text, variable in self.config.checkboxes:
            self.tr()
            if variable is None:
                # Checkboxes that have no variable (i.e., None) are just labels.
                self.td(gui.Label(text, color=settings.color_scheme.text), align=1, colspan=2)
            else:
                # Add the label and then the switch/checkbox
                self.td(gui.Label(text, color=settings.color_scheme.text), align=1)
                self.td(gui.Switch(value=getattr(settings, variable), name=variable))


    def updateGUI(self, settings):
        """
        Change all of the GUI elements based on the current settings
        """
        for text, variable in self.config.checkboxes:
            if not variable:
                continue
            if hasattr(settings, variable):
                self.form[variable].value = getattr(settings, variable)

        # Now do the sliders
        for slider in self.config.sliders:
            name = slider['name']
            self.form[name].value = getattr(settings, name)


    def updateSettings(self, settings):
        """
        Change all of the settings based on the current state of the GUI.
        """
        for text, variable in self.config.checkboxes:
            if variable:
                setattr(settings, variable, self.form[variable].value)

        # Now do the sliders
        for slider in self.config.sliders:
            name = slider['name']
            setattr(settings, name, int(self.form[name].value))

        # # If we're in single-step mode, update the GUI to reflect that.
        # if settings.singleStep:
        #     settings.pause = True
        #     self.form['pause'].value = True
        #     self.form['singleStep'].value = False


class Keys(object):
    pass


class MouseOverQueryCallback(b2QueryCallback):

    def __init__(self, p):
        super(MouseOverQueryCallback, self).__init__()
        self.point = p
        self.fixture = None


    def ReportFixture(self, fixture):
        inside = fixture.TestPoint(self.point)
        if inside:
            self.fixture = fixture
            # We found the object, so stop the query
            return False
        # Continue the query
        return True
