import mpi_sim
from mpi_sim.core.object import Object
import numpy as np
from Box2D import (b2PolygonShape, b2FixtureDef, b2Vec2, b2CircleShape)
# from mpi_sim.utils.box2d_utils import (update_drive, update_turn)  # for key control


class Human(Object):

    @staticmethod
    def default_config():
        dc = Object.default_config()

        # add default components
        dc.components.append(Human.default_speech_generator_config())
        dc.components.append(Human.default_navigation_config())

        dc.properties.append('dynamic')

        dc.position = [0., 0.]
        dc.orientation = 0.
        dc.density = 0.
        dc.mass = 80.
        dc.age = None
        dc.gender = None
        dc.wearing_mask = None
        dc.emotion = None

        # config for group navigation by the script.GroupNavigation
        dc.personal_distance = 1.5
        dc.public_distance = 3.0
        dc.max_movement_force = 0.25
        dc.max_rotation_force = 6.


        # maybe used in future for key control
        # dc.max_lin_vel = 1.5
        # dc.max_ang_vel = np.pi/2
        # dc.key_control = False

        return dc


    @staticmethod
    def default_navigation_config():
        return mpi_sim.AttrDict(
            type=mpi_sim.components.SocialForceNavigation,
            name='navigation',
            step_length_coeff = 0.2,
        )


    @staticmethod
    def default_speech_generator_config():
        return mpi_sim.AttrDict(
            type=mpi_sim.components.SpeechGenerator
        )


    ###############################################################################
    # Position and orientation


    @property
    def position(self):
        if self.is_removed:
            return None
        else:
            return np.array(self.box2d_body.position)


    @position.setter
    def position(self, position):
        """Sets the position of the agent in the next step."""

        if position is None:
            position = (0.0, 0.0)

        self.box2d_body.position = (float(position[0]), float(position[1]))


    @property
    def orientation(self):
        if self.is_removed:
            return None
        else:
            return self.box2d_body.angle


    @orientation.setter
    def orientation(self, orientation):
        """Sets the position of the agent in the next step."""

        if orientation is None:
            orientation = 0.0

        self.box2d_body.angle = float(orientation)


    ###############################################################################
    # velocities of position and orientation


    @property
    def position_velocity(self):
        if self.is_removed:
            return None
        else:
            return np.array(self.box2d_body.linearVelocity)


    @position_velocity.setter
    def position_velocity(self, position_velocity):
        """Sets a position velocity (x, y) of the agent. None to stop."""

        if position_velocity is None:
            position_velocity = (0.0, 0.0)

        self.box2d_body.linearVelocity = (float(position_velocity[0]), position_velocity[1])
        # self.box2d_body.linearVelocity = calc_linear_velocity_from_forward_velocity(
        #     forward_velocity,
        #     self.box2d_body
        # )



    @property
    def orientation_velocity(self):
        if self.is_removed:
            return None
        else:
            return self.box2d_body.angularVelocity


    @orientation_velocity.setter
    def orientation_velocity(self, orientation_velocity):
        """Sets a constant orientation velocity of the agent. None to stop."""

        if orientation_velocity is None:
            orientation_velocity = 0.0

        self.box2d_body.angularVelocity = float(orientation_velocity)


    ###############################################################################


        

    @property
    def group(self):
        """The group the agent is part of. None if he is not part of the group."""
        return self.state.group


    def _set_group(self, group):
        """Internal. Sets the group of the human."""
        self.state.group = group


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.state.group = None

        self.box2d_body = None

        
        # maybe used in future for key control
        # self.keys = None
        

    def _create_in_box2d_world(self, box2d_world):
        self.box2d_body = box2d_world.CreateDynamicBody(
            position=self.config.position,
            angle=self.config.orientation,
            linearDamping=2.,
            angularDamping=2.,
            fixtures=[
                b2FixtureDef(shape=b2PolygonShape(box=(
                    0.10/2, 0.26/2, b2Vec2(0.1, 0.12), -np.pi/12)),
                    density=self.config.density),  # right foot
                b2FixtureDef(shape=b2PolygonShape(box=(
                    0.10/2, 0.26/2, b2Vec2(-0.1, 0.12), np.pi/12)),
                    density=self.config.density),  # left foot
                b2FixtureDef(shape=b2CircleShape(
                    radius=0.1, pos=(-.25+0.05, .0)),
                    density=self.config.density),  # left shoulder
                b2FixtureDef(shape=b2CircleShape(
                    radius=0.1, pos=(.25-0.05, .0)),
                    density=self.config.density),  # right shoulder
                b2FixtureDef(shape=b2CircleShape(
                    radius=0.25, pos=(0, 0)),
                    density=self.config.density),  # torso
                b2FixtureDef(shape=b2CircleShape(
                    radius=0.12, pos=(0, 0)),
                    density=self.config.density),  # head
            ],
            userData={'obj': self, 'name': self.id}
        )
        self.box2d_body.mass = self.config.mass

        return self.box2d_body


    # maybe used in future for key control
    # def _add(self):
    #     if self.config.key_control:
    #         self.keys = self.simulation.box2d_simulation.pressed_keys


    # def _step(self):
        # step the human by updating its position, orientation, or their velocities

        # TODO: include manual control if wanted
        # if self.state.goal_position is not None or self.state.goal_orientation is not None:
            #     # apply a force to let the body move
            #     # self.box2d_body.ApplyForce(force=(100, 100), point=(0.0, 0.0), wake=True)
            #     self.box2d_body.awake = True
            #     lin_vel, ang_vel = 0., 0.
            #
            #     self.box2d_body.linearVelocity = update_drive(self.keys, lin_vel, mobile_obj=self.box2d_body, max_speed=self.config.max_lin_vel)
            #     self.box2d_body.angularVelocity = update_turn(self.keys, ang_vel, max_speed=self.config.max_ang_vel)



