from mpi_sim.objects.human import Human
from mpi_sim.objects.wall import Wall
from mpi_sim.objects.bench import Bench
from mpi_sim.objects.chair import Chair
from mpi_sim.objects.round_table import RoundTable
from mpi_sim.objects.ari_robot import ARIRobot