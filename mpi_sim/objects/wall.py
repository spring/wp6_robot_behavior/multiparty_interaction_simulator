from mpi_sim.core.object import Object
from Box2D import b2PolygonShape
import numpy as np
import warnings


class Wall(Object):

    @staticmethod
    def default_config():
        dc = Object.default_config()

        dc.properties.append('static')

        dc.position = [0.0, 0.0]
        dc.orientation = 0.0
        dc.width = 0.2
        dc.length = 2.0
        dc.density = 1.0

        dc.height = None  # this was the former length

        return dc


    @property
    def position(self):
        if self.is_removed:
            return None
        else:
            return np.array(self.box2d_body.position)


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        if self.config.height is not None:
            self.config.length = self.config.height
            warnings.warn('The configuration \'height\' for walls is replaced by \'length\'. The height parameter will be removed in future releases!')

        self.box2d_body = None


    def _create_in_box2d_world(self, box2d_world):

        self.box2d_body = box2d_world.CreateStaticBody(
            position=self.config.position,
            angle=self.config.orientation,
            shapes=b2PolygonShape(box=(self.config.width/2, self.config.length/2)),
            userData={'obj': self, 'name': self.id}
        )

        return self.box2d_body
