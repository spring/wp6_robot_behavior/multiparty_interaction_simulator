import mpi_sim
from mpi_sim.core.object import Object
from mpi_sim.utils.box2d_utils import constraint_angle, calc_linear_velocity_from_forward_velocity
# from mpi_sim.components.raycast import Raycast
from Box2D import b2Vec2, b2_pi
import numpy as np
import inspect

# TODO: replace the head raycast with a object detector


class ARIRobot(Object):

    @staticmethod
    def default_config():
        dc = Object.default_config()

        # add default components
        dc.components.append(ARIRobot.default_mapping_config())
        dc.components.append(ARIRobot.default_human_detector())
        nav_dc = ARIRobot.default_navigation_config()
        if nav_dc is not None:
            dc.components.append(nav_dc)
        dc.components.append(ARIRobot.default_speech_generator_config())

        # ari is a dynamic entity, used for creating maps of only static entities
        dc.properties.append('dynamic')

        dc.position = [0.0, 0.0]
        dc.orientation = 0.0
        dc.pan_orientation_offest = 0.0
        dc.density = 0.

        dc.max_ang_vel = np.pi/2

        dc.max_forward_speed = 5.
        dc.max_backward_speed = -0.1
        dc.max_pan_angle = 1.309
        dc.min_pan_angle = -1.309
        dc.max_speed_motor_head = 3.0
        # dc.max_speed_motor_wheel = 1.0
        dc.wheel_radius = 0.125
        dc.wheel_separation = 0.44
        dc.wheel_width = 0.075
        dc.base_footprint_radius = 0.3
        dc.total_mass = 54.893
        dc.wheel_mass = 1.65651
        dc.enable_pan = False
        # dc.enable_raycast = False
        dc.base_link2head_front_camera_optical_frame = [-0.120, -0.000, 1.466]
        dc.head_cam_fov_h = 62.
        dc.head_cam_fov_v = 48.
        # dc.delta_raycast = 0.02

        dc.personal_distance = 1.5
        dc.public_distance = 2.0
        dc.max_movement_force = 0.25
        dc.max_rotation_force = 6.

        dc.linearDamping = 2
        dc.angularDamping = 2

        return dc


    @staticmethod
    def default_mapping_config():
        r"""The default component config for the mapping component.

        The component is a OccupancyGridMapping that maps all objects which have the property 'static'.
        The component is named 'mapping'.
        """
        dc = mpi_sim.components.OccupancyGridMapping.default_config()
        dc.type = mpi_sim.components.OccupancyGridMapping
        dc.name = 'mapping'
        dc.depth = 3.0
        dc.object_filter = dict(properties='static')
        dc.resolution = 0.05
        dc.update_global_map_automatically = False
        return dc


    @staticmethod
    def default_human_detector():
        return mpi_sim.AttrDict(
            type = mpi_sim.components.ObjectDetector,
            name = 'human_detector',
            fov = np.pi,
            object_filter = dict(types=mpi_sim.objects.Human),
            ignore_objects_filter = dict(properties=['human_detector_invisible'])
        )



    @staticmethod
    def default_navigation_config():
        if not any(class_name == 'ARINavigation' for (class_name, _) in inspect.getmembers(mpi_sim.components)):
            return None
        else:
            return mpi_sim.AttrDict(
                type = mpi_sim.components.ARINavigation,
                name = 'navigation',
                ari_mapping_component = 'mapping',
                human_detector_component = 'human_detector',
            )


    @staticmethod
    def default_speech_generator_config():
        return mpi_sim.AttrDict(
                type=mpi_sim.components.SpeechGenerator
        )


    ##############################################################
    # Position and Orientation


    @property
    def position(self):
        if self.is_removed:
            return None
        else:
            return np.array(self.box2d_body.position)


    @position.setter
    def position(self, position):
        """Sets the position of the agent in the next step."""

        if position is None:
            position = (0.0, 0.0)

        self.box2d_body.position = (float(position[0]), float(position[1]))
        self.pan.body.position = np.array((float(position[0]), float(position[1]))) + np.array(self.pan_anchors)


    @property
    def orientation(self):
        if self.is_removed:
            return None
        else:
            return constraint_angle(self.box2d_body.angle)


    @orientation.setter
    def orientation(self, orientation):
        """Sets the position of the agent in the next step."""

        if orientation is None:
            orientation = 0.0

        self.box2d_body.angle = float(orientation)


    ##############################################################
    # Velocities for Position and Orientation


    @property
    def forward_velocity(self):
        r"""Velocity of ARI in its forward direction. Float."""
        if self.is_removed:
            return None
        else:
            raise NotImplementedError('Calculation of the forward velocity is not implemented!')


    @forward_velocity.setter
    def forward_velocity(self, forward_velocity):
        r"""Sets a forward velocity (float) which is the velocity in the forward direction. None to stop."""

        if forward_velocity is None:
            forward_velocity = 0.0

        # keep velocity in possible range
        forward_velocity = np.clip(forward_velocity, a_min=self.config.max_backward_speed, a_max=self.config.max_forward_speed)

        # calc the linear velocity which is in (x,y) from the forward velocity which is a scalar
        self.box2d_body.linearVelocity = calc_linear_velocity_from_forward_velocity(
            forward_velocity,
            self.box2d_body
        )


    @property
    def position_velocity(self):
        r"""Linear velocity of ARI's position. Tuple: (x,y)"""
        if self.is_removed:
            return None
        else:
            return self.box2d_body.linearVelocity


    @position_velocity.setter
    def position_velocity(self, position_velocity):
        """Sets a position velocity (linearVelocity) of the agent. None to stop."""

        if position_velocity is None:
            position_velocity = (0.0, 0.0)

        self.box2d_body.linearVelocity = (float(position_velocity[0]), position_velocity[1])


    @property
    def orientation_velocity(self):
        if self.is_removed:
            return None
        else:
            return self.box2d_body.angularVelocity


    @orientation_velocity.setter
    def orientation_velocity(self, orientation_velocity):
        """Sets a orientation velocity (angularVelocity) of the agent. None to stop."""

        if orientation_velocity is None:
            orientation_velocity = 0.0

        # keep velocity in possible range
        orientation_velocity = np.clip(orientation_velocity, a_min=-self.config.max_ang_vel, a_max=self.config.max_ang_vel)

        self.box2d_body.angularVelocity = float(orientation_velocity)


    ##############################################################


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.box2d_body = None

        # self.keys = None
        # self.raycast_obs = None
        self.groups_nav = None
        self.robot_group = None
        
        self.config.pan_orientation = self.config.orientation + self.config.pan_orientation_offest

        if self.config.enable_pan:
            self.pan_init_ang = 0.0

        self.radius = self.config.wheel_separation / 2
        self.joints = []

        self.offset_pan = [0, self.config.base_link2head_front_camera_optical_frame[0]]
        self.pan_anchors = (self.offset_pan[0], self.offset_pan[1])
        self.pan_vel = 0


    def _create_in_box2d_world(self, box2d_world):

        wheel_dim = (self.config.wheel_width / 2, self.config.wheel_radius)
        tire_anchors = [
            (-self.radius + wheel_dim[0], 0),
            (self.radius - wheel_dim[0], 0)
        ]

        self.box2d_body = box2d_world.CreateDynamicBody(position=self.config.position, angle=self.config.orientation)
        self.box2d_body.mass = self.config.total_mass - 2 * self.config.wheel_mass
        self.box2d_body.CreateCircleFixture(
            radius=self.radius,
            density=self.box2d_body.mass/(np.pi*np.power(self.config.wheel_separation/2, 2))
        )
        self.box2d_body.userData = dict(obj=self)
        self.box2d_body.userData['name'] = self.config.id
        self.box2d_body.linearDamping = self.config.linearDamping
        self.box2d_body.angularDamping = self.config.angularDamping

        self.box2d_body.CreatePolygonFixture(
            box=(*wheel_dim, b2Vec2(tire_anchors[0]), 0),
            density=self.box2d_body.mass/(wheel_dim[0] * wheel_dim[1])
        )
        self.box2d_body.CreatePolygonFixture(
            box=(*wheel_dim, b2Vec2(tire_anchors[1]), 0),
            density=self.box2d_body.mass/(wheel_dim[0] * wheel_dim[1])
        )

        # if self.config.enable_raycast:
        #     raycast_head = Raycast(fov=self.config.head_cam_fov_h, raycast_length=15)
        #     self.add_component(raycast_head, name='raycast_head')

        self.pan = PanJoint(box2d_world, self.config)
        if self.config.enable_pan:
            j = box2d_world.CreateRevoluteJoint(bodyA=self.pan.body,
                                                bodyB=self.box2d_body,
                                                localAnchorB=self.pan_anchors,
                                                localAnchorA=(0, 0),
                                                enableMotor=True,
                                                maxMotorTorque=1000,
                                                enableLimit=True,
                                                lowerAngle=self.pan.min_angle,
                                                upperAngle=self.pan.max_angle,
                                                referenceAngle=0
                                                )
        else:
            j = box2d_world.CreateWeldJoint(bodyA=self.box2d_body,
                                            bodyB=self.pan.body,
                                            localAnchorA=self.pan_anchors,
                                            localAnchorB=(0, 0),
                                            referenceAngle=0
                                            )
        self.pan.body.position = np.array(self.config.position) + np.array(self.pan_anchors)
        self.joints.append(j)

        return self.box2d_body, self.pan.body


    def _step(self):

        ##############################
        # handle ARI and groups
        # if ARI is close to a group, i.e. its position is less than group.config.person_merge_distance to the group center, then add ari to this group
        # TODO: this code seems to assume that there is only one group, allow ARI to be in several groups

        if self.groups_nav is None:
            groups_nav = []
            for script in self.simulation.get_scripts_by_type(mpi_sim.scripts.GroupNavigation):
                groups_nav.append(script)
            if not groups_nav:
                self.groups_nav = 0
            else:
                self.groups_nav = groups_nav
        
        if isinstance(self.groups_nav, list):
            for group in self.groups_nav:
                if np.linalg.norm(group.position - np.array(self.position)) <= group.config.person_merge_distance:
                    if not self.robot_group:
                        group.add_agent(self)
                        self.robot_group = group
                elif self.robot_group:                    
                    group.remove_agent(self)
                    self.robot_group = None

        #############################

        # if self.pan.body.awake:
        #     self.pan.update_turn(None, self.joints[-1], self.pan_vel)  # keys = None

        ############################

        # if self.config.enable_raycast:
        #
        #
        #     # calculate the start (point1) and end points (points2) of the raycast depending on the position and orientation of the head and body
        #
        #     delta = self.config.delta_raycast
        #     # point1 = b2Vec2(self.joints[-1].anchorA)
        #     point1 = b2Vec2(self.box2d_body.position)
        #     raycast_head = self.raycast_head
        #     angle = constraint_angle(constraint_angle(self.joints[-1].angle + np.pi/2) + self.box2d_body.angle)
        #     # lmbda = (self.radius + np.abs(self.offset_pan[1])) * np.cos(self.joints[-1].angle)
        #     lmbda = self.radius
        #     d = np.transpose(np.asarray([(lmbda + delta) * np.cos(angle), (lmbda + delta) * np.sin(angle)]))
        #     point1 = np.asarray(point1) + d
        #     angle = constraint_angle(constraint_angle(constraint_angle(self.joints[-1].angle + np.pi/2) + self.box2d_body.angle) - raycast_head.fov/2)
        #     angle_array = constraint_angle(angle + np.arange(raycast_head.array_size)*raycast_head.config.delta_angle * b2_pi / 180)
        #     d = np.transpose(np.asarray([raycast_head.config.raycast_length * np.cos(angle_array), raycast_head.config.raycast_length * np.sin(angle_array)]))
        #     raycast_head.points2 = np.asarray(point1) + d
        #     raycast_head.point1 = point1
        #     raycast_head.color = raycast_head.config.s1_color


class PanJoint(object):

    def __init__(self, world, config, density=0.01, position=(0., 0.), dimensions=(0.05/2, 0.10/2)):
        self.body = world.CreateDynamicBody(position=position, angle=config.pan_orientation)
        self.body.CreatePolygonFixture(box=dimensions, density=density)
        self.body.userData = dict(obj=self, name='pan')
        self.max_speed_motor_head = config.max_speed_motor_head
        self.max_angle = config.max_pan_angle 
        self.min_angle = config.min_pan_angle


    def update_turn(self, keys, joint, desired_torque=0.):
        if keys:
            if 'pan_left' in keys:
                desired_torque = self.max_speed_motor_head
            elif 'pan_right' in keys:
                desired_torque = -self.max_speed_motor_head
        if joint.angle < self.min_angle and desired_torque < 0.:
            desired_torque = 0.
        if joint.angle > self.max_angle and desired_torque > 0.:
            desired_torque = 0.
        if desired_torque > self.max_speed_motor_head:
            desired_torque = self.max_speed_motor_head
        if desired_torque < -self.max_speed_motor_head:
            desired_torque = -self.max_speed_motor_head
        joint.motorSpeed = desired_torque
