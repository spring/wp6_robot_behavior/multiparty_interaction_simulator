from mpi_sim.core.object import Object
from Box2D import b2PolygonShape


class GroundArea(Object):

    @staticmethod
    def default_config():
        dc = Object.default_config()

        dc.position = [0.0, 0.0]
        dc.orientation = 0.0
        dc.width = 10.0
        dc.height = 10.0
        dc.friction_modifier = 0.0

        return dc



    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.box2d_body = None
        self.friction_modifier = self.config.friction_modifier


    def _create_in_box2d_world(self, box2d_world):
        self.box2d_body = box2d_world.CreateStaticBody(position=self.config.position,
                                                       angle=self.config.orientation,
                                                       shapes=b2PolygonShape(box=(self.config.width/2, self.config.height/2)),
                                                       userData={'obj': self, 'name': self.id})

        return self.box2d_body