from mpi_sim.core.object import Object
from Box2D import b2CircleShape
import numpy as np

class RoundTable(Object):

    @staticmethod
    def default_config():
        dc = Object.default_config()

        dc.properties.append('static')
        dc.properties.append('human_detector_invisible')

        dc.position = [0.0, 0.0]
        dc.orientation = 0.0
        dc.radius = 1.0
        dc.density= 1.0

        return dc


    @property
    def position(self):
        if self.is_removed:
            return None
        else:
            return np.array(self.box2d_body.position)


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.box2d_body = None


    def _create_in_box2d_world(self, box2d_world):        
        self.box2d_body = box2d_world.CreateStaticBody(
            position=self.config.position,
            angle=self.config.orientation,
            shapes=b2CircleShape(radius=self.config.radius),
            userData={'obj': self, 'name': self.id}
        )

        return self.box2d_body