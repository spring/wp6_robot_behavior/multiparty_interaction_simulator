import mpi_sim as sim
from mpi_sim.utils.readonly_combined_dict import ReadOnlyCombinedDict
from mpi_sim.utils.readonly_dict import ReadOnlyDict
from mpi_sim.core import Box2DSimulation
import warnings
from collections import OrderedDict
import mpi_sim.core.module_library as modlib
import numpy as np
from operator import itemgetter
import pygame


class EntityReadOnlyCombinedDictionary(ReadOnlyCombinedDict):
    """Readonly combined dictionary that directly returns the entity if it was given as key.
    This allows to get quickly targets either by id or by taking the reference to the entity.
    """

    def __getitem__(self, key):

        if isinstance(key, sim.core.BaseEntity):
            return key
        else:
            return super().__getitem__(key)


class ProcessReadOnlyDictionary(ReadOnlyDict):
    """Readonly combined dictionary that directly returns the process if it was given as key.
    This allows to get quickly processes either by name or by taking the reference to the process.
    """

    def __getitem__(self, key):

        if isinstance(key, sim.Process):
            return key
        else:
            return super().__getitem__(key)


class Simulation:

    # TODO: make an access only list of objects, scripts and process, and all entities available

    @staticmethod
    def default_config():
        return sim.AttrDict(
            world = None,
            visible_area = ((0., 10.), (0., 10.)),
            object_directories = [],
            component_directories = [],
            script_directories = [],
            process_directories = [],
            objects = [],
            scripts = [],
            processes = [],
            step_length = 1/20, # length of one simulated step in seconds
            max_real_time_factor = 0, # How many simulation steps should be executed per second? 0 to run as fast as possible. 1 for real_time.
            step_epsilon = 1e-04,
        )


    @property
    def visible_area(self):
        r"""Visbible area of the world in meters ((min_x, max_x), (min_y, max_y)).

        This is only the visible area used as default for guis and maps.
        Objects can still exist outside this area.
        """
        return self._visible_area


    @property
    def box2d_world(self):
        return self._box2d_simulation.world


    @property
    def box2d_simulation(self):
        return self._box2d_simulation


    @property
    def step_counter(self):
        """Step counter of the simulation. Is None before simulation starts. Then starts from 0."""
        return self._step_counter


    @property
    def time(self):
        """Get the time of simulation in seconds."""
        return self._time


    @property
    def step_length(self):
        """Get the time of a step in seconds."""
        return self._step_length


    @property
    def has_reset_point(self):
        """Was already a reset point set?"""
        return self._is_reset_point_set


    @property
    def real_time_factor(self):
        """Get the real time factor of the simulation."""
        return self._step_length * self._clock.get_fps()


    def __init__(self, world_config=None, config=None, **kwargs):
        self.config = sim.combine_dicts(kwargs, world_config, config, self.default_config())

        self._visible_area = self.config.visible_area

        if self._visible_area[0][0] >= self._visible_area[0][1] or self._visible_area[1][0] >= self._visible_area[1][1]:
            raise ValueError('The simulation.config.visible_area is not in the correct format!')

        # stores the execution order of the step method for the entities
        self._step_order_list = []

        self._permanent_objects = OrderedDict()
        self._non_permanent_objects = OrderedDict()
        self._removed_permanent_objects = OrderedDict()

        # interface for objects which is readonly
        self.objects = EntityReadOnlyCombinedDictionary([self._permanent_objects, self._non_permanent_objects])

        self._permanent_scripts = OrderedDict()
        self._non_permanent_scripts = OrderedDict()
        self._removed_permanent_scripts = OrderedDict()

        # readonly interface for scripts
        self.scripts = EntityReadOnlyCombinedDictionary([self._permanent_scripts, self._non_permanent_scripts])

        # readonly interface for entities (scripts and objects)
        self.entities = EntityReadOnlyCombinedDictionary([
            self._permanent_objects,
            self._non_permanent_objects,
            self._permanent_scripts,
            self._non_permanent_scripts
        ])

        self._processes = OrderedDict()
        # readonly interface for processes
        self.processes = ProcessReadOnlyDictionary(self._processes)

        self._box2d_simulation = Box2DSimulation(self.config.world)

        self._is_reset_point_set = False

        self._previous_automatic_id = 0

        self._step_counter = None
        self._reset_step_counter = None

        self._time = None
        self._reset_time = None

        self._stop = None

        # variables to control the timing of the simulation and its execution
        self._clock = pygame.time.Clock()
        self._step_length = self.config.step_length

        max_real_time_factor = self.config.max_real_time_factor
        if max_real_time_factor is None or max_real_time_factor < 0. :
            max_real_time_factor = 0

        self._step_rate = max_real_time_factor * 1/self.step_length

        ####################################
        # create objects, scripts and processes from config
        modlib._load_from_directories(self.config)

        for script_descr in self.config.scripts:
            script = modlib.get_script(script_descr.type)(config=script_descr)
            self.add_script(script, id=script.config.id)

        for process_descr in self.config.processes:
            process = modlib.get_process(process_descr.type)(config=process_descr)
            self.add_process(process, name=process.config.name)
        
        for object_descr in self.config.objects:
            obj = modlib.get_object(object_descr.type)(config=object_descr)
            self.add_object(obj, id=obj.config.id)


    def get_free_id(self):
        """Generates a new free id."""

        id = self._previous_automatic_id
        id += 1
        while id in self.entities:
            id += 1
        self._previous_automatic_id = id

        return id


    #######################################################
    # handle order of entities for calling their step and rest method

    def _get_step_order_list_entity_key(self, entity):
        """Generates a unique key to identify the entity in the step_order_list."""

        # identify the key, based on the type of entity
        if isinstance(entity, (sim.Object, sim.Script)):
            key = str(entity.id)
        elif isinstance(entity, sim.Component):
            key = str(entity.object.id) + '_' + entity.name
        elif isinstance(entity, sim.Process):
            key = 'p_' + entity.name
        else:
            raise ValueError('Non-supported entity type!')
        return key


    def _add_entity_to_step_order_list(self, entity):

        key = self._get_step_order_list_entity_key(entity)

        self._step_order_list.append((entity.order, key, entity))
        self._step_order_list.sort(key=itemgetter(0))


    def _remove_entity_from_step_order_list(self, entity):

        key = self._get_step_order_list_entity_key(entity)

        remove_idx = None
        for idx, (_, cur_key, _) in enumerate(self._step_order_list):
            if cur_key == key:
                remove_idx = idx
                break

        if remove_idx is None:
            raise IndexError('Can not find an entry for key {!r} in the _step_order_list!')
        else:
            del(self._step_order_list[remove_idx])

    ##################################################################
    # Adding and removing components to be executed by the step and reset method for mpisim internal use

    def _register_component(self, component):
        self._add_entity_to_step_order_list(component)


    def _unregister_component(self, component):
        self._remove_entity_from_step_order_list(component)


    ##################################################################
    # Adding and removing of objects, scripts and processes

    def add_object(self, obj, id=None):
        """Adds an object or objects to the simulation.

        If an object is added after a reset point is set (using the set_reset_point method or by calling the step or run method the first time),
        it will be removed from the simulation if the reset method is called.

        Args:
            obj (mpi_sim.Object): Object that will be added.
            id (int): ID of the object. If None is given a unique ID will be generated. (default = None)

        Returns:
            ID (int) of the added object.
        """

        # allow to handle list of objects by internally always process lists
        if isinstance(obj, list):
            is_list_input = True
            obj_list = obj
            if id is None:
                id_list = [None] * len(obj_list)
            else:
                id_list = id
        else:
            is_list_input = False
            obj_list = [obj]
            id_list = [id]

        if len(obj_list) != len(id_list):
            raise ValueError('Number if objects and ids must be the same!')

        # create each object in list
        for idx in range(len(obj_list)):

            cur_obj, cur_id = obj_list[idx], id_list[idx]

            if not isinstance(cur_obj, sim.Object):
                raise ValueError('The object must be of type mpi_sim.Object!')

            if cur_id is None:
                cur_id = cur_obj.config.id

            if cur_id is None:
                cur_id = self.get_free_id()
            else:
                if cur_id in self.entities:
                    raise IndexError('An object or script with ID {!r} exists already!', cur_id)

            cur_obj._internal_add(simulation=self, id=cur_id)
            self._non_permanent_objects[cur_id] = cur_obj

            self._add_entity_to_step_order_list(cur_obj)

            id_list[idx] = cur_id

        if not is_list_input:
            return id_list[0]
        else:
            return id_list


    def remove_object(self, obj):
        """Removes an object from the simulation.

        If the object was added before the last reset-point was created, then it will be deactivated, and reactivated if the reset method is called.

        Args:
            obj: Either the id (int) or the object (mpi_sim.Object) that should be removed.
        """

        # get object and object_id
        if isinstance(obj, int):
            obj_id = obj
            obj = self.get_object(obj_id)

            if obj is None:
                raise ValueError('Object with id {!r} does not exist in the simulation!', obj_id)

        elif isinstance(obj, sim.Object):
            obj_id = obj.id

            if not obj == self.objects[obj_id]:
                raise ValueError('The given object is not the same as the object in the simulation with its ID!')

        else:
            raise ValueError('The given object must be an ID (int) or an object (mpi_sim.Object)!')

        self._remove_entity_from_step_order_list(obj)
        for component in obj.components.values():
            self._remove_entity_from_step_order_list(component)

        obj._internal_remove()

        if obj.is_permanent:
            # permanent objects are deactivated and stored in the _removed_objects list, so that they can be reactivated if simulation is reset
            self._permanent_objects.pop(obj_id)
            self._removed_permanent_objects[obj_id] = obj
        else:
            # non-permanent objects are deleted
            obj._internal_close()
            self._non_permanent_objects.pop(obj_id)


    def add_script(self, script, id=None):
        """Adds scripts to the simulation.

        If a script is added after a reset point is set (using the set_reset_point method or by calling the step or run method the first time),
        it will be removed from the simulation if the reset method is called.

        Args:
            script (mpi_sim.Script, list): Script or a list of Scripts.
            id (int, list): ID or list of IDs of the scripts. If None is given a unique ID will be generated. (default = None)

        Returns:
            ID (int) or list of IDs of the added scripts.
        """

        if isinstance(script, list):
            is_list_input = True
            script_list = script
            if id is None:
                id_list = [None] * len(script_list)
            else:
                id_list = id
        else:
            is_list_input = False
            script_list = [script]
            id_list = [id]

        if len(script_list) != len(id_list):
            raise ValueError('Number if scripts and ids must be the same!')

        for idx in range(len(script_list)):

            cur_script, cur_id = script_list[idx], id_list[idx]

            if not isinstance(cur_script, sim.Script):
                raise ValueError('The script must be of type mpi_sim.Script!')

            if cur_id is None:
                cur_id = cur_script.config.id

            if cur_id is None:
                cur_id = self.get_free_id()
            else:
                if cur_id in self.entities:
                    raise ValueError('An object or script with the given ID ({}) exists already!'.format(cur_id))

            cur_script._internal_add(simulation=self, id=cur_id)
            self._non_permanent_scripts[cur_id] = cur_script

            self._add_entity_to_step_order_list(cur_script)

            id_list[idx] = cur_id

        if not is_list_input:
            return id_list[0]
        else:
            return id_list


    def remove_script(self, script):
        """Removes a script from the simulation.

        If the script was added before the last reset-point was created, then it will be deactivated, and reactivated if the reset method is called.

        Args:
            script: Either the id (int) or the script (mpi_sim.Script) that should be removed.
        """

        # get script and script_id
        if isinstance(script, int):
            script_id = script
            script = self.scripts[script_id]

            if script is None:
                raise ValueError('Script with id {!r} does not exist in the simulation!', script_id)

        elif isinstance(script, sim.Script):
            script_id = script.id

            if not script == self.scripts[script_id]:
                raise ValueError('The given script is not the same as the script in the simulation with its ID!')

        else:
            raise ValueError('The given script must be an ID (int) or a script (mpi_sim.Script)!')

        self._remove_entity_from_step_order_list(script)
        script._internal_remove()

        if script.is_permanent:
            # permanent objects are deactivated and stored in the _removed_objects list, so that they can be reactivated if simulation is reset
            self._permanent_scripts.pop(script_id)
            self._removed_permanent_scripts[script_id] = script
        else:
            # non-permanent objects are deleted
            script._internal_close()
            self._non_permanent_scripts.pop(script_id)


    def add_process(self, process, name=None):
        r"""Adds a process to the simulation.

        Args:
            process (Process): Process that will be added.
            name (string): Name that should be given to the process. (optional)
        """

        # TODO: allow to give a list of processes

        if not isinstance(process, sim.Process):
            raise ValueError('Given process must be of type mpi_sim.Process!')

        if name is None:
            name = process.config.name

        if name in self._processes:
            raise ValueError('Process with the name {!r} exists already!', name)

        process._internal_add(simulation=self, name=name)
        self._processes[name] = process

        self._add_entity_to_step_order_list(process)

        return name


    def remove_process(self, process):
        r"""Removes a process.

        Args:
            process: Name or process to remove
        """

        # get process and process_name
        if isinstance(process, str):
            process_name = process
            process = self.get_process(process_name)

            if process is None:
                raise ValueError('Process with name {!r} does not exist in the simulation!', process_name)

        elif isinstance(process, sim.Process):
            process_name = process.name

            if not process == self.processes[process_name]:
                raise ValueError('The given process is not the same as the process in the simulation with its name {!r}!', process_name)

        else:
            raise ValueError('The given process must be a name (string) or a process (mpi_sim.Process)!')

        self._remove_entity_from_step_order_list(process)

        process._internal_close()
        self._processes.pop(process_name)


    def get_processes_by_type(self, type):
        r"""Returns all processes of the given class or tuple of classes.

        Args:
            type: class or list of classes.

        Returns: Processes of the given type or types. None if none is found.
        """
        return sim.utils.get_dict_items_by_type(self.processes, type)


    def get_entities_by_type(self, type):
        r"""Returns all entities of the given class or tuple of classes.

        Args:
            type: class or list of classes.

        Returns: Entities of the given type or types. None if none is found.
        """
        return sim.utils.get_dict_items_by_type(self.entities, type)


    def get_objects_by_type(self, type):
        r"""Returns all objects of the given class or tuple of classes.

        Args:
            type: class or list of classes.

        Returns: Objects of the given type or types. None if none is found.
        """
        return sim.utils.get_dict_items_by_type(self.objects, type)


    def get_scripts_by_type(self, type):
        r"""Returns all scripts of the given class or tuple of classes.

        Args:
            type: class or list of classes.

        Returns: Scripts of the given type or types. None if none is found.
        """
        return sim.utils.get_dict_items_by_type(self.scripts, type)


    def set_reset_point(self):
        r"""Creates a reset point , saving all the important properties of the box2d objects (position, force, ... ). The simulation is reset
        to this point if the reset method is called.
        """

        # save step counter and time counter
        self._reset_step_counter = self._step_counter
        self._reset_time = self._time

        #############
        # objects

        # delete all permanent objects that have been removed
        for obj in self._removed_permanent_objects.values():
            obj._internal_close()
        self._removed_permanent_objects.clear()

        # change status of all non_permanent objects to permanent
        for obj in self._non_permanent_objects.values():
            obj._internal_make_permanent()
            self._permanent_objects[obj.id] = obj
        self._non_permanent_objects.clear()

        #############
        # scripts

        # delete all permanent scripts that have been removed
        for script in self._removed_permanent_scripts.values():
            script._internal_close()
        self._removed_permanent_scripts.clear()

        # change status of all non_permanent scripts to permanent
        for script in self._non_permanent_scripts.values():
            script._internal_make_permanent()
            self._permanent_scripts[script.id] = script
        self._non_permanent_scripts.clear()

        #############
        # box2d simulation world
        self._box2d_simulation.set_reset_point()

        # save the state of all permanent objects
        for obj in self._permanent_objects.values():
            obj._internal_save_reset_state()

        # save the state of all permanent objects
        for script in self._permanent_scripts.values():
            script._internal_save_reset_state()

        self._is_reset_point_set = True


    def reset(self):
        """Resets the simulation to its last reset point. The reset point is either set by the set_reset_point method, or by calling first time
        the step or run method.

        Objects, scripts and processes that have been added after the reset point was created will be deleted from the simulation.
        """

        if not self._is_reset_point_set:
            warnings.warn('Simulation.reset() called before a reset point was set (by calling set_reset_point, step, or run). Therefore, nothing is reset!')
        else:

            self._step_counter = self._reset_step_counter
            self._time = self._reset_time

            ##########
            # objects

            # delete all non-permanent objects
            for obj in self._non_permanent_objects.values():

                # remove all components from the object from the step list
                for component in obj.components.values():
                    self._remove_entity_from_step_order_list(component)

                self._remove_entity_from_step_order_list(obj)
                obj._internal_remove()
                obj._internal_close()

            self._non_permanent_objects.clear()

            # reactivate removed permanent objects
            for obj in self._removed_permanent_objects.values():
                obj._internal_unremove()
                self._permanent_objects[obj.id] = obj
                self._add_entity_to_step_order_list(obj)

                # add all components from the object to the stop list
                for component in obj.components.values():
                    self._add_entity_to_step_order_list(component)

            self._removed_permanent_objects.clear()

            ##########
            # scripts

            # delete all non-permanent scripts
            for script in self._non_permanent_scripts.values():
                self._remove_entity_from_step_order_list(script)
                script._internal_remove()
                script._internal_close()
            self._non_permanent_scripts.clear()

            # reactivate removed permanent objects
            for script in self._removed_permanent_scripts.values():
                script._internal_unremove()
                self._permanent_scripts[script.id] = script
                self._add_entity_to_step_order_list(script)
            self._removed_permanent_scripts.clear()

            # reset all with order less that 100
            last_entity_idx = 0
            for idx, (order, _, entity) in enumerate(self._step_order_list):
                last_entity_idx = idx

                if order > 99:
                    break
                else:
                    entity._internal_reset()

            # the box2d simulation has order 100
            self._box2d_simulation.reset()

            # reset all entities that are larger/equal than 100
            if len(self._step_order_list) - 1 >= last_entity_idx:
                for idx, (order, _, entity) in enumerate(self._step_order_list[last_entity_idx:]):
                    entity._internal_reset()


    def step(self):
        """Runs the simulation for one step.

        Sets a reset point if called first time and no reset point (set_reset_point method) was created before.
        """

        if self._step_counter is None:
            self._step_counter = 0
            self._time = 0.0
        else:
            self._step_counter += 1
            self._time += self._step_length

        # set a initialization point if not already set
        if not self._is_reset_point_set:
            self.set_reset_point()

        # step all with order less that 100
        last_entity_idx = 0
        for idx, (order, _, entity) in enumerate(self._step_order_list):
            last_entity_idx = idx

            if order > 99:
                break
            else:
                entity._internal_step(self._time)

        # call the _physics_simulation_step_preprocessing method of all entities
        for (_, _, entity) in self._step_order_list:
            entity._physics_simulation_step_preprocessing()

        # the box2d simulation has order 100
        self._box2d_simulation.step(self._step_length)

        # call the _physics_simulation_step_postprocessing method of all entities
        for (_, _, entity) in self._step_order_list:
            entity._physics_simulation_step_postprocessing()

        # step all entities that are larger/equal than 100
        if len(self._step_order_list) - 1 >= last_entity_idx:
            for idx, (order, _, entity) in enumerate(self._step_order_list[last_entity_idx:]):
                entity._internal_step(self._time)

        # measures the time between steps which can be used to compute the framerate
        # if self._step_rate > 0, then this method will also sleep() to keep this rate
        self._clock.tick(self._step_rate)


    def run(self, duration=None, n_steps=None, stop_condition=None):
        """Runs the simulation for a certain amount of time, steps or until a stop_condition is fulfilled.

        Sets a reset point if called first time and no reset point (set_reset_point method) was created before.

        Args
            duration (float): Time in seconds. (default = None):
            n_steps (int): Number of steps to run. (default = None)
            stop_condition (function handle): Function that is called after each simulator step and returns True of the simulation should stop.
                The function takes the simulator and the current number of steps as input. (default = None)

        Note: Either the time, n_steps or the stop_condition parameter must be set.
        """

        # set a initialization point if not already set
        if not self._is_reset_point_set:
            self.set_reset_point()

        step = 0

        self._stop = False
        while not self._stop:

            self.step()

            step += 1

            if duration is not None and self._time >= duration:
                self._stop = True
            if n_steps is not None and step >= n_steps:
                self._stop = True
            elif stop_condition is not None and stop_condition(self, step):
                self._stop = True


    def stop(self):
        """Stops a running simulation."""
        self._stop = True


    def close(self):
        """Closes the simulation. Deletes all objects, scripts, and processes."""

        self._stop = True

        self._step_order_list.clear()

        ###########
        # Objects
        for obj in self._non_permanent_objects.values():
            obj._internal_remove()
            obj._internal_close()
        self._non_permanent_objects.clear()

        for obj in self._permanent_objects.values():
            obj._internal_remove()
            obj._internal_close()
        self._permanent_objects.clear()

        for obj in self._removed_permanent_objects.values():
            obj._internal_close()
        self._removed_permanent_objects.clear()

        ###########
        # Scripts
        for script in self._non_permanent_scripts.values():
            script._internal_remove()
            script._internal_close()
        self._non_permanent_scripts.clear()

        for script in self._permanent_scripts.values():
            script._internal_remove()
            script._internal_close()
        self._permanent_scripts.clear()

        for script in self._removed_permanent_scripts.values():
            script._internal_close()
        self._removed_permanent_scripts.clear()

        ###########
        # box2d simulation
        self._box2d_simulation.close()

    def get_objects_in_simulation(self, object_to_avoid=None):
        """Returns the objects in the simulation"""
        objects = []
        # go through objects
        for obj in self.objects.values():
            if object_to_avoid is None :
                # only include objects that have a position property
                if obj.position is not None:
                    objects.append(obj)
            else :
                if type(obj) != type(object_to_avoid):
                    if obj.position is not None:
                        objects.append(obj)
        return objects
    

    def get_nearby_objects(self, point, radius, object_types=None):
        """Returns objects that are within a specified radius of the given point (x,y) or object (obj.position).
        If a object is given as reference point, then it is not included in the list.

        Please note: Only objects that have a position property are taken into account!

        Args:
            point: Either a [x,y] position or an object with a position property for which nearby objects should be detected.
            radius (float): Radius in meters in which other objects should be found.
            object_types:   A tuple/list or a single class of object types (classes) that are regarded.
                            Example: [mpi_sim.objects.Human, mpi_sim.objects.ARIRobot].
                            (Default=None)

        Returns: A list with the nearby objects.
        """

        reference_object = None
        if isinstance(point, sim.Object):
            if hasattr(point, 'position'):
                center = point.position
                reference_object = point
            else:
                ValueError('If the parameter point is an Object then it must have a position property!')
        else:
            center = np.array(point)

        if isinstance(object_types, list):
            object_types = tuple(object_types)

        objects = []
        # go through objects
        for obj in self.objects.values():

            # do not include the reference object, if one was given
            if reference_object is None or reference_object is not obj:

                # only include specific object types if some where specified
                if object_types is None or issubclass(obj.__class__, object_types):

                    # only include objects that have a position property
                    if obj.position is not None:

                        if np.linalg.norm(obj.position - center) <= radius:
                            objects.append(obj)

        return objects
