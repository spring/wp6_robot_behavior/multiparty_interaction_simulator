import mpi_sim as sim
from mpi_sim.core.base_entity import BaseEntity
import copy


class StateEntity(BaseEntity):
    r"""
    Baseclass for entities that have a state that can be reset if the simulation resets (Objects, Components, Scripts).
    Handles the reset of the state.
    Handles also the permanent and removed state of entities.
    """

    @property
    def is_permanent(self):
        return self._is_permanent


    @property
    def is_removed(self):
        return self._is_removed


    def __init__(self, config=None, **kwargs):
        super(StateEntity, self).__init__(config=config, **kwargs)

        self.state = sim.AttrDict()
        self._saved_state = None

        self._is_permanent = None
        self._is_removed = None


    def _internal_add(self, simulation):
        self._is_permanent = False
        self._is_removed = False
        super(StateEntity, self)._internal_add(simulation)


    def _internal_save_reset_state(self):
        self._saved_state = copy.deepcopy(self.state)

        super(StateEntity, self)._internal_save_reset_state()


    def _internal_reset(self):
        self._internal_load_reset_state()

        super(StateEntity, self)._internal_reset()


    def _internal_load_reset_state(self):
        self.state = copy.deepcopy(self._saved_state)

        super(StateEntity, self)._internal_load_reset_state()


    def _internal_make_permanent(self):
        self._is_permanent = True
        self._make_permanent()


    def _make_permanent(self):
        """Called if the entity is made permanent in the simulation."""
        pass


    def _internal_remove(self):
        self._is_removed = True
        self._remove()


    def _remove(self):
        """Called if the entity is removed from the simulation."""
        pass


    def _internal_unremove(self):
        self._is_removed = False
        self._unremove()


    def _unremove(self):
        """Called if the entity is unremoved from the simulation."""
        pass


    def _internal_close(self):
        super(StateEntity, self)._internal_close()
        self._is_permanent = None
        self._is_removed = True


    def __deepcopy__(self, memo):
        """Do not do deepcopy scripts and objects, to avoid that references to scripts and objects in their states create deepcopies of them when setting a reset state."""
        return self