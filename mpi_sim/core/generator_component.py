from mpi_sim.core.component import Component


class GeneratorComponent(Component):

    @staticmethod
    def default_config():
        dc = Component.default_config()
        dc.order = 160
        return dc
