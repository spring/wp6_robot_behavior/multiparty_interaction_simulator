import mpi_sim as sim
from mpi_sim.core.state_entity import StateEntity
import mpi_sim.core.module_library as modlib
from Box2D import b2Transform, b2Body


class Object(StateEntity):

    @staticmethod
    def default_config():
        dc = StateEntity.default_config()
        dc.components = [] # list of component descriptions
        dc.id = None
        dc.properties = []  # list of properties such as, if the object is 'static' or 'dynamic' which is used by the mapping
        return dc


    @property
    def id(self):
        return self._id


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        # set when added to the simulation
        self._id = None

        self.box2d_bodies = []
        self._box2d_bodies_reset_states = []

        self.properties = self.config.properties

        self.components = sim.utils.ReadOnlyDict()

        # add components automatically that have been defined in the configuration
        for component_descr in self.config.components:
            if isinstance(component_descr, sim.Component):
                self.add_component(component_descr, name=component_descr.config.name)
            else:
                component = modlib.get_component(component_descr.type)(config=component_descr)
                self.add_component(component, name=component.config.name)


    def add_component(self, component, name=None, order=None):
        r"""Add a component to the object.

        Args:
            component (mpi_sim.Component): Component.
            name (str): Optional name of the component. If None, the standard name of the component is used (component.name) which is usually the
                        class name of the component.
            order (int): Optional order that defines when the component is executed during a step. If none, then the default order is used.

        Returns:
            Name (str) of the component.

        """

        if not isinstance(component, sim.Component):
            raise ValueError('The component must be of type mpi_sim.Component!')

        if name is None:
            name = component.config.name

        if self.exist_component(name):
            raise ValueError('Component with the name {!r} exists already!'.format(name))

        component._internal_add_to_object(obj=self, name=name, order=order)

        self.components._data[name] = component

        if self.simulation is not None:
            component._internal_add_to_simulation(simulation=self.simulation)

        return name


    def exist_component(self, component):

        if isinstance(component, str):
            name = component
        elif isinstance(component, type):
            # class
            name = component.__name__
        elif isinstance(component, sim.Component):
            name = component.name
        else:
            raise ValueError('The given process must be a name (string), class, or a component (mpi_sim.Component)!')

        return name in self.components


    def get_component(self, name):
        return self.components[name]


    def __getattr__(self, name):
        """ Returns component with given name."""
        try:
            # Throws exception if not in prototype chain
            return object.__getattribute__(self, name)
        except AttributeError:
            try:
                return self.components[name]
            except KeyError:
                raise AttributeError(name)


    def _internal_add(self, simulation, id):
        self._id = id
        super(Object, self)._internal_add(simulation)

        self._internal_create_in_box2d_world()

        # add all components to simulation
        for component in self.components.values():
            component._internal_add_to_simulation(simulation=simulation)


    def _internal_create_in_box2d_world(self):
        """Internal method to called by the Simulation which calls the Objects _add_to_simulation method."""

        bodies = self._create_in_box2d_world(self._simulation.box2d_world)

        if isinstance(bodies, tuple):
            self.box2d_bodies = list(bodies)
        elif isinstance(bodies, list):
            self.box2d_bodies = bodies
        elif isinstance(bodies, b2Body):
            self.box2d_bodies = [bodies]
        elif bodies is None:
            self.box2d_bodies = []
        else:
            raise ValueError('_create_in_box2d_world() must return a tuple/list of b2Body, a b2Body, or None!')

        # add a reference to the object to each body
        for body in self.box2d_bodies:
            if body.userData is None:
                body.userData = dict()
            elif not isinstance(body.userData, dict):
                raise ValueError('The userData property of each b2Body must be either None or a dictionary!')

            if 'object' in body.userData:
                raise ValueError('The userData dictionary of b2Body\'s can not have an \'object\' element!')

            body.userData['object'] = self


    def _create_in_box2d_world(self, box2d_world):
        r"""Should be overridden to create box2d bodies, joints, and fixtures to the simulation world.
        The function has to return a list of all bodies that have been created.
        """
        pass


    def _internal_save_reset_state(self):

        # save the state of all components
        for component in self.components.values():
            component._internal_save_reset_state()

        # save the state of all bodies
        self._box2d_bodies_reset_states = []
        for body in self.box2d_bodies:
            body_state = sim.AttrDict()

            # Save all necessary body properties
            body_state.position = body.position.copy()
            body_state.angle = body.angle
            body_state.transform = b2Transform()
            body_state.transform.Set(body.transform.position, body.transform.angle)
            body_state.awake = body.awake
            body_state.active = body.active
            body_state.linearVelocity = body.linearVelocity.copy()
            body_state.angularVelocity = body.angularVelocity

            self._box2d_bodies_reset_states.append(body_state)

        super(Object, self)._internal_save_reset_state()


    def _internal_reset(self):
        super(Object, self)._internal_reset()


    def _internal_load_reset_state(self):

        # load the state of all bodies
        for body_idx, body_state in enumerate(self._box2d_bodies_reset_states):

            # load all necessary body properties
            self.box2d_bodies[body_idx].position = body_state.position.copy()
            self.box2d_bodies[body_idx].angle = body_state.angle
            self.box2d_bodies[body_idx].transform.Set(body_state.transform.position, body_state.transform.angle)
            self.box2d_bodies[body_idx].awake = body_state.awake
            self.box2d_bodies[body_idx].active = body_state.active
            self.box2d_bodies[body_idx].linearVelocity = body_state.linearVelocity.copy()
            self.box2d_bodies[body_idx].angularVelocity = body_state.angularVelocity

        # save the state of all components
        for component in self.components.values():
            component._internal_load_reset_state()

        super(Object, self)._internal_load_reset_state()


    def _internal_remove(self):

        self._internal_deactivate_in_box2d_world()
        super(Object, self)._internal_remove()

        for component in self.components.values():
            component._internal_remove()


    def _internal_deactivate_in_box2d_world(self):

        for box2d_body in self.box2d_bodies:
            box2d_body.active = False

        self._deactivate_in_box2d_world()


    def _deactivate_in_box2d_world(self):
        pass


    def _internal_unremove(self):

        self._internal_activate_in_box2d_world()
        super(Object, self)._internal_unremove()

        for component in self.components.values():
            component._internal_unremove()


    def _internal_activate_in_box2d_world(self):

        for box2d_body in self.box2d_bodies:
            box2d_body.active = True

        self._activate_in_box2d_world()


    def _activate_in_box2d_world(self):
        pass


    def _internal_close(self):
        self._internal_destroy_in_box2d_world()
        super(Object, self)._internal_close()

        for component in self.components.values():
            component._internal_close()

        self._id = None


    def _internal_destroy_in_box2d_world(self):
        """Internal Method. Destroys all bodies that are in self.box2d_bodies and calls self._destroy_in_box2d_world method."""

        # destroy all bodies
        for box2d_body in self.box2d_bodies:
            self.simulation.box2d_world.DestroyBody(box2d_body)
        self.box2d_bodies.clear()

        self._destroy_in_box2d_world(self.simulation.box2d_world)


    def _destroy_in_box2d_world(self, box2d_world):
        """Should be overridden to destroy custom box2d bodies from the simulation world.

        Note: Bodies that have been added to self.box2d_bodies have been already destroyed.
        """
        pass
