import mpi_sim as sim
import Box2D
from Box2D import b2ContactListener, b2GetPointStates, b2DestructionListener, b2Fixture, b2Joint
from time import time


class fwDestructionListener(b2DestructionListener):
    """
    The destruction listener callback:
    "SayGoodbye" is called when a joint or shape is deleted.
    """

    def __init__(self, test, **kwargs):
        super(fwDestructionListener, self).__init__(**kwargs)
        self.test = test

    def SayGoodbye(self, obj):
        if isinstance(obj, b2Joint):
            self.test.JointDestroyed(obj)
        elif isinstance(obj, b2Fixture):
            self.test.FixtureDestroyed(obj)



class Box2DSimulation(b2ContactListener):

    @staticmethod
    def default_config():
        dc = sim.AttrDict()
        # size of the world in meters ([[x_min, x_max], [y_min, y_max]])
        dc.vel_iters = 8
        dc.pos_iters = 3
        dc.timestep = 1/20

        # Miscellaneous testbed options
        dc.pause = False
        dc.singleStep = False

        # Makes physics results more accurate (see Box2D wiki)
        dc.enable_warm_starting = True
        dc.enable_continuous = True     # Calculate time of impact
        dc.enable_sub_stepping = False

        dc.maxContactPoints = 100

        dc.world = sim.AttrDict(
            gravity=(0, 0),
            doSleep=True
        )

        return dc


    def __init__(self, config=None, **kwargs):
        self.config = sim.combine_dicts(kwargs, config, self.default_config())

        # TODO (Alex): create the world
        super().__init__()

        self.reset()

        self.world = Box2D.b2World(**self.config.world)

        self.destructionListener = fwDestructionListener(test=self)
        self.world.destructionListener = self.destructionListener
        self.world.contactListener = self

        # TODO: is a ground body needed ?
        self.groundbody = self.world.CreateBody(userData={'name': 'ground'})


    def set_reset_point(self):
        pass


    def reset(self):
        self.points = []
        self.using_contacts = False
        self.n_steps = 0


    def step(self, step_length):
        self.n_steps += 1

        cur_timestep = self.config.timestep
        if self.config.pause:
            if self.config.singleStep:
                self.config.singleStep = False
            else:
                cur_timestep = 0.0
        
        # Set the other settings that aren't contained in the flags
        self.world.warmStarting = self.config.enable_warm_starting
        self.world.continuousPhysics = self.config.enable_continuous
        self.world.subStepping = self.config.enable_sub_stepping

        # Reset the collision points
        self.points = []

        # Tell Box2D to step
        self.t_step = time()
        self.world.Step(cur_timestep, self.config.vel_iters, self.config.pos_iters)
        self.world.ClearForces()
        self.t_step = time() - self.t_step


    def close(self):
        self.world.contactListener = None
        self.world.destructionListener = None
        self.world.renderer = None

    
    def PreSolve(self, contact, old_manifold):
        """
        This is a critical function when there are many contacts in the world.
        It should be optimized as much as possible.
        """
        # if not (self.config.drawContactPoints or
        #         self.config.drawContactNormals or self.using_contacts):
        #     return
        if len(self.points) > self.config.maxContactPoints:
            return

        manifold = contact.manifold
        if manifold.pointCount == 0:
            return

        state1, state2 = b2GetPointStates(old_manifold, manifold)
        if not state2:
            return

        worldManifold = contact.worldManifold

        # TODO: find some way to speed all of this up.
        self.points.extend(
            [dict(
                fixtureA=contact.fixtureA,
                fixtureB=contact.fixtureB,
                position=worldManifold.points[i],
                normal=worldManifold.normal.copy(),
                state=state2[i],)
                for i, point in enumerate(state2)
            ]
        )


    def FixtureDestroyed(self, fixture):
        """
        Callback indicating 'fixture' has been destroyed.
        """
        pass


    def JointDestroyed(self, joint):
        """
        Callback indicating 'joint' has been destroyed.
        """
        pass


    def get_bodies(self, object_types=None, object_ids=None, object_properties=None, search_operator='and'):
        r"""Returns bodies which objects have a certain type, id, or property.

        TODO: detailed description
        """

        if not search_operator.lower() in ['and', 'or']:
            raise ValueError('Parameter \'operator\' must be either \'and\' or \'or\'.')
        is_or_operator = (search_operator.lower() == 'or')

        # ensure inputs are tuples
        def as_tuple(val):
            if val is None:
                return tuple()
            if isinstance(val, list):
                return tuple(val)
            if not isinstance(val, tuple):
                return tuple([val])
            return val

        object_types = as_tuple(object_types)
        object_ids = as_tuple(object_ids)
        object_properties = as_tuple(object_properties)

        # return all bodies if no search conditions are given
        if not object_types and not object_ids and not object_properties:
            return self.world.bodies

        bodies = []
        for body in self.world.bodies:

            if 'object' in body.userData:
                obj = body.userData['object']

                if is_or_operator:

                    if obj.id in object_ids:
                        bodies.append(body)

                    elif isinstance(obj, object_types):
                        bodies.append(body)

                    else:
                        for search_prop in object_properties:
                            if search_prop in obj.properties:
                                bodies.append(body)
                                break

                else:  # search operator is and

                    # check each search condition, ignore conditions if the user did not define them (for example: not object_ids)
                    if not object_ids or obj.id in object_ids:
                        if not object_types or isinstance(obj, object_types):

                            if not object_properties:
                                # no search condition for properties, so do not take it into account
                                bodies.append(body)
                            else:
                                for search_prop in object_properties:
                                    if search_prop in obj.properties:
                                        bodies.append(body)
                                        break

        return bodies
