from mpi_sim.core.component import Component


class SensorComponent(Component):

    @staticmethod
    def default_config():
        dc = Component.default_config()
        dc.order = 120
        return dc
