from mpi_sim.core.state_entity import StateEntity


class Script(StateEntity):

    @staticmethod
    def default_config():
        dc = StateEntity.default_config()
        dc.objects = []
        dc.scripts = []
        dc.processes = []
        dc.id = None
        return dc


    @property
    def id(self):
        return self._id


    def __init__(self, config=None, **kwargs):
        super().__init__(config, **kwargs)

        # will be set when added to the simulation
        self._id = None

        # TODO: add the objects, scripts and processe that are defined in the config automatically to the simulation


    def _internal_add(self, simulation, id):
        self._id = id
        super(Script, self)._internal_add(simulation)


    def _internal_close(self):
        super(Script, self)._internal_close()
        self._id = None