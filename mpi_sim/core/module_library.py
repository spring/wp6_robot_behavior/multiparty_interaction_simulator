import mpi_sim
import os
from glob import glob
import importlib
import inspect


class ModuleLibrary:
    """Library of python modules that can be accessed via their names as strings."""

    # TODO: allow to load subdirectories

    def __init__(self):
        self.objects = dict()
        self.objects['ARIRobot'] = mpi_sim.objects.ARIRobot
        self.objects['Bench'] = mpi_sim.objects.Bench
        self.objects['Chair'] = mpi_sim.objects.Chair
        self.objects['Human'] = mpi_sim.objects.Human
        self.objects['RoundTable'] = mpi_sim.objects.RoundTable
        self.objects['Wall'] = mpi_sim.objects.Wall

        self.components = dict()
        if any(class_name == 'ARINavigation' for (class_name,_) in inspect.getmembers(mpi_sim.components)):
            self.components['ARINavigation'] = mpi_sim.components.ARINavigation
        self.components['ObjectDetector'] = mpi_sim.components.ObjectDetector
        self.components['OccupancyGridMapping'] = mpi_sim.components.OccupancyGridMapping
        self.components['SocialForceNavigation'] = mpi_sim.components.SocialForceNavigation
        self.components['SpeechGenerator'] = mpi_sim.components.SpeechGenerator

        self.scripts = dict()
        self.scripts['GroupNavigation'] = mpi_sim.scripts.GroupNavigation

        self.processes = dict()
        self.processes['GUI'] = mpi_sim.processes.GUI


    def load_from_directories(self, config):

        if 'object_directories' in config:
            self._load_modules_to_dict(self.objects, config.object_directories, mpi_sim.Object)

        if 'component_directories' in config:
            self._load_modules_to_dict(self.components, config.component_directories, mpi_sim.Component)

        if 'script_directories' in config:
            self._load_modules_to_dict(self.scripts, config.script_directories, mpi_sim.Script)

        if 'process_directories' in config:
            self._load_modules_to_dict(self.processes, config.process_directories, mpi_sim.Process)


    @staticmethod
    def _load_modules_to_dict(dic, directories, type):

        for directory in directories:

            for file in glob(os.path.join(directory, "*.py")):
                name = os.path.splitext(os.path.basename(file))[0]

                # Ignore __ files
                if not name.startswith("__"):

                    spec = importlib.util.spec_from_file_location('X', file)
                    module = importlib.util.module_from_spec(spec)
                    spec.loader.exec_module(module)

                    for member in dir(module):
                        handler_class = getattr(module, member)

                        if handler_class and inspect.isclass(handler_class):

                            if issubclass(handler_class, type):
                                class_name = handler_class.__name__
                                if class_name in dic:
                                    raise ValueError('Class with name {!r} was already loaded!')
                                dic[class_name] = handler_class


# create the global module library
_module_library = None


def _create_mobule_library_if_not_exist():
    global _module_library
    if _module_library is None:
        _module_library = ModuleLibrary()


def _load_from_directories(config):
    _create_mobule_library_if_not_exist()
    _module_library.load_from_directories(config)


def get_object(obj):
    _create_mobule_library_if_not_exist()

    if isinstance(obj, type):
        # if class itself, return it
        return obj
    else:
        return _module_library.objects[obj]


def get_component(component):
    _create_mobule_library_if_not_exist()
    if isinstance(component, type):
        # if class itself, return it
        return component
    else:
        return _module_library.components[component]


def get_script(script):
    _create_mobule_library_if_not_exist()
    if isinstance(script, type):
        # if class itself, return it
        return script
    else:
        return _module_library.scripts[script]


def get_process(process):
    _create_mobule_library_if_not_exist()
    if isinstance(process, type):
        # if class itself, return it
        return process
    else:
        return _module_library.processes[process]
