import mpi_sim as sim
import numpy as np

class BaseEntity:
    r"""
    Baseclass for all simulation entities (Objects, Components, Scripts, Processes).
    Handles their order and timing of execution, and the reference to the simulation.
    """

    @staticmethod
    def default_config():
        return sim.AttrDict(
            order=200,
            execution_rate=20 #rate of execution steps in Hz (#executions per second)
        )


    @property
    def simulation(self):
        return self._simulation


    @property
    def order(self):
        return self._order


    @property
    def timepoint_of_last_execution(self):
        return self._timepoint_of_last_execution


    @property
    def execution_rate(self):
        """Get rate of execution steps in Hz (#executions per second)."""
        return self._execution_rate


    def __init__(self, config=None, **kwargs):
        self.config = sim.combine_dicts(kwargs, config, self.default_config(), copy_mode='copy')

        self._simulation = None

        if self.config.order is None:
            raise ValueError('The order of a simulation entity must to be set!')
        self._order = self.config.order

        self._execution_rate = self.config.execution_rate
        self._execution_length = 1/self._execution_rate

        self._timepoint_of_last_execution = -np.inf


    def _internal_add(self, simulation):
        self._simulation = simulation

        self._step_epsilon = self._simulation.config.step_epsilon

        self._add()


    def _add(self):
        r"""Called of the entity is added to the simulation."""
        pass


    def _internal_save_reset_state(self):
        self._save_reset_state()


    def _save_reset_state(self):
        pass


    def _internal_reset(self):

        # set last execution timepoint to neg infty to execute in the first step
        self._timepoint_of_last_execution = -np.inf

        self._reset()


    def _reset(self):
        pass


    def _internal_load_reset_state(self):
        self._load_reset_state()


    def _load_reset_state(self):
        pass


    def _internal_step(self, simulation_time):

        if (simulation_time - self._timepoint_of_last_execution) >= (self._execution_length - self._step_epsilon):
            self._step()
            self._timepoint_of_last_execution = simulation_time


    def _physics_simulation_step_preprocessing(self):
        r"""Called directly before the physical simulation was stepped. This is not called in a certain order!"""
        pass


    def _physics_simulation_step_postprocessing(self):
        r"""Called directly after the physical simulation was stepped. This is not called in a certain order!"""
        pass

    def _step(self):
        pass


    def _internal_close(self):
        self._close()
        self._simulation = None


    def _close(self):
        """Called if the entity is deleted."""
        pass