from mpi_sim.core.base_entity import BaseEntity


class Process(BaseEntity):

    @staticmethod
    def default_config():
        dc = BaseEntity.default_config()
        dc.name = None
        dc.order = 220
        return dc


    @property
    def name(self):
        return self._name


    def __init__(self, config=None, **kwargs):
        super(Process, self).__init__(config=config, **kwargs)

        # will be set when added to the simulation
        self._name = None

        # default name is the name of the class of the process
        if self.config.name is None:
            self.config.name = type(self).__name__


    def _internal_add(self, simulation, name):
        self._name = name
        super(Process, self)._internal_add(simulation)


    def _internal_close(self):
        super(Process, self)._internal_close()
        self._name = None