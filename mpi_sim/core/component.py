import mpi_sim as sim


class Component(sim.core.StateEntity):

    @staticmethod
    def default_config():
        dc = sim.core.StateEntity.default_config()
        dc.name = None
        dc.order = 140
        return dc


    @property
    def name(self):
        return self._name


    @property
    def object(self):
        return self._object


    def __init__(self, config=None, **kwargs):
        super(Component, self).__init__(config=config, **kwargs)

        self._object = None

        # default name is the name of the class
        if self.config.name is None:
            self.config.name = type(self).__name__


    def _internal_add_to_object(self, obj, name, order=None):
        self._object = obj
        self._name = name

        if order is None:
            if self.config.order is None:
                raise ValueError('The order of a simulation entity has to be set!')

            self._order = self.config.order
        else:
            self._order = order

        self._add_to_object()


    def _add_to_object(self):
        pass


    def _internal_add_to_simulation(self, simulation):
        super(Component, self)._internal_add(simulation)

        # register the component in the simulation, so that its step and reset methods are called
        self._simulation._register_component(self)

        self._add_to_simulation()


    def _add_to_simulation(self):
        pass


    def _internal_close(self):
        super(Component, self)._internal_close()
        self._name = None
        self._object = None


