import mpi_sim
from mpi_sim.core.generator_component import GeneratorComponent
import numpy as np


class SocialForceNavigation(GeneratorComponent):

    @staticmethod
    def default_config():
        dc = GeneratorComponent.default_config()
        dc.personal_distance = 1.5
        dc.public_distance = 3.0
        dc.max_movement_force = 0.25
        dc.max_rotation_force = 6.
        dc.goal_distance_threshold = 0.2
        dc.step_length_coeff = 1
        dc.avoid_ari = True
        return dc

    ###############################################################################

    @property
    def goal_position(self):
        """The goal position of the human. None if none."""
        return self.state.goal_position


    @goal_position.setter
    def goal_position(self, goal_position):
        """Sets a goal position (x,y) that the agent tries to reach."""
        self.state.goal_position = goal_position


    @property
    def goal_orientation(self):
        """The goal position of the human. None if none."""
        return self.state.goal_orientation


    @goal_orientation.setter
    def goal_orientation(self, goal_orientation):
        """Sets a goal orientation (float in [-pi,pi]) that the agent tries to reach."""
        self.state.goal_orientation = goal_orientation

    ###############################################################################

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        # goal position and orientation for the navigation
        self.state.goal_position = None
        self.state.goal_orientation = None


    def stop(self):
        r"""Stops the navigation."""
        self.state.goal_position = None
        self.state.goal_orientation = None


    def _step(self):
        # step the agent, i.e. let it walk towards its goal_position, goal_orientation

        # print('stats : ', self.box2d_body.inertia, self.box2d_body.awake, self.box2d_body.active, self.box2d_body.transform)
        # print(self.box2d_body.localCenter, self.box2d_body.worldCenter, self.box2d_body.linearVelocity, self.box2d_body.angularVelocity)
        # if goal position is given use it to update the human
        if self.state.goal_position is not None:

            # calc goal and repulsion force
            goal_force = self._calc_goal_force()
            repulsion_force = self._calc_repulsion_force()

            # combine both forces to update the position
            next_pos = self.object.position + (goal_force + repulsion_force) * self.config.step_length_coeff * self.simulation.step_length

            self.object.position = next_pos

        # if goal orientation is given use it to update the human
        if self.state.goal_orientation is not None:

            f_orientation = None
            if self.state.goal_position:
                goal_distance = np.linalg.norm(self.goal_position - self.object.position)

                if goal_distance > self.config.goal_distance_threshold:
                    f_orientation = mpi_sim.utils.measure_relative_angle(self.object, self.goal_position)

            if f_orientation is None:
                f_orientation = mpi_sim.utils.angle_difference(self.object.orientation, self.state.goal_orientation)

            # combine both forces to update the position
            next_orientation = self.object.orientation + f_orientation * self.config.step_length_coeff * self.simulation.step_length

            self.object.orientation = next_orientation
        # TODO: include manual control if wanted

        # if self.state.goal_position is not None or self.state.goal_orientation is not None:
            #     # apply a force to let the body move
            #     # self.box2d_body.ApplyForce(force=(100, 100), point=(0.0, 0.0), wake=True)
            #     self.box2d_body.awake = True
            #     lin_vel, ang_vel = 0., 0.
            #
            #     self.box2d_body.linearVelocity = update_drive(self.keys, lin_vel, mobile_obj=self.box2d_body, max_speed=self.config.max_lin_vel)
            #     self.box2d_body.angularVelocity = update_turn(self.keys, ang_vel, max_speed=self.config.max_ang_vel)


    def _calc_repulsion_force(self, position=None):

        if position is None:
            position = self.object.position

        f_repulsion = position * 0.0

        # identify other agents inside the personal distance of the agent
        if self.config.avoid_ari :
            agents = self.simulation.get_nearby_objects(self.object, self.config.personal_distance, (mpi_sim.objects.Human, mpi_sim.objects.ARIRobot))
        else :
            agents = self.simulation.get_nearby_objects(self.object, self.config.personal_distance, mpi_sim.objects.Human)

        if agents:
            other_agents_pos = np.array([agent.position for agent in agents])  # [n_agent * 2 (x and y pos)] array
            vec_from_other_agents = other_agents_pos - position

            # identify smallest distance d_min to another agent
            distance_to_other_agents = np.linalg.norm(vec_from_other_agents, axis=1)
            d_min = np.min(distance_to_other_agents)

            # compute repulsion force
            R = np.sum(vec_from_other_agents, axis=0)
            f_repulsion = -(self.config.personal_distance - d_min) ** 2 * R / np.linalg.norm(R)
        # print("FORCE AT REPULSIVE:", f_repulsion)
        return f_repulsion


    def _calc_goal_force(self, position=None):

        if position is None:
            position = self.object.position

        goal_direction = self.goal_position - position
        goal_distance = np.linalg.norm(goal_direction)
        np.seterr(invalid='ignore')
        f_goal = min(1.0, goal_distance / self.config.goal_distance_threshold) * goal_direction/goal_distance

        return f_goal
