## Software gym_box2d_diff_drive
##
## Derived from pybox2d distributed under zlib License (c), 2008-2011 Ken Lauer
## sirkne at gmail dot com
##
## This file, corresponding to gym_box2d_diff_drive module of WP6, is part of a
## project that has received funding from the European Union’s Horizon 2020
## research and innovation programme under grant agreement No 871245.
##
## Copyright (C) 2022 by INRIA
## Authors : Chris Reinke, Alex Auternaud, Victor Sanchez
## alex dot auternaud at inria dot fr
## victor dot sanchez at inria dot fr

from mpi_sim.core.component import Component
import mpi_sim as sim
from Box2D import (b2Color, b2Vec2)
import pygame

class PointDrawer(Component):

    @staticmethod
    def default_config():
        dc = sim.SensorComponent.default_config()
        dc.name = 'point_drawer'
        return dc

    
    def add_point_to_draw(self, point):
        self.list_of_point_to_draw.append(point)
    
    @property
    def clear_list_of_point(self):
        self.list_of_point_to_draw.clear()

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)
        self.list_of_point_to_draw =[]            
   
    def _step(self):
        pass 
        # for point in self.list_of_point_to_draw :
        #     self.simulation.box2d_world.renderer.DrawPoint(b2Vec2(point[0],point[1]), 100.0, b2Color(1, 1, 1))
        # pygame.display.flip()
        
        
        

        
