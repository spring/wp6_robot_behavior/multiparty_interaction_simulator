import mpi_sim as sim
from mpi_sim.core.sensor_component import SensorComponent


class EmotionSensor(SensorComponent):

    @staticmethod
    def default_config():
        dc = SensorComponent.default_config()

        dc.default_emotion = 'neutral'

        # emotion (string)
        dc.sensor_pr_model = dict(
            neutral = dict(neutral=0.95, happy=0.01, fear=0.01, disgust=0.01, tired=0.01, sleepy=0.01),
            happy   = dict(neutral=0.01, happy=0.95, fear=0.01, disgust=0.01, tired=0.01, sleepy=0.01),
            fear    = dict(neutral=0.01, happy=0.01, fear=0.95, disgust=0.01, tired=0.01, sleepy=0.01),
            disgust = dict(neutral=0.01, happy=0.01, fear=0.01, disgust=0.95, tired=0.01, sleepy=0.01),
            tired   = dict(neutral=0.01, happy=0.01, fear=0.01, disgust=0.01, tired=0.95, sleepy=0.01),
            sleepy  = dict(neutral=0.01, happy=0.01, fear=0.01, disgust=0.01, tired=0.01, sleepy=0.95),
        )

        return dc


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self._emotion_history = dict()


    def _step(self):

        # erase the history
        self._emotion_history = dict()


    def get_emotion(self, person):
        """Get the emotion of a person that has a EmotionGenerator"""

        if isinstance(person, int):
            person = self.simulation.get_object(person)

        if person.id in self._emotion_history:
            # if the emotion was already sensed, then return it
            sensed_emotion = self._emotion_history[person.id]
        else:
            sensed_emotion = self.config.default_emotion

            emotion_generator = person.get_component(sim.Component.EmotionGenerator)

            emotion = emotion_generator.emotion

            if emotion in self.config.sensor_pr_model:
                emotion_pr_model = self.config.sensor_pr_model[emotion]

                # TODO: generate sensed emotion from the model

                # # TODO: take distance, viewing angle to the person and the viewing angle of the person to the agent into account
                # # get distance to person
                # sim.utils.get_distance(self.object, person)
                #
                # # get viewing angle to person
                # sim.measure_angle(self.object, person)
                #
                # # get viewing angle of person
                # sim.measure_angle(person, self.object)

            self._emotion_history[person.id] = sensed_emotion

        return sensed_emotion


