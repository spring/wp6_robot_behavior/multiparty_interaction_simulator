import mpi_sim
from mpi_sim.core.sensor_component import SensorComponent
import numpy as np
from Box2D import b2Vec2, b2RayCastCallback


class RayCastClosestCallback(b2RayCastCallback):
    """This callback finds the closest hit"""

    def __init__(self, ignore_objects_filter, **kwargs):
        b2RayCastCallback.__init__(self, **kwargs)

        self.ignore_objects_filter = ignore_objects_filter

        self.detected_fixture = None
        self.detected_point = None
        self.detected_normal = None


    def ReportFixture(self, fixture, point, normal, fraction):
        """
        Called for each fixture found in the query. You control how the ray
        proceeds by returning a float that indicates the fractional length of
        the ray. By returning 0, you set the ray length to zero. By returning
        the current fraction, you proceed to find the closest point. By
        returning 1, you continue with the original ray clipping. By returning
        -1, you will filter out the current fixture (the ray will not hit it).
        """

        # ignore the sensor object if one is defined
        if mpi_sim.utils.object_filter(fixture.body.userData['object'], **self.ignore_objects_filter):
            return -1

        self.detected_fixture = fixture
        self.detected_point = b2Vec2(point)
        self.detected_normal = b2Vec2(normal)

        return fraction


class ObjectDetector(SensorComponent):
    r"""SensorComponent that simulates a visual object detector based on a camera or similar senor.


    sensor_body: Body that represents the sensor that is doing the detection, such as a camera or lidar scanner.

    """

    @staticmethod
    def default_config():
        dc = SensorComponent.default_config()
        dc.name = 'object_detector'
        dc.sensor_body = None
        dc.object_filter = None   # which objects can be detected
        dc.fov = 2 * np.pi  # field of view in rad
        dc.max_distance = None  # possible distance until which objects are perceived. If None, then no max depth exists
        dc.ignore_objects_filter = dict(
            types=[],
            ids=[],
            objects=[],
            properties=[],
        )  # list of which should be ignored, even if they are in the path of a target object
        dc.ignore_parent_object = True  # should other bodies and fixtures of the object that has the sensor be ignored?
        return dc


    @property
    def detected_objects(self):
        r"""Get a list of the detected objects."""
        return self.state.detected_objects


    @property
    def detected_object_ids(self):
        r"""Get a list of the ID's of detected objects."""
        return self.state.detected_object_ids


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        if self.config.object_filter is None or len(self.config.object_filter) == 0:
            raise ValueError('The object_filter configuration of the ObjectDetector must be set!')

        if self.config.sensor_body is None:
            self._sensor_body = None

        self.state.detected_objects = []
        self.state.detected_object_ids = []

        # ignore objects should be a dict
        self._ignore_objects_filter = self.config.ignore_objects_filter
        if self._ignore_objects_filter is None:
            self._ignore_objects_filter = dict()


    def _add_to_object(self):
        # add parent object to list of objects to ignore
        if self.config.ignore_parent_object:
            if 'objects' not in self._ignore_objects_filter:
                self._ignore_objects_filter['objects'] = []
            if not isinstance(self._ignore_objects_filter['objects'], list):
                self._ignore_objects_filter['objects'] = [self._ignore_objects_filter['objects']]
            if self.object not in self._ignore_objects_filter['objects']:
                self._ignore_objects_filter['objects'].append(self.object)


    def _step(self):
        self.detect()


    def detect(self):

        # if no sensor body was given, then use the body of the parent object
        if self._sensor_body is None and self.object.box2d_bodies:
            self._sensor_body = self.object.box2d_bodies[0]

        sensor_body_coordinates = (self._sensor_body.position[0], self._sensor_body.position[1], self._sensor_body.angle)

        self.state.detected_objects = []
        self.state.detected_object_ids = []

        # go over all bodies that fit the filter
        bodies = self.simulation.box2d_simulation.get_bodies(
            object_types=self.config.object_filter.get('types', None),
            object_ids=self.config.object_filter.get('ids', None),
            object_properties=self.config.object_filter.get('properties', None),
        )
        for body in bodies:

            sim_object = body.userData['object']

            # skip the body if its object was already detected
            if sim_object.id in self.state.detected_object_ids:
                continue

            # skip the body if it is too far away
            if self.config.max_distance and not self.config.max_distance == np.inf:
                if mpi_sim.utils.measure_center_distance(self._sensor_body, body) > self.config.max_distance:
                    continue

            # skip the body if it is not in the field of view
            angle_to_sensor = mpi_sim.utils.measure_relative_angle(sensor_body_coordinates, body.position)
            if abs(angle_to_sensor) > self.config.fov / 2:
                continue

            # check if a direct line of sight exist to the body using a raycast
            point1 = self._sensor_body.position
            point2 = body.position

            raycast_callback = RayCastClosestCallback(ignore_objects_filter=self._ignore_objects_filter)
            self.simulation.box2d_world.RayCast(raycast_callback, point1=point1, point2=point2)

            # skip body if there is no line of sight
            if raycast_callback.detected_fixture.body != body:
                continue

            # body fulfills all requirements and is therefore detected
            # add the object of the body to the list of detected bodies, if it does not already exist
            self.state.detected_objects.append(sim_object)
            self.state.detected_object_ids.append(sim_object.id)