import warnings
import mpi_sim
from mpi_sim.core.generator_component import GeneratorComponent
from social_mpc.controller import RobotController
from social_mpc.config.config import RobotConfig
import pkg_resources
import numpy as np
import yaml
from collections import namedtuple
from mpi_sim.utils.box2d_utils import constraint_angle
import cv2
import os
import exputils as eu
# TODO: need group identification for SocialMPC


class RobotState:
    robot_pose = None
    robot_velocity = None
    joint_angles = None
    joint_velocities = None
    local_map = None
    global_map = None
    groups_pose = None
    humans_pose = None
    humans_velocity = None
    humans_visible = None
    groups_id = None
    humans_group_id = None
    humans_id = None
    config = None
    robot_config = None


MapConfig = namedtuple('MapConfig', [
    'global_map_size',
    'global_map_scale',
    'global_map_width',
    'global_map_height',
    'local_map_size',
    'local_map_scale',
    'local_map_width',
    'local_map_height']
)


MyRobotConfig = namedtuple('RobotConfig', [
     'has_pan',
     'has_tilt',
     'min_pan_angle',
     'max_pan_angle',
     'base_footprint_radius']
)


class ARINavigation(GeneratorComponent):

    @staticmethod
    def default_config():
        dc = GeneratorComponent.default_config()
        dc.name = 'navigation'

        dc.ari_mapping_component = None
        dc.human_detector_component = None

        dc.thresh_orientation = 0.2  # rad
        dc.thresh_distance_position = 0.28  # m
        dc.thresh_distance_human = 0.2  # m
        dc.thresh_orientation_pan = 0.2  # rad
        dc.max_human_nb = 20
        dc.max_groups_world = 10

        dc.robot_config = mpi_sim.AttrDict()
        dc.robot_config.base_footprint2base_link = [0., 0., 0.125]
        dc.robot_config.base_link2wheel_left_link = [0., 0.220, 0.0]
        dc.robot_config.base_link2wheel_right_link = [0., -0.220, 0.0]
        dc.robot_config.base_link2caster_left_link = [-0.455, 0.102, -0.080]
        dc.robot_config.base_link2caster_right_link = [-0.455, -0.102, -0.080]
        dc.robot_config.base_link2head_1_link = [-0.226, 0.000, 1.192]
        dc.robot_config.base_link2head_2_link = [-0.183, 0.000, 1.284]
        dc.robot_config.base_link2head_front_camera_link = [-0.120, -0.000, 1.466]
        dc.robot_config.base_link2head_front_camera_optical_frame = [-0.120, -0.000, 1.466]
        dc.robot_config.base_footprint_radius= 0.3
        dc.robot_config.wheel_radius = 0.125
        dc.robot_config.wheel_width = 0.075
        dc.robot_config.wheel_separation = 0.44
        dc.robot_config.wheel_torque = 6.0
        dc.robot_config.wheel_velocity = 1.0
        dc.robot_config.caster_radius = 0.045
        dc.robot_config.caster_width = 0.055
        dc.robot_config.caster_separation_x = 0.91
        dc.robot_config.caster_offset_y = 0.1025
        dc.robot_config.caster_offset_z = -0.08
        dc.robot_config.total_mass = 54.893
        dc.robot_config.wheel_mass = 1.65651
        dc.robot_config.L1 = [[0, 0, 1, 0], [1, 0, 0, 0.182], [0, 1, 0, -0.063], [0, 0, 0, 1]]
        dc.robot_config.L2 = [[0, 0, -1, 0.093], [0, 1, 0, -0.043], [1, 0, 0, 0], [0, 0, 0, 1]]
        dc.robot_config.L3 = [[1, 0, 0, 0], [0, 1, 0, 0.225], [0, 0, 1, -1.192], [0, 0, 0, 1]]
        dc.robot_config.local_map_depth= 3.0
        dc.robot_config.local_map_px_size= 0.05
        dc.robot_config.max_speed_motor_wheel = 1.0
        dc.robot_config.max_forward_speed= 0.5
        dc.robot_config.max_backward_speed= -0.1
        dc.robot_config.max_forward_acc= 0.2
        dc.robot_config.max_ang_vel= 0.5
        dc.robot_config.max_ang_acc= 0.4
        dc.robot_config.max_pan_angle = 1.309
        dc.robot_config.min_pan_angle = -1.309
        dc.robot_config.max_tilt_angle = 0.4363
        dc.robot_config.min_tilt_angle = -0.2618
        dc.robot_config.max_speed_motor_head = 3.0
        dc.robot_config.head_friction = 1.0
        dc.robot_config.head_damping = 0.5
        dc.robot_config.head_cam_fov_h = 62.0
        dc.robot_config.head_cam_fov_v = 48.0
        dc.robot_config.base_cam_fov_h = 360.0
        dc.robot_config.has_pan = True
        dc.robot_config.has_tilt = False


        return dc
    

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        # possible actions the navigation is doing
        self.state.goal_position = None
        self.state.goal_orientation = None
        self.state.position_look_at = None

        self._mapping_component = None
        self._human_detector_component = None

        self.action_idx = None
        self.actions = None
        self.thresh_orientation = self.config.thresh_orientation  # rad
        self.thresh_orientation_pan = self.config.thresh_orientation_pan  # rad
        self.thresh_distance_position = self.config.thresh_distance_position  # m
        self.thresh_distance_human = self.config.thresh_distance_human  # m
        self.max_human_nb = self.config.max_human_nb
        self.max_groups_world = self.config.max_groups_world

        self.is_set_go_towards_human = False
        self.is_set_go_towards_position = False
        self.is_set_look_at_human = False
        self.is_set_look_at_position = False

        # TODO: remove the need for configuration in yml files
        #       add the configuration in the default config under a social_mpc and robot sub dictionary
        
        # config_filename = pkg_resources.resource_filename('modules', '/local_scratch/victor/Documents/RobotLearn/Projects/drl_robot_navigation/drl-navigation-training/code/multiparty_interaction_simulator/modules/social_mpc.yaml')
        self.mpc_controller = RobotController()
        
        # get the robot config
        # robot_config_filename = pkg_resources.resource_filename('modules', 'robot.yaml')
        # config = yaml.load(open(robot_config_filename), Loader=yaml.FullLoader)
        config_dict = eu.AttrDict.from_yaml('/local_scratch/victor/Documents/RobotLearn/Projects/drl_robot_navigation/drl-navigation-training/code/multiparty_interaction_simulator/modules/Motor_Controller/social_mpc/config/robot.yaml').toDict()
        self.robot_config = RobotConfig(dict=config_dict)

        # initialize the robot state
        self.state.robot_state = RobotState()
        self.state.robot_state.robot_config = MyRobotConfig(
            self.robot_config.has_pan,
            self.robot_config.has_tilt,
            self.robot_config.min_pan_angle,
            self.robot_config.max_pan_angle,
            self.robot_config.base_footprint_radius
        )


    def _add_to_simulation(self):

        # # get mapping component or add it if it does not exist
        # if self.object.exist_component('mapping'):
        #     self._ari_mapping = self.object.mapping
        # else:
        #     # use as config the default config as defined in the ari robot
        #     self._ari_mapping = mpi_sim.components.OccupancyGridMapping(config=mpi_sim.objects.ARIRobot.default_mapping_config())
        #     self.object.add_component(self._ari_mapping)

        # initialize the mpc by calling it for 1 step
        self.mpc_controller.controller_config.goto_target_id = -1
        self.mpc_controller.controller_config.human_target_id = -1
        self.mpc_controller.controller_config.pan_target_id = -1
        self.mpc_controller.controller_config.goto_target_pose = np.zeros_like(self.mpc_controller.controller_config.goto_target_pose)
        self._recreate_state()
        self.mpc_controller.step(self.state.robot_state)

        self.action_idx = 0
        self.actions = np.copy(self.mpc_controller.initial_action)


    def get_mapping_component(self):

        # if we do not have the component yet, then use the config ari_mapping_component to get it
        if self._mapping_component is None:

            if self.config.ari_mapping_component is None:
                # user defined no component, so create one

                # use as config the default config as defined in the ari robot
                self._mapping_component = mpi_sim.components.OccupancyGridMapping(config=mpi_sim.objects.ARIRobot.default_mapping_config())
                self.object.add_component(self._mapping_component)

            elif isinstance(self.config.ari_mapping_component, str):
                # user defined a name of the component that can be found in the object

                # if the component exist already, then set it
                if self.object.exist_component(self.config.ari_mapping_component):
                    self._mapping_component = self.object.get_component(self.config.ari_mapping_component)

            elif isinstance(self.config.ari_mapping_component, mpi_sim.Component):
                # user defined a component object, then use this one
                self._mapping_component = self.config.ari_mapping_component

            else:
                raise ValueError('Unsupported type for config \'ari_mapping_component\'! Must be eiter None, a string, or a Component.')

        return self._mapping_component


    def get_human_detector_component(self):

        # if we do not have the component yet, then use the config ari_mapping_component to try to set it
        if self._human_detector_component is None:

            # the config is not None, so try to get the component
            if self.config.human_detector_component is not None:
                # user defined no component, so create

                if isinstance(self.config.human_detector_component, str):
                    # user defined a name of the component that can be found in the object

                    # if the component exist already, then set it
                    if self.object.exist_component(self.config.human_detector_component):
                        self._human_detector_component = self.object.get_component(self.config.human_detector_component)

                elif isinstance(self.config.human_detector_component, mpi_sim.Component):
                    # user defined a component object, then use this one
                    self._human_detector_component = self.config.human_detector_component

                else:
                    raise ValueError('Unsupported type for config \'human_detector_component\'! Must be eiter None, a string, or a Component.')

        return self._human_detector_component


    def _reset_robot_state(self):

        ari_mapping = self.get_mapping_component()

        self.state.robot_state.humans_id = np.ones(self.max_human_nb) * -1
        self.state.robot_state.humans_group_id = np.ones(self.max_human_nb) * -1
        self.state.robot_state.groups_id = np.ones(self.max_groups_world) * -1
        self.state.robot_state.humans_visible = np.ones(self.max_human_nb)
        self.state.robot_state.humans_pose = np.zeros((self.max_human_nb, 3))  # x, y, angle
        self.state.robot_state.groups_pose = np.zeros((self.max_groups_world, 2))  # x, y
        self.state.robot_state.humans_velocity = np.zeros((self.max_human_nb, 2))
        self.state.robot_state.robot_pose = np.zeros(3)
        self.state.robot_state.robot_velocity = np.zeros(2)
        if self.state.robot_state.robot_config.has_pan and self.state.robot_state.robot_config.has_tilt:
            self.state.robot_state.joint_angles = np.zeros(2)
            self.state.robot_state.joint_velocities = np.zeros(2)
        elif not self.state.robot_state.robot_config.has_pan and not self.state.robot_state.robot_config.has_tilt:
            self.state.robot_state.joint_angles = None
            self.state.robot_state.joint_velocities = None
        else:
            self.state.robot_state.joint_angles = np.zeros(1)
            self.state.robot_state.joint_velocities = np.zeros(1)
        self.state.robot_state.global_map = np.zeros_like(ari_mapping.global_map)
        self.state.robot_state.local_map = np.zeros_like(ari_mapping.local_map)

        self.state.robot_state.config = MapConfig(
            ari_mapping.global_map_area,
            1/ari_mapping.resolution,  # scale in cells per m
            ari_mapping.global_map.shape[0],
            ari_mapping.global_map.shape[1],
            ari_mapping.local_map_area,
            1/ari_mapping.resolution,  # scale in cells per m
            ari_mapping.local_map.shape[0],
            ari_mapping.local_map.shape[1],
        )


    def set_go_towards_position(self, goal_position, goal_orientation, local=False, reset=False):
        self.state.goal_position = goal_position
        self.state.goal_orientation = goal_orientation

        self.mpc_controller.controller_config.goto_target_id = -1
        self.mpc_controller.controller_config.human_target_id = -1
        if reset:
            goto_target_pose = np.zeros_like(self.mpc_controller.controller_config.goto_target_pose)
        else:
            if local:
                local = 1
            else:
                local = 0
            goto_target_pose = np.array([*goal_position, goal_orientation, local, 1])
        self.mpc_controller.controller_config.goto_target_pose = goto_target_pose
        if not goto_target_pose.any():
            self.mpc_controller.reset_shared_values()
        self.mpc_controller.set_goto_target(self.state.robot_state, target=goto_target_pose)
        self.mpc_controller.set_mode()
        self.mpc_controller.set_weights()

        self.is_set_go_towards_position = True


    def set_go_towards_human(self, human_id):
        self.state.goal_position = None
        self.state.goal_orientation = None

        # get human_id and human object
        if isinstance(human_id, mpi_sim.Object):
            human = human_id
            human_id = human_id.id
        else:
            human = self.simulation.objects[human_id]

        # if the human is not already detected, then try to detect it now
        if human not in self._get_detected_humans():

            human_detector = self.get_human_detector_component()
            if human_detector is not None:
                human_detector.detect()

                # if the human is still not detected, create a warning
                if human not in self._get_detected_humans():
                    warnings.warn('Human with id {} is not detected. Therefore the navigation can not go_towards it.')

        # need to recreate the MPC state, so that we are sure the MPC knows the currently detected humans
        self._recreate_state()

        self.mpc_controller.controller_config.goto_target_id = human_id
        self.mpc_controller.controller_config.human_target_id = -1
        self.mpc_controller.controller_config.goto_target_pose = np.zeros_like(self.mpc_controller.controller_config.goto_target_pose)
        self.mpc_controller.set_goto_target(self.state.robot_state, target=human_id)
        self.mpc_controller.set_mode()
        self.mpc_controller.set_weights()
        if human_id == -1:
            self.mpc_controller.reset_shared_values()

        self.is_set_go_towards_human = True


    def set_look_at_human(self, human_id):
        self.state.position_look_at = None
        
        # get human_id and human object
        if isinstance(human_id, mpi_sim.Object):
            human = human_id
            human_id = human_id.id
        else:
            human = self.simulation.objects[human_id]

        # if the human is not already detected, then try to detect it now
        if human not in self._get_detected_humans():

            human_detector = self.get_human_detector_component()
            if human_detector is not None:
                human_detector.detect()

                # if the human is still not detected, create a warning
                if human not in self._get_detected_humans():
                    warnings.warn('Human with id {} is not detected. Therefore the navigation can not look_at it.')

        # need to recreate the MPC state, so that we are sure the MPC knows the currently detected humans
        self._recreate_state()

        self.mpc_controller.controller_config.pan_target_id = human_id
        self.mpc_controller.set_pan_target(self.state.robot_state, target=human_id)
        self.mpc_controller.set_mode()
        self.mpc_controller.set_weights()

        self.is_set_look_at_human = True


    def set_look_at_position(self, goal_position, local=False, reset=False):
        self.state.position_look_at = None

        self.state.position_look_at = goal_position

        if reset:
            look_at_target_pose = np.zeros(self.mpc_controller.controller_config.target_pose_look_dim)
        else:
            if local:
                local = 1
            else:
                local = 0
            look_at_target_pose = np.array([*goal_position, local, 1])
        self.mpc_controller.controller_config.pan_target_id = -1
        self.mpc_controller.set_pan_target(self.state.robot_state, target=look_at_target_pose)
        self.mpc_controller.set_mode()
        self.mpc_controller.set_weights()

        self.is_set_look_at_position = True


    def stop(self):
        r"""Stops any ongoing robot navigation action."""

        # stop base navigation
        self.state.goal_position = None
        self.state.goal_orientation = None

        self.mpc_controller.controller_config.goto_target_id = -1
        self.mpc_controller.controller_config.human_target_id = -1
        self.mpc_controller.controller_config.goto_target_pose = np.zeros_like(self.mpc_controller.controller_config.goto_target_pose)
        self.mpc_controller.set_goto_target(self.state.robot_state, target=-1)
        self.mpc_controller.set_mode()
        self.mpc_controller.set_weights()
        self.is_set_go_towards_human = False
        self.is_set_go_towards_position = False

        # stop head navigation
        self.state.position_look_at = None

        self.mpc_controller.controller_config.pan_target_id = -1
        self.mpc_controller.set_pan_target(self.state.robot_state, target=(0, 0))
        self.is_set_look_at_position = False

        self.mpc_controller.reset_shared_values()


    def _step(self):

        #####################################
        # recreate the robot state

        self._recreate_state()

        #####################################
        # use the mpc controller for the navigation       

        if self.is_set_go_towards_human or self.is_set_go_towards_position or self.is_set_look_at_human or self.is_set_look_at_position:

            x_robot = self.object.position[0]
            y_robot = self.object.position[1]
            yaw_robot = self.object.orientation
            yaw_robot_pan = yaw_robot - self.object.joints[-1].bodyA.angle
            x_final = self.mpc_controller.shared_target[2]
            y_final = self.mpc_controller.shared_target[3]
            th_final = self.mpc_controller.shared_target[4]
            dist_to_goto_target = np.linalg.norm(np.array([x_robot - x_final, y_robot - y_final]))
            delta_orient = np.abs(constraint_angle(yaw_robot - th_final))
            delta_orient_pan = 0.
            if self.state.position_look_at is not None:
                yaw_target_pan = constraint_angle(-np.arctan2(self.state.position_look_at[0] - x_robot, self.state.position_look_at[1] - y_robot))
                delta_orient_pan = np.abs(constraint_angle(yaw_robot_pan - yaw_target_pan))

            if self.is_set_go_towards_human and self.mpc_controller.goto_goal[-1]:
                if dist_to_goto_target <= self.thresh_distance_human and delta_orient <= self.thresh_orientation:
                    self.stop()

            if self.is_set_go_towards_position and self.mpc_controller.goto_goal[-1]:
                if dist_to_goto_target <= self.thresh_distance_position and delta_orient <= self.thresh_orientation:
                    self.set_go_towards_position(self.state.goal_position, self.state.goal_orientation, reset=True)
            
            if self.mpc_controller.pan_goal[-1]:
                if delta_orient_pan <= self.thresh_orientation_pan:
                    if self.is_set_look_at_human:
                        self.stop()
                    if self.is_set_look_at_position:
                        self.set_look_at_position(self.state.position_look_at, reset=True)

            self.mpc_controller.step(self.state.robot_state)
            self.action_idx = 0
            self.actions = self.mpc_controller.actions
            action = np.array(self.actions[self.action_idx])

            self.object.pan_vel = -action[0]
            self.object.forward_velocity = action[2]
            self.object.orientation_velocity = action[1]

    
    def _recreate_state(self):

        ari_mapping = self.get_mapping_component()

        self._reset_robot_state()
        angle = constraint_angle(self.object.orientation)
        self.state.robot_state.robot_pose = np.array([*self.object.position, angle])
        v = np.array([-np.sin(angle), np.cos(angle)])
        self.state.robot_state.robot_velocity[0] = np.dot(np.asarray(self.object.position_velocity), v)
        self.state.robot_state.robot_velocity[1] = self.object.orientation_velocity
        self.state.robot_state.joint_angles[:] = constraint_angle(-self.object.joints[-1].bodyA.angle)
        # self.state.robot_state.joint_velocities[:] = -np.array(self.object.joints[-1].bodyA.linearVelocity)
        kernel = cv2.getStructuringElement(
            cv2.MORPH_ELLIPSE,
            (2, 2)
        )
        local_map = cv2.dilate(ari_mapping.local_map, kernel, iterations=self.mpc_controller.controller_config.dilation_iter)
        self.state.robot_state.global_map = np.flip(ari_mapping.global_map.T, axis=0)
        self.state.robot_state.local_map = np.flip(local_map.T, axis=0)

        # use the human_detector to identify which persons are seen
        for human_idx, human in enumerate(self._get_detected_humans()):

            if human_idx >= self.max_human_nb:
                warnings.warn('Number of humans in simulation is larger than limit for ARI\'s SocialMPC. Set max_human_nb of the ARI Navigation component higher.')
                break
            self.state.robot_state.humans_id[human_idx] = human.id
            angle = constraint_angle(human.orientation)
            self.state.robot_state.humans_pose[human_idx, :] = [*human.position, angle]
            v = np.array([-np.sin(angle), np.cos(angle)])
            self.state.robot_state.humans_velocity[human_idx, :] = [np.dot(human.position_velocity, v), human.orientation_velocity]
            self.state.robot_state.humans_visible[human_idx] = 0.


    def _get_detected_humans(self):
        human_detector = self.get_human_detector_component()
        if human_detector is None:
            humans = self.simulation.get_objects_by_type(mpi_sim.objects.Human)
        else:
            humans = human_detector.detected_objects

        return humans


    def _get_detected_human_ids(self):
        human_detector = self.get_human_detector_component()
        if human_detector is None:
            humans = self.simulation.get_objects_by_type(mpi_sim.objects.Human)
            humans_ids = [human.id for human in humans]
        else:
            humans_ids = human_detector.detected_object_ids

        return humans_ids


    def _close(self):
        self.mpc_controller.close()
