import mpi_sim
from mpi_sim.core.sensor_component import SensorComponent
import numpy as np
from mpi_sim.utils.box2d_utils import constraint_angle


import cv2

# TODO: if there are several agents that do mapping, use a common mapping process to generate the global map to save resources


class SocialMapping(SensorComponent):
    r"""Component that creates a social map centered and oriented around its object, for example a robot agent."""

    @staticmethod
    def default_config():
        dc = SensorComponent.default_config()
        dc.name = 'social_mapping'
        dc.area = None  # area of global map ((x_min, x_max), (y_min, y_max)), if none it is simulation.visible_area
        dc.depth = 5.0  # depth if local in meters
        dc.object_filter = None  # filter for the elements that should be mapped
        dc.resolution = 0.05  # resolution of the maps in m per cell
        dc.update_global_map_automatically = True  # should the global map be automatically updated each step
        dc.update_local_map_automatically = False # update local map each time the smulation is step
        dc.imported_global_map_path = False # Import Global map - path
        dc.imported_global_map_resolution = False  # Import Global map - resolution
        dc.imported_global_map_area = False # Import Global map - area (can either be a tuple of a function is the area is not squared)
        dc.imported_global_map_px_coord_center = None
        dc.transform_position_to_map_coordinate = None
        dc.transform_real_to_sim_orientation = None
        return dc


    @property
    def resolution(self):
        r"""Get the tesolution of the maps in m per cell."""
        return self._resolution


    @property
    def global_map(self):
        r"""Get the global social map from which the local map is generated."""
        if self.state.global_map is None:
            return None
        else:
            return self.update_global_map()


    @property
    def global_map_area(self):
        r"""The area that is spanned by the global map defined as ((x_min, x_max), (y_min, y_max))"""
        return self._global_map_area


    @property
    def global_map_shape(self):
        r"""The shape of the global map defined as (width, height)."""
        return self._global_map_shape


    @property
    def local_map(self):
        r"""Get the local social map that is centered and oriented around the object of the component."""
        if self.state.local_map is None:
            return None
        else:
            return self.update_local_map()


    @property
    def local_map_area(self):
        r"""The area that is spanned by the local map defined as ((x_min, x_max), (y_min, y_max)) in reference frame of the local map."""
        return self._local_map_area


    @property
    def local_map_shape(self):
        r"""The shape of the local map defined as (width, height)."""
        return self._local_map_shape
    
    def set_human_velocities(self, human_velocities):
        r""" Set the velocities of the human """
        # Necessary when the humans are navigating with the navigation component
        self.human_velocities= human_velocities


    def set_global_map(self, global_map):
        self.state.global_map = mpi_sim.utils.SocialMap(
                map=global_map,
                area=self._global_map_area,
                resolution=self._resolution
            )

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)
        self.state.global_map = None

        if not self.config.imported_global_map_path : 
            self._resolution = self.config.resolution
            self._global_map_area = self.config.area

            self._global_map_shape = None
            
            # is the global map initially created, if not do it before creating the local map
            self.state.is_global_map_initialized = False

            # initialize the global map if possible
            if self._global_map_area is not None:
                self._global_map_shape = mpi_sim.utils.get_social_map_shape(area=self._global_map_area, resolution=self._resolution)
                self.state.global_map = mpi_sim.utils.SocialMap(
                    map=np.zeros(self._global_map_shape),
                    area=self._global_map_area,
                    resolution=self._resolution
                )

            

            
        else : 
            self._resolution = self.config.imported_global_map_resolution
            self._global_map_area = self.config.imported_global_map_area
            self.state.is_global_map_initialized = True #The global map is initialized and doesn't need to be updated 

            if isinstance(self.config.imported_global_map_path, str):

                imported_global_map = np.load(file=self.config.imported_global_map_path)
                self.shape_imported_global_map = np.shape(imported_global_map)
                self.state.global_map = mpi_sim.utils.SocialMap(
                    map=np.zeros_like(imported_global_map),
                    area=self._global_map_area,
                    resolution=self._resolution
                )

                self._global_map_shape = np.shape(imported_global_map)

            else :
                raise UserWarning('The parameter \'imported_global_map_path\' should be a type str but it is {}'.format(type(self.config.imported_global_map_path)))

        self.human_velocities = None
        self._local_map_area = ((-self.config.depth, self.config.depth), (-self.config.depth, self.config.depth))
        local_map_length = int((self.config.depth / self._resolution) * 2)
        self._local_map_shape = (local_map_length, local_map_length)
        self.state.local_map = mpi_sim.utils.SocialMap(
            map=np.zeros(self._local_map_shape),
            area=None,
            resolution=self._resolution
        )
        
    def _add_to_simulation(self):

        # if the global map area should span the whole simulation (config.area=None), then read visible_area from the simulation when added to it
        if self._global_map_area is None:
            self._global_map_area = self.simulation.visible_area
            self._global_map_shape = mpi_sim.utils.get_social_map_shape(area=self._global_map_area, resolution=self._resolution)
            self.state.global_map = mpi_sim.utils.SocialMap(
                map=np.zeros(self._global_map_shape),
                area=self._global_map_area,
                resolution=self._resolution
            )


    def update_global_map(self):
        
        if not self.config.imported_global_map_path :

            self.state.global_map = mpi_sim.utils.create_social_map(
                    self.simulation,
                    area=self._global_map_area,
                    resolution=self._resolution,
                    human_velocities=self.human_velocities
            ) 
        else :
            self.state.global_map = mpi_sim.utils.create_social_map(
                    self.simulation,
                    area=self._global_map_area,
                    resolution=self._resolution,
                    human_velocities=self.human_velocities,
                    flag_imported_global_map=True
            ) 

        return self.state.global_map.map


    def update_local_map(self):
        if self.state.global_map is None:
            self.update_global_map()
        if not self.config.imported_global_map_path :
            self.state.local_map = mpi_sim.utils.create_local_perspective_social_map(
                global_map=self.state.global_map,
                position=self.object.position,
                orientation=constraint_angle(self.object.orientation),
                depth=self.config.depth,
            )
        else :
            # Same but we need to relocate the position from the center of the imported room
            self.state.local_map = mpi_sim.utils.create_local_perspective_social_map(
                global_map=self.state.global_map,
                # find a way to explain this 
                position=self.object.position,
                orientation=constraint_angle(self.config.transform_real_to_sim_orientation(self.object.orientation)), 
                depth=self.config.depth,
                from_given_center = self.config.imported_global_map_px_coord_center,
                transform_position_to_map_coordinate = self.config.transform_position_to_map_coordinate
            )
        return self.state.local_map.map


    def _step(self):
        if self.config.update_global_map_automatically or not self.state.is_global_map_initialized:
            self.update_global_map()
            self.state.is_global_map_initialized = True
        if self.config.update_local_map_automatically :
            self.update_local_map()
