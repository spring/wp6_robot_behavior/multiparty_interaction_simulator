import mpi_sim as sim
from mpi_sim.core.sensor_component import SensorComponent
import numpy as np

class GazeSensor(SensorComponent):

    @staticmethod
    def default_config():
        dc = SensorComponent.default_config()

        dc.default_gaze_direction = 0.0
        dc.default_gaze_target = None

        dc.direction_error_sigma = 0.01
        dc.target_error_border = 0.5

        return dc


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self._gaze_history = dict()
        self.raycast_sensor = None

        # TODO: detect if the parent ARI agent has an head camera raycast component
        #       if not: create one

    def _step(self):

        # erase the history
        self._gaze_history = dict()

        # TODO: get list of humans seen by the raycast sensor

        # TODO: check for each human if the face is visible

        # TODO: identify the gaze of each human
        #self._gaze_history[human.id] = (human.gaze.target, human.gaze.direction)

        # TODO: allow to include an error model


    def get_gaze(self, person):
        """Get the gaze (direction, target) of a person that has a GazeGenerator Component."""

        if isinstance(person, int):
            person = self.simulation.get_object(person)

        if person.id in self._gaze_history:
            # if the emotion was already sensed, then return it
            gaze_tuple = self._gaze_history[person.id]
        else:
            gaze_direction = self.config.default_gaze_direction
            gaze_target = self.config.default_gaze_target

            gaze_generator = person.get_component(sim.Component.GazeGenerator)

            if gaze_generator is not None:
                gaze_direction_error = np.random.randn() * self.config.direction_error_sigma

                gaze_direction = gaze_generator.gaze_direction + gaze_direction_error

                if gaze_direction_error < self.config.target_error_border:
                    gaze_target = gaze_generator.gaze_target
                else:
                    gaze_target = None

            gaze_tuple = (gaze_direction, gaze_target)

            self._gaze_history[person.id] = gaze_tuple

        return gaze_tuple
