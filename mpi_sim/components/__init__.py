# import the ARINavigation only if possible, i.e. if the social navigation is installed
try:
    from mpi_sim.components.ari_navigation import ARINavigation
except ImportError:
    pass
# Uncomment to debug
# from mpi_sim.components.ari_navigation import ARINavigation

from mpi_sim.components.gaze_generator import GazeGenerator
from mpi_sim.components.object_detector import ObjectDetector
from mpi_sim.components.occupancy_grid_mapping import OccupancyGridMapping
from mpi_sim.components.social_mapping import SocialMapping
from mpi_sim.components.social_force_navigation import SocialForceNavigation
from mpi_sim.components.speech_generator import SpeechGenerator
from mpi_sim.components.raycast_sensor import RayCastSensor
from mpi_sim.components.point_drawer import PointDrawer

