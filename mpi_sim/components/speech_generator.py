import mpi_sim as sim
from mpi_sim.core.generator_component import GeneratorComponent
from collections import deque
from collections import namedtuple


SpeechAct = namedtuple('SpeechAct', ['content', 'act', 'start_time', 'end_time', 'start_step', 'end_step'])


class SpeechGenerator(GeneratorComponent):

    @staticmethod
    def default_config():
        dc = GeneratorComponent.default_config()

        dc.name = 'speech'

        # number of words spoken per minute
        dc.words_per_minute = 130.0

        # maximum length of a word before it gets cut and counted as an separate words.
        dc.max_word_length = 10

        return dc


    @property
    def current_speech_act(self):
        """Current speech act (string). For example: "WELCOME".
        None if there is no speech act.
        """
        return self.state.current_speech_act


    @property
    def current_speech_content(self):
        """Speech content (string) of the current speech act.
        None if there is not speech act.

        Example: "Hello. I am ARI."
        """
        return self.state.current_speech_content


    @property
    def current_speech(self):
        """Current word that is said in this time step.
        Returns an empty string if at the current time step no new word is said.
        None if there is no current speech act.

        Examples over several steps: "Hello.", "", "", "I", "", "am", "", "ARI."
        """
        return self.state.current_speech


    @property
    def previous_speech_act(self):
        act = None
        if self.state.is_speech_act:
            if len(self.state.history) > 1:
                act = self.state.history[-2].act
        else:
            if len(self.state.history) > 0:
                act = self.state.history[-1].act
        return act


    @property
    def previous_speech_content(self):
        content = None
        if self.state.is_speech_act:
            if len(self.state.history) > 1:
                content = self.state.history[-2].content
        else:
            if len(self.state.history) > 0:
                content = self.state.history[-1].content
        return content


    @property
    def history(self):
        """The history stores the previous speech acts as a list.
        The first element is the first speech act and the last one the previous or current speech act.
        Each element is a tuple with: (speech_content, speech_act, start_time, end_time).
        """
        return self.state.history


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.state.history = []

        self.state.current_speech_act = None
        self.state.current_speech_content = None
        self.state.current_speech = None

        self.state.next_speech_act = None
        self.state.next_speech_content = None
        self.state.next_speech_per_step = None
        self.state.is_new_speech_act = False
        self.state.is_speech_act = False


    def say(self, content, act='UNKNOWN'):
        """Generates the given speech act at the next step.

        If the content is None, then no speech act is generated.
        If a new speech act (or None) is given while another is still going on the the previous one is stopped.

        Args:
            content (string): Content of the speech act, for example: 'Hello, I am ARI.'
            act (string): Speech act, for example: 'WELCOME'. (default='UNKNOWN')
        """

        if self.state.is_speech_act:
            # if a new speech act is given before the previous is finished, set the end-time of the previous in the history to the previous time
            self._set_end_time_of_current_speech_act_in_history()

        if content:
            self.state.is_new_speech_act = True
            self.state.is_speech_act = True
            self.state.next_speech_act = act
            self.state.next_speech_content = content

            # compute how and when to say certain speech contents
            self.state.next_speech_per_step = self.get_speech_per_step(content)

        else:
            # if no content is given, stop the current speech act and say nothing
            self.state.is_new_speech_act = False
            self.state.is_speech_act = False
            self.state.next_speech_act = None
            self.state.next_speech_content = None
            self.state.next_speech_per_step = None


    def _reset(self):
        pass


    def _step(self):

        if not self.state.is_speech_act:
            # if there is no speech act, sat nothing
            self.state.current_speech_act = None
            self.state.current_speech_content = None
            self.state.current_speech = None
        else:
            # otherwise

            # if there is new speech
            if self.state.is_new_speech_act:

                # set the current speech act
                self.state.current_speech_act = self.state.next_speech_act
                self.state.current_speech_content = self.state.next_speech_content
                self.state.next_speech_act = None
                self.state.next_speech_content = None

                # add the speech to the history if the current start time
                self.state.history.append(SpeechAct(
                    self.state.current_speech_content,
                    self.state.current_speech_act,
                    self.simulation.time,
                    None,
                    self.simulation.step_counter,
                    None
                ))

                self.state.is_new_speech_act = False

            # if there is something to say
            if self.state.next_speech_per_step:

                # get next speech content and remove it from the speech content deque
                self.state.current_speech = self.state.next_speech_per_step.popleft()

                # if this is the last speech content
                if len(self.state.next_speech_per_step) == 0:
                    # speech act is finished --> update time in history
                    self._set_end_time_of_current_speech_act_in_history()

                    # define that the speech act is over, thus next step the generator will generate no speech act
                    self.state.is_speech_act = False
                    self.state.next_speech_act = None
                    self.state.next_speech_content = None
                    self.state.next_speech_per_step = None


    def _set_end_time_of_current_speech_act_in_history(self, end_time=None, end_step=None):
        """Sets the end time of the current speech act in the history"""

        if end_time is None:
            end_time = self.simulation.time
        if end_step is None:
            end_step = self.simulation.step_counter

        history_element = self.state.history[-1]

        # to save each history element as a tuple I have to recreate the previous tuple, because I can not change tuples
        self.state.history[-1] = (SpeechAct(
                    history_element.content,
                    history_element.act,
                    history_element.start_time,
                    end_time,
                    history_element.start_step,
                    end_step
        ))


    def get_speech_per_step(self, speech_content):
        """Creates a deque list that defines the speech for each time step depending on the given speech content.

        Args:
            speech_content: String with the speech content. For example: "Hello. I am ARI."

        Returns:
            A list with the words and word elements that are spoken at each time step.
        """

        # we multiply the words per minute by 2 to include also spaces between words which are here counted as separate words
        words_per_second = self.config.words_per_minute / 60

        words_per_time_step = self.simulation.step_length * words_per_second

        # create a list with all words
        word_list = speech_content.split()

        # split all words that are too long
        split_word_list = []
        for word_idx, word in enumerate(word_list):
            while len(word) > self.config.max_word_length:
                split_word_list.append(word[:self.config.max_word_length])
                word = word[self.config.max_word_length:]
            split_word_list.append(word)

            # add spaces at the end of words besides the final word of the content
            if word_idx < len(word_list) - 1:
                split_word_list[-1] += ' '

        # create output per time step until all words are processed
        speech_per_time_step = deque()
        # this counter is used to identify when the next word is spoken
        # this happens if it is >= 1.0, then it is decreased by 1.0 to start again
        # at each step it is increased by the number of words that are spoken per step
        time_till_next_word = 0.0
        word_idx = 0
        while word_idx < len(split_word_list):
            time_till_next_word += words_per_time_step

            current_word = ''

            # use a loop over time_till_next_word in case the wpm is high or the seconds per step is low,
            # so that several words per step (time_till_next_word >= 2) are maybe spoken
            while time_till_next_word >= 1.0 and word_idx < len(split_word_list):
                current_word += split_word_list[word_idx]
                word_idx += 1
                time_till_next_word -= 1.0

            speech_per_time_step.append(current_word)

        return speech_per_time_step
