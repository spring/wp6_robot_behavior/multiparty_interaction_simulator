import mpi_sim as sim
from mpi_sim.core.generator_component import GeneratorComponent
import numpy as np


class GazeGenerationModes:
    NONE = 1
    DIRECTION = 2
    TARGETS = 3


class GazeGenerator(GeneratorComponent):
    """A Gaze Generator for human agents."""

    @staticmethod
    def default_config():
        dc = GeneratorComponent.default_config()
        dc.name = 'gaze'

        # average time between changing the gaze
        dc.change_rate = 1.5

        dc.random_number_sampling = sim.utils.RandomNumberSampling.default_config()

        dc.target = None
        dc.direction = None

        dc.direction_sampling = sim.AttrDict()
        dc.direction_sampling.prefered_direction = None
        dc.direction_sampling.direction_std = 0.5

        dc.target_sampling = sim.AttrDict()
        dc.target_sampling.main_targets = None
        dc.target_sampling.side_targets = None
        dc.target_sampling.main_target_pr = 0.8

        return dc


    @property
    def direction(self):
        return self.state.gaze_direction


    @direction.setter
    def direction(self, direction):
        self.state.mode = GazeGenerationModes.NONE
        self.state.gaze_direction = direction
        self.state.gaze_target = None


    @property
    def target(self):
        return self.state.gaze_target


    @target.setter
    def target(self, target):
        self.state.mode = GazeGenerationModes.NONE
        self.state.gaze_target = self.simulation.objects[target]
        self.state.gaze_direction = sim.utils.measure_relative_angle(self.object, self.state.gaze_target)


    @property
    def change_rate(self):
        return self.state.change_rate


    @change_rate.setter
    def change_rate(self, rate):
        if rate <= 0:
            raise ValueError('Gaze change rate must be >= 0!')

        self.state.change_rate = rate
        self.state.change_probability = 1 / rate


    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        self.state.gaze_direction = 0.0
        self.state.gaze_target = None

        self.change_rate = self.config.change_rate

        # detect which sampling method should be used according to the configuration
        if self.config.target is not None:
            mode = GazeGenerationModes.NONE
            self.state.gaze_target = self.config.target

        elif self.config.direction is not None:
            mode = GazeGenerationModes.NONE
            self.state.gaze_direction = self.config.direction

        elif self.config.target_sampling.main_targets is not None:
            mode = GazeGenerationModes.TARGETS
            self.state.main_targets = sim.utils.as_list(self.config.target_sampling.main_targets)
            self.state.side_targets = sim.utils.as_list(self.config.target_sampling.side_targets)
            self.state.main_target_pr = self.config.target_sampling.main_target_pr

        else:
            mode = GazeGenerationModes.DIRECTION
            self.state.prefered_direction = self.config.direction_sampling.prefered_direction
            self.state.direction_std = self.config.direction_sampling.direction_std

        self.state.mode = mode
        self.state.is_reset_mode = True

        # get random numbers without randomly sampling at each step, which reduces the performance
        self.random_number_sampling = sim.utils.RandomNumberSampling(self.config.random_number_sampling)


    def _add_to_simulation(self):
        # save list of targets as object references if added to the simulation
        if self.state.mode == GazeGenerationModes.TARGETS:
            self.state.main_targets = [self.simulation.entities[trg] for trg in self.state.main_targets]
            self.state.side_targets = [self.simulation.entities[trg] for trg in self.state.side_targets]


    def _step(self):

        if self.state.mode != GazeGenerationModes.NONE:

            # should the gaze be changed (always change after the sampling mode was changed)
            if self.state.is_reset_mode or self.random_number_sampling.rand() < self.state.change_probability:

                if self.state.mode == GazeGenerationModes.DIRECTION:
                    new_direction, new_target = self._do_direction_sampling()
                elif self.state.mode == GazeGenerationModes.TARGETS:
                    new_direction, new_target = self._do_target_sampling()
                else:
                    raise ValueError('Unknown sampling mode!')

                self.state.gaze_direction = new_direction
                self.state.gaze_target = new_target

                self.state.is_reset_mode = False


    ########################################
    # Sampling by direction angle

    def set_direction_sampling(self, prefered_direction=None, direction_std=0.5):
        """Sets up the sampling of the gaze direction directly by angle.

        Args:
            prefered_direction (float): Preferred direction in rad. If None, then the direction will be the direction of the parent object.

        """

        if not isinstance(direction_std, float):
            raise ValueError('The given direction must be an angle in rad!')

        self.state.prefered_direction = prefered_direction
        self.state.direction_std = direction_std
        self.state.is_reset_mode = True


    def _do_direction_sampling(self):

        if self.state.prefered_direction is None:
            # use orientation of parent object (usually a human) as default direction
            prefered_direction = self.object.orientation
        else:
            prefered_direction = self.state.prefered_direction

        new_direction = self.random_number_sampling.rand() * self.state.direction_std + prefered_direction
        new_direction = sim.utils.constraint_angle(new_direction)
        return new_direction, None


    #########################################
    # Sampling by target object

    def set_target_sampling(self, main_targets, side_targets, main_target_pr=0.8):
        r''' Starts to generate gaze by sampling over target objects.
        Has a list of main and side targets and a probability to look at a main target.
        The probability inbetween main targets and side targets are the same.

        Args:
            main_targets (Object, list): Object or list of main targets.
            side_targets (Object, list): Object or list of side targets.
            main_target_pr (float): Probability to look at one of the main targets. (Default=0.8)
        '''

        main_targets = sim.utils.as_list(main_targets)
        side_targets = sim.utils.as_list(side_targets)

        if main_target_pr < 0 and main_target_pr > 1:
            raise ValueError('main_target_pr must be between 0 and 1!')

        # convert targets to target references instead of ids, to avoid further converstions at each step
        self.state.main_targets = [self.simulation.entities[trg] for trg in main_targets]
        self.state.side_targets = [self.simulation.entities[trg] for trg in side_targets]
        self.state.main_target_pr = main_target_pr
        self.state.mode = GazeGenerationModes.TARGETS
        self.state.is_reset_mode = True


    def _do_target_sampling(self):

        # should a side or a main target be looked at?
        if self.state.main_targets and self.random_number_sampling.rand() < self.state.main_target_pr:
            targets = self.state.main_targets
        elif self.state.side_targets:
            targets = self.state.side_targets
        else:
            targets = [None]

        if len(targets) == 1:
            target = targets[0]
        else:
            # targets between side targets and main targets have a equal probability
            target = np.random.choice(targets)

        # get direction to target
        direction = sim.utils.measure_relative_angle(self.object, target)

        return direction, target
