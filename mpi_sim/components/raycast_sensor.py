## Software gym_box2d_diff_drive
##
## Derived from pybox2d distributed under zlib License (c), 2008-2011 Ken Lauer
## sirkne at gmail dot com
##
## This file, corresponding to gym_box2d_diff_drive module of WP6, is part of a
## project that has received funding from the European Union’s Horizon 2020
## research and innovation programme under grant agreement No 871245.
##
## Copyright (C) 2022 by INRIA
## Authors : Chris Reinke, Alex Auternaud, Victor Sanchez
## alex dot auternaud at inria dot fr
## victor dot sanchez at inria dot fr

import pygame
import numpy as np
from mpi_sim.core.sensor_component import SensorComponent
import mpi_sim as sim
from Box2D import (b2Color, b2Vec2, b2RayCastCallback)
# TODO: rename this component to something like ObjectDetector which can be configured to say which object types should be detected


class RayCastClosestCallback(b2RayCastCallback):
    """This callback finds the closest hit"""

    def __init__(self, wrong_fix, **kwargs):
        b2RayCastCallback.__init__(self, **kwargs)
        self.wrong_fix = wrong_fix
        self.fixture = None
        self.point = None
        self.normal = None
        self.hit = False


    def ReportFixture(self, fixture, point, normal, fraction):
        """
        Called for each fixture found in the query. You control how the ray
        proceeds by returning a float that indicates the fractional length of
        the ray. By returning 0, you set the ray length to zero. By returning
        the current fraction, you proceed to find the closest point. By
        returning 1, you continue with the original ray clipping. By returning
        -1, you will filter out the current fixture (the ray will not hit it).
        """

        if fixture.body.userData['name'] not in self.wrong_fix :
            self.hit = True
            self.fixture = fixture
            self.point = b2Vec2(point)
            self.normal = b2Vec2(normal)

        return fraction

class RayCastSensor(SensorComponent):

    @staticmethod
    def default_config():
        dc = sim.SensorComponent.default_config()
        dc.name = 'raycast_sensor'
        dc.field_of_view = 360 # Range of the lidar in degree
        dc.delta_angle = 2. # Angle between two beams of lidar in degree
        dc.ray_max_range = 20 # Max distance readable by a given beam
        dc.noise_ratio = 0
        dc.draw_raycast = False
        dc.update_raycast_every_step = False
        dc.max_raycast_shape = 'square' # can either be a 'circle' or a 'square'
        return dc

    @property
    def raycast_distances(self):
        if self.update_raycast_every_step :
            return np.round(self.ray_distance_list, 4)
        else :
            return self.update_raycast()
    
    @property
    def raycast_angles(self):
        if self.update_raycast_every_step :
            return self.ray_angle_list
        else :
            self.update_raycast()
            return self.ray_angle_list
    
    @property
    def raycast_angles_from_agent_view(self):
        if self.update_raycast_every_step :
            return sim.utils.constraint_angle(np.round(self.ray_angle_list-np.degrees(self.orientation),2), min_value=self.min_value_angle, max_value=self.max_value_angle) 
        else :
            self.update_raycast()
            return sim.utils.constraint_angle(np.round(self.ray_angle_list-np.degrees(self.orientation),2), min_value=self.min_value_angle, max_value=self.max_value_angle) 
    

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)
        self.wrong_fix = ['left wheel', 'right wheel', 'pan']
        self.fov = self.config.field_of_view
        self.delta_angle = self.config.delta_angle
        self.min_angle_fov = None
        self.max_angle_fov = None
        self.ray_distance_list = None # List containing the raycast distance
        self.ray_angle_list = None # List containing the angle of each ray
        self.coord_points_max_range_rays = None
        self.position = None
        self.orientation = None
        self.coord_point_rays = None
        self.draw = self.config.draw_raycast
        self.check_done = False
        self.max_value_angle = 180
        self.min_value_angle = -180
        self.update_raycast_every_step = self.config.update_raycast_every_step
        
  
    def _polar_to_cartesian(self, distance, angle):
        return distance*np.cos(angle), distance*np.sin(angle)
    
    def _update_coord_max_range_rays(self):
        r''' Update the cartesian coordinates of the max range rays 
        
        Note : It takes into account the orientation and position of the object '''

        self.coord_points_max_range_rays = []
        if self.config.max_raycast_shape == 'circle':
            for angle in self.ray_angle_list :
                self.coord_points_max_range_rays.append(self._polar_to_cartesian(distance=self.config.ray_max_range, angle=np.radians(angle) + np.pi/2))
        elif self.config.max_raycast_shape == 'square':
            for angle in self.ray_angle_list :
                self.coord_points_max_range_rays.append(self._polar_to_cartesian(distance=np.sqrt(2)*self.config.ray_max_range, angle=np.radians(angle) + np.pi/2))
            self.coord_points_max_range_rays = np.clip(self.coord_points_max_range_rays, -self.config.ray_max_range, +self.config.ray_max_range)
        self.coord_points_max_range_rays = np.array(self.coord_points_max_range_rays) + np.array(self.position)
        
    def _coord_around_object(self, position, angle, radius_object = 0.3):
        r''' Computes the coord of a point around object given a certain angle 

        Note :  This function exists because the ARIRobot was overlapping with the fixture detection
                To avoid the phenomenon we measure the fixture between a circle around the object and the max range '''
        return position + radius_object*np.array([np.cos(angle), np.sin(angle)])

    def update_raycast(self):
        self.position = self.object.position # cartesian
        self.orientation = self.object.orientation # in radians
        
        # Update the field of view range w.r.t to the robot's orientation
        self.min_angle_fov = np.round(np.degrees(self.orientation) + self.fov/2, 3) 
        self.max_angle_fov = np.round(np.degrees(self.orientation) - self.fov/2, 3)
        
        # Update the angle of the point cloud and constraint it around -180 and 180 degrees
        self.ray_angle_list = np.linspace(self.min_angle_fov, self.max_angle_fov, int(self.fov/self.config.delta_angle) + 1)
        
        # Restriction and modulo in the range -180/180
        length = self.max_value_angle - self.min_value_angle
        self.ray_angle_list = np.where(self.ray_angle_list > self.max_value_angle, self.min_value_angle + ((self.ray_angle_list - self.max_value_angle) % length), self.ray_angle_list)
        self.ray_angle_list = np.where(self.ray_angle_list < self.min_value_angle, self.max_value_angle - ((self.min_value_angle - self.ray_angle_list) % length), self.ray_angle_list)
        ray_angle_list_from_agent_viewpoint = np.round(self.ray_angle_list-np.degrees(self.orientation),3)

        # Update the variable self.coord_points_max_range_rays
        self._update_coord_max_range_rays()
        
        # Update the raycast points whether there are obstacle between the object and the max range 
        self.ray_distance_list = []
        self.coord_point_rays = []
        # Measurement Noise
        noise = np.random.normal(0,1,size=2)*self.config.noise_ratio

        

        for id, point_ray in enumerate(self.coord_points_max_range_rays):
            callback = RayCastClosestCallback(wrong_fix=self.wrong_fix)
            # Origin of rays around the object
            position = self._coord_around_object(self.position, angle=np.radians(self.ray_angle_list[id]))
            self.simulation.box2d_world.RayCast(callback, point1=position, point2=point_ray)

            if callback.hit and callback.fixture.body.userData['name'] != self.object.box2d_body.userData['name']:
                self.ray_distance_list.append(sim.utils.measure_center_distance(self.position, np.array(callback.point)+ noise))
                self.coord_point_rays.append(np.array(callback.point)+ noise)
                if self.draw :
                    self.point11 = self.simulation.box2d_world.renderer.to_screen(self.position)
                    if not ray_angle_list_from_agent_viewpoint[id]:
                        color = b2Color(1, 0, 0)
                    else :
                        color = b2Color(0.8, 0, 0.8)
                    self.draw_hit(
                            cb_point=np.array(callback.point)+ noise,
                            cb_normal=callback.normal,
                            point1=self.point11,
                            color=color
                        )
            else:
                self.ray_distance_list.append(sim.utils.measure_center_distance(self.position, np.array(point_ray)+noise))
                self.coord_point_rays.append(np.array(point_ray)+noise)
                if self.draw :
                    if not ray_angle_list_from_agent_viewpoint[id]:
                        color = b2Color(0.9, 0, 0)
                    else :
                        color = b2Color(0.8, 0, 0.8)
                    self.point11 = self.simulation.box2d_world.renderer.to_screen(self.position)
                    self.point22 = self.simulation.box2d_world.renderer.to_screen(np.array(point_ray)+noise)
                    self.simulation.box2d_world.renderer.DrawSegment(self.point11, self.point22, color)
                    self.simulation.box2d_world.renderer.DrawPoint(self.point22, 5.0, color)

        if self.draw :
            pygame.display.flip()
        
        return np.round(self.ray_distance_list, 4)
            
            
    def _check(self):
        if not self.fov :
            self.compute_raycast = False
        if not self.delta_angle :
            raise Warning('The \'delta_angle\' variable is has been set to {}. Please change that parameter to a non zero value'.format(self.delta_angle))

    def _step(self):
        if not self.check_done :
            self._check()
            self.check_done = True
        if self.update_raycast_every_step :
            self.update_raycast()


    def draw_hit(self, cb_point, cb_normal, point1, color):
        cb_point = self.simulation.box2d_world.renderer.to_screen(cb_point)
        head = b2Vec2(cb_point) + 0.5 * cb_normal

        self.simulation.box2d_world.renderer.DrawPoint(cb_point, 5.0, color)
        self.simulation.box2d_world.renderer.DrawSegment(point1, cb_point, color)
        self.simulation.box2d_world.renderer.DrawSegment(cb_point, head, color)    
        
        
        

        
