import mpi_sim
from mpi_sim.core.sensor_component import SensorComponent
import numpy as np
from mpi_sim.utils.box2d_utils import constraint_angle
import cv2

# TODO: if there are several agents that do mapping, use a common mapping process to generate the global map to save resources


class OccupancyGridMapping(SensorComponent):
    r"""Component that creates an occupancy grid map centered and oriented around its object, for example a robot agent."""

    @staticmethod
    def default_config():
        dc = SensorComponent.default_config()
        dc.name = 'occupancy_grid_mapping'
        dc.area = None  # area of global map ((x_min, x_max), (y_min, y_max)), if none it is simulation.visible_area
        dc.depth = 5.0  # depth if local in meters
        dc.object_filter = None  # filter for the elements that should be mapped
        dc.resolution = 0.05  # resolution of the maps in m per cell
        dc.update_global_map_automatically = False  # should the global map be automatically updated each step
        dc.transform = None # transform functions of the global map
        dc.update_local_map_automatically = False # update local map each time the smulation is step
        dc.imported_global_map_path = False # Import Global map - path
        dc.imported_global_map_resolution = False  # Import Global map - resolution
        dc.imported_global_map_area = False # Import Global map - area (can either be a tuple of a function is the area is not squared)
        dc.imported_global_map_px_coord_center = None
        dc.transform_position_to_map_coordinate = None
        dc.transform_real_to_sim_orientation = None
        dc.position_noise = 0
        dc.orientation_noise = 0
        return dc


    @property
    def resolution(self):
        r"""Get the tesolution of the maps in m per cell."""
        return self._resolution


    @property
    def global_map(self):
        r"""Get the global occupancy grid map from which the local map is generated."""
        if self.state.global_map is None:
            return None
        else:
            return self.update_global_map()


    @property
    def global_map_area(self):
        r"""The area that is spanned by the global map defined as ((x_min, x_max), (y_min, y_max))"""
        return self._global_map_area


    @property
    def global_map_shape(self):
        r"""The shape of the global map defined as (width, height)."""
        return self._global_map_shape


    @property
    def local_map(self):
        r"""Get the local occupancy grid map that is centered and oriented around the object of the component."""
        if self.state.local_map is None:
            return None
        else:
            return self.update_local_map()
    
    @property
    def local_map_noisy(self):
        r"""Get the noisy version of the local occupancy grid map that is centered and oriented around the object of the component."""
        if self.state.local_map_noisy is None:
            return None
        else:
            return self.update_noisy_local_map()


    @property
    def local_map_area(self):
        r"""The area that is spanned by the local map defined as ((x_min, x_max), (y_min, y_max)) in reference frame of the local map."""
        return self._local_map_area


    @property
    def local_map_shape(self):
        r"""The shape of the local map defined as (width, height)."""
        return self._local_map_shape

    
    def set_global_map(self, global_map):
        self.state.global_map = mpi_sim.utils.OccupancyGridMap(
                map=global_map,
                area=self._global_map_area,
                resolution=self._resolution
            )

    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)

        

        if not self.config.imported_global_map_path :
            self._resolution = self.config.resolution
            self._global_map_area = self.config.area

            self._global_map_shape = None
            self.state.global_map = None

            # is the global map initially created, if not do it before creating the local map
            self.state.is_global_map_initialized = False

            # initialize the global map if possible
            if self._global_map_area is not None:
                self._global_map_shape = mpi_sim.utils.get_occupancy_grid_map_shape(area=self._global_map_area, resolution=self._resolution)
                self.state.global_map = mpi_sim.utils.OccupancyGridMap(
                    map=np.zeros(self._global_map_shape),
                    area=self._global_map_area,
                    resolution=self._resolution
                )
        else :
            self._resolution = self.config.imported_global_map_resolution
            self._global_map_area = self.config.imported_global_map_area
            self.state.is_global_map_initialized = True #The global map is initialized and doesn't need to be updated 

            if isinstance(self.config.imported_global_map_path, str):

                self.imported_global_map = np.load(file=self.config.imported_global_map_path)
                
                self.state.global_map = mpi_sim.utils.OccupancyGridMap(
                    map=self.imported_global_map,
                    area=self._global_map_area,
                    resolution=self._resolution
                )


            else :
                raise UserWarning('The parameter \'imported_global_map_path\' should be a type str but it is {}'.format(type(self.config.imported_global_map_path)))

        self._local_map_area = ((-self.config.depth, self.config.depth), (-self.config.depth, self.config.depth))
        local_map_length = int((self.config.depth / self._resolution) * 2)
        self._local_map_shape = (local_map_length, local_map_length)
        self.state.local_map = mpi_sim.utils.OccupancyGridMap(
            map=np.zeros(self._local_map_shape),
            area=None,
            resolution=self._resolution
        )

        self.state.local_map_noisy = mpi_sim.utils.OccupancyGridMap(
            map=np.zeros(self._local_map_shape),
            area=None,
            resolution=self._resolution
        )


    def _add_to_simulation(self):

        # if the global map area should span the whole simulation (config.area=None), then read visible_area from the simulation when added to it
        if self._global_map_area is None:
            self._global_map_area = self.simulation.visible_area
            self._global_map_shape = mpi_sim.utils.get_occupancy_grid_map_shape(area=self._global_map_area, resolution=self._resolution)
            self.state.global_map = mpi_sim.utils.OccupancyGridMap(
                map=np.zeros(self._global_map_shape),
                area=self._global_map_area,
                resolution=self._resolution
            )


    def update_global_map(self):
        if not self.config.imported_global_map_path :
            self.state.global_map = mpi_sim.utils.create_occupancy_grid_map(
                self.simulation,
                area=self._global_map_area,
                resolution=self._resolution,
                object_filter=self.config.object_filter,
                transform=self.config.transform
            )
        else :
            
            #imported_global_map = cv2.circle(imported_global_map*255, center = [75, 65], radius = 7, color = (255, 255, 255), thickness=-1)/255  
            if self.config.transform:
               self.imported_global_map=self.config.transform(np.load(file=self.config.imported_global_map_path))  
            self.state.global_map = mpi_sim.utils.OccupancyGridMap(
                    map=self.imported_global_map,
                    area=self._global_map_area,
                    resolution=self._resolution
                )
            

        return self.state.global_map.map


    def update_local_map(self):
        if self.state.global_map is None:
            self.update_global_map()
        if not self.config.imported_global_map_path :

            self.state.local_map = mpi_sim.utils.create_local_perspective_occupancy_grid_map(
                global_map=self.state.global_map,
                position=self.object.position,
                orientation=constraint_angle(self.object.orientation),
                depth=self.config.depth,
            )
        else :
            # Same but we need to relocate the position from the center of the imported room
            self.state.local_map = mpi_sim.utils.create_local_perspective_occupancy_grid_map(
                global_map=self.state.global_map,
                # find a way to explain this 
                position=self.object.position,
                orientation=constraint_angle(self.config.transform_real_to_sim_orientation(self.object.orientation)), 
                depth=self.config.depth,
                from_given_center = self.config.imported_global_map_px_coord_center,
                transform_position_to_map_coordinate = self.config.transform_position_to_map_coordinate
            )

        return self.state.local_map.map
    
    def update_noisy_local_map(self):
        if self.state.global_map is None:
            self.update_global_map()
        if not self.config.imported_global_map_path :

            self.state.local_map_noisy = mpi_sim.utils.create_local_perspective_occupancy_grid_map(
                global_map=self.state.global_map,
                position=self.object.position + self.config.position_noise*np.array([np.random.uniform(low=-1, high=1),np.random.uniform(low=-1, high=1)]),
                orientation=constraint_angle(self.object.orientation)+ self.config.orientation_noise*np.random.uniform(low=-1, high=1),
                depth=self.config.depth,
            )
        else :
            # Same but we need to relocate the position from the center of the imported room
            self.state.local_map_noisy = mpi_sim.utils.create_local_perspective_occupancy_grid_map(
                global_map=self.state.global_map,
                position=self.object.position + self.config.position_noise*np.array([np.random.uniform(low=-1, high=1),np.random.uniform(low=-1, high=1)]),
                orientation=constraint_angle(self.config.transform_real_to_sim_orientation(self.object.orientation)) + self.config.orientation_noise*np.random.uniform(low=-1, high=1),
                depth=self.config.depth,
                from_given_center = self.config.imported_global_map_px_coord_center,
                transform_position_to_map_coordinate = self.config.transform_position_to_map_coordinate
            )

        return self.state.local_map_noisy.map


    def _step(self):
        if self.config.update_global_map_automatically or not self.state.is_global_map_initialized:
            self.update_global_map()
            self.state.is_global_map_initialized = True
        if self.config.update_local_map_automatically :
            self.update_local_map()
            self.update_noisy_local_map()
