## Software gym_box2d_diff_drive
##
## Derived from pybox2d distributed under zlib License (c), 2008-2011 Ken Lauer
## sirkne at gmail dot com
##
## This file, corresponding to gym_box2d_diff_drive module of WP6, is part of a
## project that has received funding from the European Union’s Horizon 2020
## research and innovation programme under grant agreement No 871245.
##
## Copyright (C) 2022 by INRIA
## Authors : Chris Reinke, Alex Auternaud
## alex dot auternaud at inria dot fr

import pygame
from mpi_sim.core.component import Component
import numpy as np

from Box2D import (b2Color, b2Vec2, b2RayCastCallback)
import mpi_sim as sim


# TODO: remove this raycast component


class RayCastClosestCallback(b2RayCastCallback):
    """This callback finds the closest hit"""

    def __init__(self, wrong_fix, **kwargs):
        b2RayCastCallback.__init__(self, **kwargs)
        self.wrong_fix = wrong_fix
        self.fixture = None
        self.point = None
        self.normal = None
        self.hit = False


    def ReportFixture(self, fixture, point, normal, fraction):
        """
        Called for each fixture found in the query. You control how the ray
        proceeds by returning a float that indicates the fractional length of
        the ray. By returning 0, you set the ray length to zero. By returning
        the current fraction, you proceed to find the closest point. By
        returning 1, you continue with the original ray clipping. By returning
        -1, you will filter out the current fixture (the ray will not hit it).
        """
        if fixture.body.userData['name'] not in self.wrong_fix  or not isinstance(fixture.body.userData['obj'], sim.objects.ari_robot.ARIRobot):
            self.hit = True
            self.fixture = fixture
            self.point = b2Vec2(point)
            self.normal = b2Vec2(normal)

        return fraction



# TODO: rename this component to something like ObjectDetector which can be configured to say which object types should be detected

class Raycast(Component):

    @staticmethod
    def default_config():
        dc = sim.Component.default_config()
        dc.render = False
        dc.delta_angle = 2.
        dc.raycast_length = 15.  # TODO: this should be the largest area in the simulation
        dc.p1_color = (0.4, 0.9, 0.4)
        dc.s1_color = (0.8, 0.8, 0.8)
        dc.s2_color = (0.9, 0.9, 0.4)
        dc.wall_color = (1., 1., 0.)  # yellow
        dc.bench_color = (0.9, 0., 0.7)  # magenta
        dc.human_color = (0.9, 0., 0.1)  # red
        dc.walking_man_color = (0.9, 0.4, 0.1)
        return dc


    def __init__(self, fov, config=None, **kwargs):
        super().__init__(config=config, **kwargs)
        self.wrong_fix = ['left wheel', 'right wheel', 'pan']
        self.callback_class = RayCastClosestCallback
        self.detected_humans = []
        self.detected_human_ids = []
        self.gui_process = None  # TODO: Remove
        self.point1 = None
        self.points2 = []
        self.point11 = None
        self.point22 = []
        self.color = None  # TODO: Remove
        self.fov = np.radians(fov)
        self.array_size = int(fov/self.config.delta_angle) + 1
    

    def _add(self):
        for process in self.simulation._processes.values():
            if isinstance(process, sim.mpi_sim.processes.GUI):
                self.gui_process = process        


    # def Keyboard(self, key):
    #     if key == Keys.K_b:
    #         idx = ((self.callbacks.index(self.callback_class) + 1) %
    #                len(self.callbacks))
    #         self.callback_class = self.callbacks[idx]
    

    def draw_hit(self, cb_point, cb_normal, point1, color):

        # TODO: move the rendering to the gui

        cb_point = self.simulation.box2d_world.renderer.to_screen(cb_point)
        head = b2Vec2(cb_point) + 0.5 * cb_normal

        self.simulation.box2d_world.renderer.DrawPoint(cb_point, 5.0, color)
        self.simulation.box2d_world.renderer.DrawSegment(point1, cb_point, color)
        self.simulation.box2d_world.renderer.DrawSegment(cb_point, head, color)


    def _step(self):

        if self.config.render:

            # TODO: move the rendering to the gui, actually we do not need to render the full raycasts, only the ones that hit humans

            for point_2 in self.points2:
                callback = RayCastClosestCallback(wrong_fix=self.wrong_fix)
                self.simulation.box2d_world.RayCast(callback, point1=self.point1, point2=point_2)
                self.point11 = self.simulation.box2d_world.renderer.to_screen(self.point1)
                self.point22 = self.simulation.box2d_world.renderer.to_screen(point_2)
                if callback.hit:
                        self.draw_hit(
                            cb_point=callback.point,
                            cb_normal=callback.normal,
                            point1=self.point11,
                            color=b2Color(self.color)
                        )
                else:
                    self.simulation.box2d_world.renderer.DrawSegment(self.point11, self.point22, b2Color(self.color))
            pygame.display.flip()

        # do a raycast towards each human that is in the possible sight of the detector

        # TODO: check if the human is in the potential field of view first before casting a ray cast

        for human in self.simulation.get_objects_by_type(sim.mpi_sim.objects.Human):
            callback = self.callback_class(wrong_fix=self.wrong_fix)
            point2 = human.position
            self.simulation.box2d_world.RayCast(callback, point1=self.point1, point2=point2)

            is_hit = False
            if callback.hit and callback.fixture.body.userData['name'] == human.box2d_body.userData['name']:
                is_hit = True

            if is_hit:
                self.detected_humans.append(human)
                self.detected_human_ids.append(human.id)


        # TODO: also save the raycast, i.e. point1, point2, so that the gui can draw it
